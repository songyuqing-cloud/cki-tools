"""KCIDB adjust_dumpfiles tests."""

import json
import os
import unittest
from unittest import mock

from kcidb_wrap import v4

from cki.kcidb import adjust_dumpfiles

# pylint: disable=no-self-use


class TestAdjustDumpfiles(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki.kcidb.adjust_dumpfiles."""

    def setUp(self) -> None:
        self.test_obj = v4.Test({'id': 1234, 'origin': 'redhat', 'build_id': ''})

    @mock.patch('cki.kcidb.adjust_dumpfiles.LOGGER.debug')
    @mock.patch('cki.kcidb.adjust_dumpfiles.KCIDBAdapter.dump')
    @mock.patch('cki.kcidb.adjust_dumpfiles.pathlib.Path')
    @mock.patch.dict(os.environ, {'UPT_RESULTS': 'run.done'})
    @mock.patch('glob.glob', new=lambda x: ['x'])
    def test_adjust_file(self, mock_path, mock_dump, mock_debug):
        """Ensure adjust_file doesn't break."""
        schema = v4.craft_kcidb_data([], [], [self.test_obj.render()])
        mock_path.return_value.read_text.return_value = json.dumps(schema, indent=4)
        mock_path.return_value.is_file.return_value = True
        mock_path.return_value.name = 'test.json'

        adjust_dumpfiles.adjust_file(mock_path.return_value,
                                     adjust_dumpfiles.append_upt_test_results)

        mock_dump.assert_called()
        mock_debug.assert_called_with('Trying to adjust file %s', 'test.json')

    @mock.patch('cki.kcidb.adjust_dumpfiles.KCIDBAdapter.dump')
    @mock.patch('cki.kcidb.adjust_dumpfiles.pathlib.Path')
    @mock.patch.dict(os.environ, {'UPT_RESULTS': 'run.done'})
    def test_adjust_file_no_file(self, mock_path, mock_dump):
        """Ensure adjust_file doesn't break."""
        mock_path.return_value.read_text.return_value = '{}'
        mock_path.return_value.is_file.return_value = False

        adjust_dumpfiles.adjust_file(mock_path.return_value,
                                     adjust_dumpfiles.adjust_pipeline_dumpfiles)

        mock_dump.assert_not_called()

    @mock.patch('cki.kcidb.adjust_dumpfiles.LOGGER.debug')
    @mock.patch('cki.kcidb.adjust_dumpfiles.KCIDBAdapter.dump')
    @mock.patch('glob.glob', new=lambda x: [])
    @mock.patch.dict(os.environ, {'UPT_RESULTS': 'run.done'})
    def test_adjust_file_no_adjustment(self, mock_dump, mock_debug):
        """Ensure adjust_file makes no adjustments when we don't change the data."""
        mock_path = mock.Mock()
        mock_path.read_text.return_value = '{}'
        mock_path.is_file.return_value = True
        mock_path.name = 'test.json'

        adjust_dumpfiles.adjust_file(mock_path, adjust_dumpfiles.append_upt_test_results)

        mock_dump.assert_not_called()
        mock_debug.assert_called_with('Trying to adjust file %s', 'test.json')

    @mock.patch('cki.kcidb.adjust_dumpfiles.adjust_file')
    @mock.patch('cki.kcidb.adjust_dumpfiles.pathlib.Path')
    def test_adjust_pipeline_dumpfiles(self, mock_path, mock_adjust_file):
        """Ensure adjust_pipeline_dumpfiles works."""

        func = adjust_dumpfiles.append_upt_test_results
        adjust_dumpfiles.adjust_pipeline_dumpfiles('somepath')
        mock_adjust_file.assert_any_call(mock_path.return_value, func)

    @mock.patch('cki.kcidb.adjust_dumpfiles.adjust_pipeline_dumpfiles')
    def test_main(self, mock_adjust_file):
        """Ensure main works."""
        with mock.patch.dict(os.environ, {'KCIDB_DUMPFILE_NAME': 'somepath',
                                          'UPT_RESULTS': 'run.done'}):
            adjust_dumpfiles.main()
            mock_adjust_file.assert_called()
