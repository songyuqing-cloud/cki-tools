"""KCIDB adapter tests."""

from argparse import Namespace
import os
import pathlib
import sys
import unittest
from unittest import mock

from freezegun import freeze_time
from kcidb_wrap import v4
from rcdefinition.rc_data import SKTData

from cki.kcidb.adapter import KCIDBAdapter
from cki.kcidb.adapter import main
from cki.kcidb.utils import is_empty

# pylint: disable=no-self-use

# Environment variables used
DEF_ENV_MOCK_DICT = {'CI_JOB_ID': '1234', 'CI_PIPELINE_ID': '5678', 'CI_PROJECT_DIR': '/tmp/',
                     'CI_JOB_YMD': '2018/11/30', 'CI_SERVER_URL': 'https://hostname',
                     'CI_JOB_NAME': 'build x86_64', 'CI_JOB_STAGE': 'build',
                     'CI_PROJECT_ID': '2', 'CI_PROJECT_PATH': 'cki-project/cki-pipeline',
                     'CI_COMMIT_SHA': 'deadbeef', 'CI_COMMIT_REF_NAME': 'block',
                     'commit_hash': 'c1483003cbe5136b32f73ddfdcdddfba3e5a9c67',
                     'name': 'rhel8',
                     'tree_name': 'rhel8',
                     'git_url': 'git://aaa',
                     'branch': 'rhel8',
                     'submitter': 'juser@redhat.com',
                     'config_target': 'olddefconfig',
                     'KCIDB_DUMPFILE_NAME': 'artifacts/kcidb_all.json',
                     'brew_task_id': '1234',
                     'server_url': 'https://', 'web_url': 'https://', 'top_url': 'https://'
                     }

DEF_BUILD = v4.Build({'id': '1234', 'checkout_id': 'redhat:5678', 'origin': 'redhat'})
DEF_CHECKOUT = v4.Checkout({'id': 'redhat:5678', 'origin': 'redhat'})

RC_FILE_CONTENT = '''
[state]
config_file = whatever-path-to-config
buildlog = whatever-path-to-build-log
mergelog = whatever-path-to-merge-log
kernel_arch = x86_64
tarball_file = a/long/non-existent-path-to-tarball
repo_path = something
kernel_package_url = https://
[checkout]
publishing_time = 2021-02-10T14:32:07.661846590Z
start_time = 2021-02-10T15:32:07.661846590Z
comment=foo
[build]
start_time = 2021-02-10T15:32:07.661846590Z
'''

RC_FILE_CONTENT_OLD = '''
[state]
config_file = whatever-path-to-config
buildlog = whatever-path-to-build-log
mergelog = whatever-path-to-merge-log
kernel_arch = x86_64
tarball_file = a/long/non-existent-path-to-tarball
repo_path = something
kernel_package_url = https://
[revision]
publishing_time = 2021-02-10T14:32:07.661846590Z
start_time = 2021-02-10T15:32:07.661846590Z
description=bar
[build]
start_time = 2021-02-10T15:32:07.661846590Z
'''


class TestKCIDBAdapter(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki.kcidb.adapter.KCIDBAdapter."""

    def setUp(self) -> None:
        rc_data = SKTData.deserialize(RC_FILE_CONTENT)
        config = {'deserialize.return_value': rc_data, 'return_value': rc_data}
        self.mock1 = mock.patch('cki.kcidb.adapter.SKTData', **config)
        self.mock1.start()

    @mock.patch('cki.kcidb.adapter.utils.upload_file')
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_upload_file(self, mock_upload):
        # pylint: disable=no-self-use
        """Ensure upload_file call is passed."""
        args = ('path', 'fname')
        kwargs = {'file_content': 'content', 'source_path': 'source_path'}

        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'build'}))
        adapter.upload_file(*args, **kwargs)
        mock_upload.assert_not_called()

        adapter = KCIDBAdapter(Namespace(**{'upload': True, 'artifact': 'build'}))

        adapter.upload_file(*args, **kwargs)

        mock_upload.assert_called_with(adapter.visibility, *args, **kwargs)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_get_checkout(self):
        """Ensure get_checkout works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'checkout'}))
        checkout = adapter.get_checkout()
        self.assertTrue(checkout.valid)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.LOGGER.error', mock.Mock())
    def test_get_build(self):
        """Ensure get_build works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'build'}))
        build = adapter.get_build()
        self.assertTrue(build.valid)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_checkout_id(self):
        """Ensure checkout_id uses pipeline_id."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        adapter.pipeline_id = '123'
        self.assertEqual(adapter.checkout_id, f'redhat:{adapter.pipeline_id}')

    def test_patch_contents2hash(self):
        """Ensure patch_contents2hash works."""
        self.assertIsNone(KCIDBAdapter.patch_contents2hash([]))

        self.assertEqual(KCIDBAdapter.patch_contents2hash([b'hello']),
                         'e62965c744c6b6df887dbcdc8349aa67ad79c762f2310b40dc444893719163fd')

    @mock.patch('cki.kcidb.adapter.LOGGER.debug')
    @mock.patch('cki.kcidb.adapter.SESSION')
    def test__download_url(self, mock_session, mock_debug):
        # pylint: disable=protected-access
        """Ensure _download_url works."""
        url = 'https://'

        KCIDBAdapter._download_url(url, params='something')
        mock_debug.assert_any_call('fetching %s', url)

        KCIDBAdapter._download_url(url, params=None)
        mock_debug.assert_any_call('fetching %s', url)
        mock_session.get.assert_called_with(url, stream=True, params={})

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter._download_url')
    def test__download_patches(self, mock_download_url):
        # pylint: disable=protected-access
        """Ensure _download_patches calls _download_url as many times as it should."""
        KCIDBAdapter._download_patches(['http://link', 'http://link2'])

        assert mock_download_url.call_count == 2

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_build_artifacts_path(self):
        """Ensure build_artifacts_path crafts path as expected."""
        expected = '2018/11/30/5678/build_x86_64_redhat:1234'
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        self.assertEqual(expected, adapter.build_artifacts_path)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_config_url(self, mock_upload_file):
        """Ensure config_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'config_file', None):
            _ = adapter.config_url
            mock_upload_file.assert_not_called()

        _ = adapter.config_url
        mock_upload_file.assert_called_with(adapter.build_artifacts_path, '.config',
                                            source_path=adapter.data.state.config_file)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_build_log_url(self, mock_upload_file):
        """Ensure build_log_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'buildlog', None):
            _ = adapter.build_log_url
            mock_upload_file.assert_not_called()

        _ = adapter.build_log_url
        mock_upload_file.assert_called_with(adapter.build_artifacts_path, 'build.log',
                                            source_path=adapter.data.state.buildlog)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_rev_log_url(self, mock_upload_file):
        """Ensure rev_log_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'mergelog', None):
            _ = adapter.rev_log_url
            mock_upload_file.assert_not_called()

        _ = adapter.rev_log_url
        mock_upload_file.assert_called_with(adapter.checkout_artifacts_path, 'merge.log',
                                            source_path=adapter.data.state.mergelog)

    @freeze_time('2021-02-10T16:32:07.661846+00:00')
    def test_get_brew_checkout(self):
        """Ensure get_brew_checkout works."""
        expected = {'contacts': ['juser@redhat.com'],
                    'start_time': '2021-01-21T07:41:05+00:00',
                    'id': 'redhat:5678',
                    'misc': {'job': {
                        'commit_message_title': None,
                        'created_at': '2021-02-10T15:32:07.661846+00:00',
                        'duration': 3600,
                        'finished_at': '2021-02-10T16:32:07.661846+00:00',
                        'id': 1234,
                        'kernel_version': None,
                        'name': 'build x86_64',
                        'stage': 'build',
                        'started_at': '2021-02-10T15:32:07.661846+00:00',
                        'tag': None,
                        'test_hash': None},
                        'pipeline': {
                            'created_at': '2021-01-21T07:41:05+00:00',
                            'duration': None,
                            'finished_at': None,
                            'id': 5678,
                            'project': {
                                'id': 2,
                                'instance_url': 'https://hostname',
                                'path_with_namespace': 'cki-project/cki-pipeline'},
                            'ref': 'block',
                            'sha': 'deadbeef',
                            'started_at': '2021-01-21T07:41:05+00:00',
                            'variables': {
                                'branch': 'rhel8',
                                'commit_hash': 'c1483003cbe5136b32f73ddfdcdddfba3e5a9c67',
                                'config_target': 'olddefconfig',
                                'discovery_time': '2021-01-21T07:41:05+00:00',
                                'git_url': 'git://aaa',
                                'name': 'rhel8',
                                'submitter': 'juser@redhat.com'}}},
                    'origin': 'redhat',
                    'tree_name': 'rhel8',
                    'valid': True}
        env = DEF_ENV_MOCK_DICT.copy()
        # Brew uses discovery time.
        env['discovery_time'] = '2021-01-21T07:41:05+00:00'
        with mock.patch.dict(os.environ, env):
            adapter = KCIDBAdapter(Namespace(**{'upload': False}))

            self.assertEqual(expected, adapter.get_brew_checkout().to_mapping())

    @mock.patch('cki.kcidb.adapter.KojiWrap.taskinfo')
    @freeze_time('2021-02-10T16:32:07.661846590+00:00')
    def test_get_brew_build(self, mock_koji):
        """Ensure get_brew_build works."""
        expected = {'architecture': 'x86_64',
                    'duration': 9048,
                    'id': 'redhat:1234_x86_64',
                    'misc': {'debug': False,
                             'job': {
                                 'commit_message_title': None,
                                 'created_at': '2021-02-10T15:32:07.661846+00:00',
                                 'duration': 3600,
                                 'finished_at': '2021-02-10T16:32:07.661846+00:00',
                                 'id': 1234,
                                 'kernel_version': None,
                                 'name': 'build x86_64',
                                 'stage': 'build',
                                 'started_at': '2021-02-10T15:32:07.661846+00:00',
                                 'tag': None,
                                 'test_hash': None},
                             'pipeline': {
                                 'created_at': '2021-01-21T07:41:05+00:00',
                                 'duration': None,
                                 'finished_at': None,
                                 'id': 5678,
                                 'project': {
                                     'id': 2,
                                     'instance_url': 'https://hostname',
                                     'path_with_namespace': 'cki-project/cki-pipeline'},
                                 'ref': 'block',
                                 'sha': 'deadbeef',
                                 'started_at': '2021-01-21T07:41:05+00:00',
                                 'variables': {
                                     'branch': 'rhel8',
                                     'commit_hash': 'c1483003cbe5136b32f73ddfdcdddfba3e5a9c67',
                                     'config_target': 'olddefconfig',
                                     'discovery_time': '2021-01-21T07:41:05+00:00',
                                     'git_url': 'git://aaa',
                                     'name': 'rhel8',
                                     'submitter': 'juser@redhat.com'}}},
                    'origin': 'redhat',
                    'checkout_id': 'redhat:5678',
                    'start_time': '2021-02-10T15:32:07.661846+00:00',
                    'valid': True}
        mock_koji.return_value = {'Started': 'Thu Jan 14 18:23:57 2021',
                                  'Finished': 'Thu Jan 14 20:54:45 2021'}

        env = DEF_ENV_MOCK_DICT.copy()
        # Brew uses discovery time.
        env['discovery_time'] = '2021-01-21T07:41:05+00:00'
        with mock.patch.dict(os.environ, env):
            adapter = KCIDBAdapter(Namespace(**{'upload': False}))

            build = adapter.get_brew_build()

            self.assertEqual(expected, build.to_mapping())

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.LOGGER.error')
    def test_build_output_files(self, mock_error):
        """Ensure build_output_files checks that path to file exists."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        _ = adapter.build_output_files
        mock_error.assert_called_with("Job %s: File not found: %s", adapter.jobid,
                                      adapter.data.state.tarball_file)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_build_output_files_upload(self, mock_upload_file):
        """Ensure build_output_files uploads build artifacts when they exist."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        mock_upload_file.return_value = 'url1'
        expected = [{'name': 'non-existent-path-to-tarball', 'url': 'url1'},
                    {'name': 'kernel_package_url', 'url': 'https://'}]

        with mock.patch('cki.kcidb.adapter.pathlib.Path.is_file', lambda x: True):
            result = adapter.build_output_files
            self.assertEqual(expected, result)

        mock_upload_file.assert_called_with(adapter.build_artifacts_path,
                                            os.path.basename(adapter.data.state.tarball_file),
                                            source_path=adapter.data.state.tarball_file)

    @mock.patch('cki.kcidb.adapter.validate')
    @mock.patch('cki.kcidb.adapter.json.dump')
    def test_dump(self, mock_json_dump, mock_validate):
        """Ensure dump works."""
        data = {}
        path2file = 'whatever'

        with mock.patch('builtins.open') as mock_open:
            KCIDBAdapter.dump(data, path2file)

            mock_validate.assert_called_with(data)
            mock_open.assert_called_with(path2file, 'w')
            mock_json_dump.assert_called()

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.craft_kcidb_data')
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.dump')
    def test_output_objects(self, mock_dump, mock_craft):
        """Ensure output_objects works."""
        path2dump = 'path'
        test = v4.Test({'id': '1234', 'build_id': '1234', 'origin': 'redhat'})

        def prep_object(obj):
            return [{key: value for key, value in obj.to_mapping().items()
                     if not is_empty(value)}]

        KCIDBAdapter.output_objects(path2dump, [DEF_CHECKOUT])

        mock_craft.assert_called_with(prep_object(DEF_CHECKOUT), [], [])
        mock_dump.assert_called()

        KCIDBAdapter.output_objects(path2dump, [DEF_BUILD])

        mock_craft.assert_called_with([], prep_object(DEF_BUILD), [])
        mock_dump.assert_called()

        KCIDBAdapter.output_objects(path2dump, [test])

        mock_craft.assert_called_with([], [], prep_object(test))
        mock_dump.assert_called()

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter')
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_main_brew(self, mock_adapter):
        """Ensure main works for brew env."""
        mock_adapter.return_value.selftests = []
        mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_BUILD]

        with mock.patch.object(sys, 'argv', ['./app', 'build']):
            mock_adapter.return_value.get_brew_build.return_value = DEF_BUILD

            main()

            mock_adapter.return_value.output_objects.assert_called_with(
                pathlib.Path(DEF_ENV_MOCK_DICT['KCIDB_DUMPFILE_NAME']),
                [DEF_BUILD])

        mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_CHECKOUT]
        with mock.patch.object(sys, 'argv', ['./app', 'checkout']):
            mock_adapter.return_value.get_brew_checkout.return_value = DEF_CHECKOUT

            main()
            mock_adapter.return_value.output_objects.assert_called_with(
                pathlib.Path(DEF_ENV_MOCK_DICT['KCIDB_DUMPFILE_NAME']), [DEF_CHECKOUT])

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter')
    def test_main_non_brew(self, mock_adapter):
        """Ensure main works for non-brew env."""
        newenv = DEF_ENV_MOCK_DICT.copy()
        del newenv['brew_task_id']

        mock_adapter.return_value.selftests = []
        with mock.patch.dict(os.environ, newenv):
            mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_BUILD]
            with mock.patch.object(sys, 'argv', ['./app', 'build']):

                mock_adapter.return_value.get_build.return_value = DEF_BUILD

                main()
                mock_adapter.return_value.output_objects.assert_called_with(
                    pathlib.Path(newenv['KCIDB_DUMPFILE_NAME']),
                    [DEF_BUILD])

            mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_CHECKOUT]
            with mock.patch.object(sys, 'argv', ['./app', 'checkout']):
                mock_adapter.return_value.get_checkout.return_value = DEF_CHECKOUT

                main()
                mock_adapter.return_value.output_objects.assert_called_with(
                    pathlib.Path(newenv['KCIDB_DUMPFILE_NAME']),
                    [DEF_CHECKOUT])

    def test_umb_tests(self):
        """Ensure umb_tests works."""
        mock_dict = DEF_ENV_MOCK_DICT.copy()
        mock_dict['started_at'] = '2021-01-21T07:41:05+00:00'
        mock_dict['data'] = '/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4ADbAJ5dAC2ewEdDZH3xj1iFxpw6Odwp47o' \
                            'YbD9TQrMMAOdpO2vsnUifoRV+r/0WWN28hS7BjNcC+gpP72sON48tteccKdlqjnV5XP' \
                            'mQg5EBsUmGtxvl/R39wZeAFp/jziS3Y7kU3UOQG9kVscQYHCAcSReDH1yT9DC8lkPck' \
                            'gaRKOb6bxmZmOzLwmDACJPXR3NpB3wLJL7P6cXfjf5fcgRweZAAAAAA6J9i7iv0bMMA' \
                            'AboB3AEAAGihJCixxGf7AgAAAAAEWVo='

        with mock.patch.dict(os.environ, mock_dict):
            adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'umb-tests'}))
            self.assertIsInstance(adapter.umb_tests[0], v4.UMBTest)


class TestKCIDBAdapterOldRcData(unittest.TestCase):
    # pylint: disable=too-many-public-methods
    """Test cki.kcidb.adapter.KCIDBAdapter with old RC data format."""

    def setUp(self) -> None:
        rc_data = SKTData.deserialize(RC_FILE_CONTENT_OLD)
        config = {'deserialize.return_value': rc_data, 'return_value': rc_data}
        self.mock1 = mock.patch('cki.kcidb.adapter.SKTData', **config)
        self.mock1.start()

    @mock.patch('cki.kcidb.adapter.utils.upload_file')
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_upload_file(self, mock_upload):
        # pylint: disable=no-self-use
        """Ensure upload_file call is passed."""
        args = ('path', 'fname')
        kwargs = {'file_content': 'content', 'source_path': 'source_path'}

        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'build'}))
        adapter.upload_file(*args, **kwargs)
        mock_upload.assert_not_called()

        adapter = KCIDBAdapter(Namespace(**{'upload': True, 'artifact': 'build'}))

        adapter.upload_file(*args, **kwargs)

        mock_upload.assert_called_with(adapter.visibility, *args, **kwargs)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_get_checkout(self):
        """Ensure get_checkout works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'checkout'}))
        checkout = adapter.get_checkout()
        self.assertTrue(checkout.valid)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.LOGGER.error', mock.Mock())
    def test_get_build(self):
        """Ensure get_build works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'build'}))
        build = adapter.get_build()
        self.assertTrue(build.valid)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_checkout_id(self):
        """Ensure checkout_id uses pipeline_id."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        adapter.pipeline_id = '123'
        self.assertEqual(adapter.checkout_id, f'redhat:{adapter.pipeline_id}')

    def test_patch_contents2hash(self):
        """Ensure patch_contents2hash works."""
        self.assertIsNone(KCIDBAdapter.patch_contents2hash([]))

        self.assertEqual(KCIDBAdapter.patch_contents2hash([b'hello']),
                         'e62965c744c6b6df887dbcdc8349aa67ad79c762f2310b40dc444893719163fd')

    @mock.patch('cki.kcidb.adapter.LOGGER.debug')
    @mock.patch('cki.kcidb.adapter.SESSION')
    def test__download_url(self, mock_session, mock_debug):
        # pylint: disable=protected-access
        """Ensure _download_url works."""
        url = 'https://'

        KCIDBAdapter._download_url(url, params='something')
        mock_debug.assert_any_call('fetching %s', url)

        KCIDBAdapter._download_url(url, params=None)
        mock_debug.assert_any_call('fetching %s', url)
        mock_session.get.assert_called_with(url, stream=True, params={})

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter._download_url')
    def test__download_patches(self, mock_download_url):
        # pylint: disable=protected-access
        """Ensure _download_patches calls _download_url as many times as it should."""
        KCIDBAdapter._download_patches(['http://link', 'http://link2'])

        assert mock_download_url.call_count == 2

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_build_artifacts_path(self):
        """Ensure build_artifacts_path crafts path as expected."""
        expected = '2018/11/30/5678/build_x86_64_redhat:1234'
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        self.assertEqual(expected, adapter.build_artifacts_path)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_config_url(self, mock_upload_file):
        """Ensure config_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'config_file', None):
            _ = adapter.config_url
            mock_upload_file.assert_not_called()

        _ = adapter.config_url
        mock_upload_file.assert_called_with(adapter.build_artifacts_path, '.config',
                                            source_path=adapter.data.state.config_file)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_build_log_url(self, mock_upload_file):
        """Ensure build_log_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'buildlog', None):
            _ = adapter.build_log_url
            mock_upload_file.assert_not_called()

        _ = adapter.build_log_url
        mock_upload_file.assert_called_with(adapter.build_artifacts_path, 'build.log',
                                            source_path=adapter.data.state.buildlog)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_rev_log_url(self, mock_upload_file):
        """Ensure rev_log_url works."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        with mock.patch.object(adapter.data.state, 'mergelog', None):
            _ = adapter.rev_log_url
            mock_upload_file.assert_not_called()

        _ = adapter.rev_log_url
        mock_upload_file.assert_called_with(adapter.checkout_artifacts_path, 'merge.log',
                                            source_path=adapter.data.state.mergelog)

    @freeze_time('2021-02-10T16:32:07.661846+00:00')
    def test_get_brew_checkout(self):
        """Ensure get_brew_checkout works."""
        expected = {'contacts': ['juser@redhat.com'],
                    'start_time': '2021-01-21T07:41:05+00:00',
                    'id': 'redhat:5678',
                    'misc': {'job': {
                        'commit_message_title': None,
                        'created_at': '2021-02-10T15:32:07.661846+00:00',
                        'duration': 3600,
                        'finished_at': '2021-02-10T16:32:07.661846+00:00',
                        'id': 1234,
                        'kernel_version': None,
                        'name': 'build x86_64',
                        'stage': 'build',
                        'started_at': '2021-02-10T15:32:07.661846+00:00',
                        'tag': None,
                        'test_hash': None},
                        'pipeline': {
                            'created_at': '2021-01-21T07:41:05+00:00',
                            'duration': None,
                            'finished_at': None,
                            'id': 5678,
                            'project': {
                                'id': 2,
                                'instance_url': 'https://hostname',
                                'path_with_namespace': 'cki-project/cki-pipeline'},
                            'ref': 'block',
                            'sha': 'deadbeef',
                            'started_at': '2021-01-21T07:41:05+00:00',
                            'variables': {
                                'branch': 'rhel8',
                                'commit_hash': 'c1483003cbe5136b32f73ddfdcdddfba3e5a9c67',
                                'config_target': 'olddefconfig',
                                'discovery_time': '2021-01-21T07:41:05+00:00',
                                'git_url': 'git://aaa',
                                'name': 'rhel8',
                                'submitter': 'juser@redhat.com'}}},
                    'origin': 'redhat',
                    'tree_name': 'rhel8',
                    'valid': True}
        env = DEF_ENV_MOCK_DICT.copy()
        # Brew uses discovery time.
        env['discovery_time'] = '2021-01-21T07:41:05+00:00'
        with mock.patch.dict(os.environ, env):
            adapter = KCIDBAdapter(Namespace(**{'upload': False}))

            self.assertEqual(expected, adapter.get_brew_checkout().to_mapping())

    @mock.patch('cki.kcidb.adapter.KojiWrap.taskinfo')
    @freeze_time('2021-02-10T16:32:07.661846590+00:00')
    def test_get_brew_build(self, mock_koji):
        """Ensure get_brew_build works."""
        expected = {'architecture': 'x86_64',
                    'duration': 9048,
                    'id': 'redhat:1234_x86_64',
                    'misc': {'debug': False,
                             'job': {
                                 'commit_message_title': None,
                                 'created_at': '2021-02-10T15:32:07.661846+00:00',
                                 'duration': 3600,
                                 'finished_at': '2021-02-10T16:32:07.661846+00:00',
                                 'id': 1234,
                                 'kernel_version': None,
                                 'name': 'build x86_64',
                                 'stage': 'build',
                                 'started_at': '2021-02-10T15:32:07.661846+00:00',
                                 'tag': None,
                                 'test_hash': None},
                             'pipeline': {
                                 'created_at': '2021-01-21T07:41:05+00:00',
                                 'duration': None,
                                 'finished_at': None,
                                 'id': 5678,
                                 'project': {
                                     'id': 2,
                                     'instance_url': 'https://hostname',
                                     'path_with_namespace': 'cki-project/cki-pipeline'},
                                 'ref': 'block',
                                 'sha': 'deadbeef',
                                 'started_at': '2021-01-21T07:41:05+00:00',
                                 'variables': {
                                     'branch': 'rhel8',
                                     'commit_hash': 'c1483003cbe5136b32f73ddfdcdddfba3e5a9c67',
                                     'config_target': 'olddefconfig',
                                     'discovery_time': '2021-01-21T07:41:05+00:00',
                                     'git_url': 'git://aaa',
                                     'name': 'rhel8',
                                     'submitter': 'juser@redhat.com'}}},
                    'origin': 'redhat',
                    'checkout_id': 'redhat:5678',
                    'start_time': '2021-02-10T15:32:07.661846+00:00',
                    'valid': True}
        mock_koji.return_value = {'Started': 'Thu Jan 14 18:23:57 2021',
                                  'Finished': 'Thu Jan 14 20:54:45 2021'}

        env = DEF_ENV_MOCK_DICT.copy()
        # Brew uses discovery time.
        env['discovery_time'] = '2021-01-21T07:41:05+00:00'
        with mock.patch.dict(os.environ, env):
            adapter = KCIDBAdapter(Namespace(**{'upload': False}))

            build = adapter.get_brew_build()

            self.assertEqual(expected, build.to_mapping())

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.LOGGER.error')
    def test_build_output_files(self, mock_error):
        """Ensure build_output_files checks that path to file exists."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))

        _ = adapter.build_output_files
        mock_error.assert_called_with("Job %s: File not found: %s", adapter.jobid,
                                      adapter.data.state.tarball_file)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.upload_file')
    def test_build_output_files_upload(self, mock_upload_file):
        """Ensure build_output_files uploads build artifacts when they exist."""
        adapter = KCIDBAdapter(Namespace(**{'upload': False}))
        mock_upload_file.return_value = 'url1'
        expected = [{'name': 'non-existent-path-to-tarball', 'url': 'url1'},
                    {'name': 'kernel_package_url', 'url': 'https://'}]

        with mock.patch('cki.kcidb.adapter.pathlib.Path.is_file', lambda x: True):
            result = adapter.build_output_files
            self.assertEqual(expected, result)

        mock_upload_file.assert_called_with(adapter.build_artifacts_path,
                                            os.path.basename(adapter.data.state.tarball_file),
                                            source_path=adapter.data.state.tarball_file)

    @mock.patch('cki.kcidb.adapter.validate')
    @mock.patch('cki.kcidb.adapter.json.dump')
    def test_dump(self, mock_json_dump, mock_validate):
        """Ensure dump works."""
        data = {}
        path2file = 'whatever'

        with mock.patch('builtins.open') as mock_open:
            KCIDBAdapter.dump(data, path2file)

            mock_validate.assert_called_with(data)
            mock_open.assert_called_with(path2file, 'w')
            mock_json_dump.assert_called()

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('cki.kcidb.adapter.craft_kcidb_data')
    @mock.patch('cki.kcidb.adapter.KCIDBAdapter.dump')
    def test_output_objects(self, mock_dump, mock_craft):
        """Ensure output_objects works."""
        path2dump = 'path'
        test = v4.Test({'id': '1234', 'build_id': '1234', 'origin': 'redhat'})

        def prep_object(obj):
            return [{key: value for key, value in obj.to_mapping().items()
                     if not is_empty(value)}]

        KCIDBAdapter.output_objects(path2dump, [DEF_CHECKOUT])

        mock_craft.assert_called_with(prep_object(DEF_CHECKOUT), [], [])
        mock_dump.assert_called()

        KCIDBAdapter.output_objects(path2dump, [DEF_BUILD])

        mock_craft.assert_called_with([], prep_object(DEF_BUILD), [])
        mock_dump.assert_called()

        KCIDBAdapter.output_objects(path2dump, [test])

        mock_craft.assert_called_with([], [], prep_object(test))
        mock_dump.assert_called()

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter')
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_main_brew(self, mock_adapter):
        """Ensure main works for brew env."""
        mock_adapter.return_value.selftests = []
        mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_BUILD]

        with mock.patch.object(sys, 'argv', ['./app', 'build']):
            mock_adapter.return_value.get_brew_build.return_value = DEF_BUILD

            main()

            mock_adapter.return_value.output_objects.assert_called_with(
                pathlib.Path(DEF_ENV_MOCK_DICT['KCIDB_DUMPFILE_NAME']),
                [DEF_BUILD])

        mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_CHECKOUT]
        with mock.patch.object(sys, 'argv', ['./app', 'checkout']):
            mock_adapter.return_value.get_brew_checkout.return_value = DEF_CHECKOUT

            main()
            mock_adapter.return_value.output_objects.assert_called_with(
                pathlib.Path(DEF_ENV_MOCK_DICT['KCIDB_DUMPFILE_NAME']), [DEF_CHECKOUT])

    @mock.patch('cki.kcidb.adapter.KCIDBAdapter')
    def test_main_non_brew(self, mock_adapter):
        """Ensure main works for non-brew env."""
        newenv = DEF_ENV_MOCK_DICT.copy()
        del newenv['brew_task_id']

        mock_adapter.return_value.selftests = []
        with mock.patch.dict(os.environ, newenv):
            mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_BUILD]
            with mock.patch.object(sys, 'argv', ['./app', 'build']):

                mock_adapter.return_value.get_build.return_value = DEF_BUILD

                main()
                mock_adapter.return_value.output_objects.assert_called_with(
                    pathlib.Path(newenv['KCIDB_DUMPFILE_NAME']),
                    [DEF_BUILD])

            mock_adapter.return_value.collect_in_single_dumpfile.return_value = [DEF_CHECKOUT]
            with mock.patch.object(sys, 'argv', ['./app', 'checkout']):
                mock_adapter.return_value.get_checkout.return_value = DEF_CHECKOUT

                main()
                mock_adapter.return_value.output_objects.assert_called_with(
                    pathlib.Path(newenv['KCIDB_DUMPFILE_NAME']),
                    [DEF_CHECKOUT])

    def test_umb_tests(self):
        """Ensure umb_tests works."""
        mock_dict = DEF_ENV_MOCK_DICT.copy()
        mock_dict['started_at'] = '2021-01-21T07:41:05+00:00'
        mock_dict['data'] = '/Td6WFoAAATm1rRGAgAhARYAAAB0L+Wj4ADbAJ5dAC2ewEdDZH3xj1iFxpw6Odwp47o' \
                            'YbD9TQrMMAOdpO2vsnUifoRV+r/0WWN28hS7BjNcC+gpP72sON48tteccKdlqjnV5XP' \
                            'mQg5EBsUmGtxvl/R39wZeAFp/jziS3Y7kU3UOQG9kVscQYHCAcSReDH1yT9DC8lkPck' \
                            'gaRKOb6bxmZmOzLwmDACJPXR3NpB3wLJL7P6cXfjf5fcgRweZAAAAAA6J9i7iv0bMMA' \
                            'AboB3AEAAGihJCixxGf7AgAAAAAEWVo='

        with mock.patch.dict(os.environ, mock_dict):
            adapter = KCIDBAdapter(Namespace(**{'upload': False, 'artifact': 'umb-tests'}))
            self.assertIsInstance(adapter.umb_tests[0], v4.UMBTest)
