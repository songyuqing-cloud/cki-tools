"""KCIDB objects tests."""
# pylint: disable=no-member,protected-access,too-many-lines
# noqa: E501
import tempfile
import unittest

import gitlab
import responses

from cki.kcidb import objects
from cki.kcidb import utils
from tests.mocks import get_mocked_bucket
from tests.mocks import mock_gitlab


class TestGitlabPipeline(unittest.TestCase):
    """Test the Gitlab pipeline class."""
    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)
        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        self.g_pipeline = g_project.pipelines.get(567899)

    @responses.activate
    def test_variables(self):
        """Test variables property."""
        self.mock_gitlab()
        pipeline = objects.GitlabPipeline(self.g_pipeline)

        self.assertEqual(24, len(pipeline.variables.keys()))
        self.assertEqual('Baseline: mainline.kernel.org master:decd6167bf4f',
                         pipeline.variables['title'])

    @responses.activate
    def test_jobs(self):
        """Test jobs property."""
        self.mock_gitlab()
        pipeline = objects.GitlabPipeline(self.g_pipeline)

        self.assertEqual(5, len(pipeline.jobs))

    @responses.activate
    def test_get_job(self):
        """Test get_job method."""
        self.mock_gitlab()

        pipeline = objects.GitlabPipeline(self.g_pipeline)

        # Get merge job.
        job = pipeline.get_job(stage='merge')
        self.assertEqual(841324, job.id)

        # No failed merge.
        job = pipeline.get_job(stage='merge', status='failed')
        self.assertEqual(None, job)

        # Get some builds.
        job = pipeline.get_job(stage='build', arch='x86_64')
        self.assertEqual(841325, job.id)

        # No aarch64 build.
        job = pipeline.get_job(stage='build', arch='aarch64')
        self.assertEqual(None, job)

        # No foobar stage.
        job = pipeline.get_job(stage='foobar', arch='aarch64')
        self.assertEqual(None, job)

        # Test exact name filter
        job = pipeline.get_job(stage='build', name='build x86_64')
        self.assertEqual(841325, job.id)

        # Test exact name filter with missing job
        job = pipeline.get_job(stage='publish', name='publish foobar')
        self.assertEqual(None, job)

        # Test without 'stage' parameter
        job = pipeline.get_job(name='build x86_64')
        self.assertEqual(841325, job.id)

        # Test get by id
        job = pipeline.get_job(job_id=841325)
        self.assertEqual(841325, job.id)

        # Test no reverse order
        job = pipeline.get_job(reverse=False)
        self.assertEqual(841323, job.id)

    def test_artifacts_path(self):
        """Test artifacts_path property."""
        pipeline = objects.GitlabPipeline(self.g_pipeline)
        self.assertEqual('2020/05/14/567899', pipeline.artifacts_path)

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/jobs',
             'gitlab_projects_2_pipelines_567899_jobs.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841323',
             'gitlab_projects_2_jobs_841323_prepare.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841325',
             'gitlab_projects_2_jobs_841325_build.json'),
        ]
        mock_gitlab(json_mocks)


class TestJob(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(567899))
        g_job = objects.GitlabJob(g_project.jobs.get(841324))
        self.job = objects.Job(g_pipeline, g_job)

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/841324/artifacts/rc',
             'gitlab_projects_2_jobs_841324_rc'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    def test_pipeline(self):
        """Test pipeline property."""
        self.assertIsInstance(self.job.pipeline, objects.GitlabPipeline)

    @responses.activate
    def test_rc(self):
        """Test rc file property."""
        self.mock_gitlab()
        self.assertDictEqual(
            {'runner': {},
             'state': {'commit_message_title': "Merge branch 'akpm' (patches from Andrew)",
                       'mergelog': 'artifacts/merge.log',
                       'stage_merge': 'pass',
                       'tag': '-decd616',
                       'test_hash': 'decd6167bf4f6bec1284006d0522381b44660df3',
                       'workdir': '/builds/cki-project/cki-pipeline/workdir'}},
            self.job.rc)

    def test_artifacts_path(self):
        """Test artifacts_path."""
        self.assertIsInstance(self.job.artifacts_path, NotImplementedError)

    @responses.activate
    def test_download_file(self):
        """Test _download_file method."""
        responses.add(responses.GET, 'https://server/file', body=b'file body')
        self.assertEqual(
            b'file body',
            self.job._download_file('https://server/file')
        )

    @responses.activate
    def test_download_file_fail(self):
        """Test _download_file method. Check for raise_for_status."""
        responses.add(responses.GET, 'https://server/file', status=404)
        self.assertRaises(Exception, self.job._download_file, 'https://server/file')

    @responses.activate
    def test_download_file_gitlab(self):
        """Test _download_file method with Gitlab url."""
        responses.add(
            responses.GET,
            'https://gitlab/api/v4/projects/2/jobs/123/artifacts/artifacts/file',
            body=b'file body'
        )
        responses.add(
            responses.GET,
            'https://gitlab/api/v4/projects/2/pipelines/567899/jobs',
            json=[{'id': 123}]
        )
        responses.add(
            responses.GET,
            'https://gitlab/api/v4/projects/2/jobs/123',
            json={'id': 123}
        )
        self.assertEqual(
            b'file body',
            self.job._download_file(
                'https://gitlab.com/api/v4/projects/2/jobs/123/artifacts/artifacts/file',
            )
        )

        # Ensure all requests had the private token set.
        self.assertTrue(
            all(
                call.request.headers.get('PRIVATE-TOKEN') == 'token'
                for call in responses.calls
            )
        )

    @responses.activate
    @unittest.mock.patch('cki.kcidb.objects.Job._render')
    def test_render(self, _render):
        """Test render method."""
        self.mock_gitlab()
        _render.return_value = {
            'remove_none': None,
            'not_remove': 'something inherited from _render',
            'misc': {'other': 'also inherited'}
        }
        self.assertDictEqual(
            {
                'origin': 'redhat',
                'not_remove': 'something inherited from _render',
                'misc': {
                    'other': 'also inherited',
                    'job': {
                        'commit_message_title': "Merge branch 'akpm' (patches from Andrew)",
                        'created_at': '2020-05-14T20:29:50.801Z',
                        'duration': 106.740939,
                        'finished_at': '2020-05-14T20:33:09.808Z',
                        'id': 841324,
                        'kernel_version': None,
                        'name': 'merge',
                        'stage': 'merge',
                        'started_at': '2020-05-14T20:31:23.067Z',
                        'tag': '-decd616',
                        'test_hash': 'decd6167bf4f6bec1284006d0522381b44660df3'
                    },
                    'pipeline': {
                        'created_at': '2020-05-14T20:29:50.757Z',
                        'duration': 18379,
                        'finished_at': '2020-05-15T01:36:22.946Z',
                        'id': 567899,
                        'project': {
                            'id': 2,
                            'path_with_namespace': 'cki-project/cki-pipeline',
                            'instance_url': 'https://gitlab'
                        },
                        'ref': 'mainline.kernel.org',
                        'sha': 'c783244cf6a6bacc63f70872cf4bff0d2aabc74a',
                        'started_at': '2020-05-14T20:29:52.697Z',
                        'variables': {
                            'architectures': 'x86_64',
                            'branch': 'master',
                            'builder_image':
                            'registry.gitlab.com/cki-project/containers/builder-fedora',
                            'cki_pipeline_branch': 'mainline.kernel.org',
                            'cki_pipeline_id':
                            'd660494abb0f4b9931a4ea6dac22f8e35c725abd3fb76d595f768db93ae6cbc5',
                            'cki_pipeline_type': 'baseline',
                            'cki_project': 'cki-project/cki-pipeline',
                            'commit_hash': 'decd6167bf4f6bec1284006d0522381b44660df3',
                            'config_target': 'olddefconfig',
                            'git_url':
                            'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
                            'kernel_type': 'upstream',
                            'mail_add_maintainers_to': 'cc',
                            'mail_bcc': 'mail@redhat.com',
                            'mail_from': 'CKI Project <cki-project@redhat.com>',
                            'make_target': 'targz-pkg',
                            'name': 'mainline.kernel.org',
                            'patch_urls': 'http://patchwork/patch/2200409/mbox/ '
                                          'http://patchwork/patch/2200411/mbox/',
                            'report_template': 'limited',
                            'report_types': 'email',
                            'require_manual_review': 'False',
                            'test_set': 'kt1',
                            'title': 'Baseline: mainline.kernel.org '
                                     'master:decd6167bf4f',
                            'tree_name': 'upstream',
                            'test_mode': 'skt',
                        }
                    }
                }
            },
            self.job.render())
        self.assertTrue(_render.called)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS',
                         {'private': 'private-bucket', 'public': 'public-bucket'})
    def test_bucket(self):
        """Test bucket value is the correct."""
        # No variables.
        with unittest.mock.patch('cki.kcidb.objects.GitlabPipeline.variables', {}):

            self.assertEqual(self.job.visibility, 'private')

        # Private.
        with unittest.mock.patch('cki.kcidb.objects.GitlabPipeline.variables',
                                 {'artifacts_visibility': 'private'}):
            self.assertEqual(self.job.visibility, 'private')

        # Public.
        with unittest.mock.patch('cki.kcidb.objects.GitlabPipeline.variables',
                                 {'artifacts_visibility': 'public'}):

            self.assertEqual(self.job.visibility, 'public')


class TestCheckout(unittest.TestCase):
    """Test Checkout methods."""

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(567899))
        g_job = objects.GitlabJob(g_project.jobs.get(841324))
        self.checkout = objects.Checkout(g_pipeline, g_job)
        self.checkout.upload_artifacts()

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/841324/artifacts/rc',
             'gitlab_projects_2_jobs_841324_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/841324/artifacts/artifacts/merge%2Elog', None),
            ('http://patchwork/patch/2200409/raw/', 'patch one'),
            ('http://patchwork/patch/2200411/raw/', 'patch two'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    def test_artifacts_path(self):
        """Test artifacts path property."""
        self.mock_gitlab()
        self.assertEqual('2020/05/14/567899', self.checkout.artifacts_path)

    @responses.activate
    def test_patchset_files(self):
        """Test patchset_files."""
        self.mock_gitlab()
        self.assertListEqual(
            [{'name': 'mbox', 'url': 'http://patchwork/patch/2200409/mbox/'},
             {'name': 'mbox', 'url': 'http://patchwork/patch/2200411/mbox/'}],
            self.checkout.patchset_files)

        del self.checkout.pipeline.variables['patch_urls']
        self.assertListEqual([], self.checkout.patchset_files)

    def test_contacts(self):
        """Test contacts."""
        self.assertListEqual([], self.checkout.contacts)

    @responses.activate
    def test_log_url(self):
        """Test log_url."""
        self.mock_gitlab()
        self.assertEqual(
            f'endpoint/bucket/prefix/{self.checkout.artifacts_path}/merge.log',
            self.checkout.log_url)

    @responses.activate
    def test_valid(self):
        """Test valid."""
        self.mock_gitlab()
        self.assertTrue(self.checkout.valid)

        self.checkout.rc['state']['stage_merge'] = 'failed'
        self.assertFalse(self.checkout.valid)

    def test_misc(self):
        """Test misc."""
        self.assertDictEqual({}, self.checkout.misc)

    @responses.activate
    def test_id(self):
        """Test id."""
        self.mock_gitlab()
        self.assertEqual('redhat:567899', self.checkout.id)

    @unittest.mock.patch('cki.kcidb.objects.SESSION')
    def test_patchset_hash_patch_raw(self, session):
        """Test raw patch url replacing for patchwork urls."""
        class GetMock:
            # pylint: disable=too-few-public-methods
            """Mock get call."""

            @property
            def content(self):
                """Return dummy content."""
                return b'foobar'
        session.get.return_value = GetMock()

        self.checkout.pipeline.variables['patch_urls'] = (
            'http://patchwork/patch/2200409/mbox/ '
            'http://patchwork/patch/2200410/mbox '
            'http://patchwork/patch/2200411/raw/ '
            'http://other/patch/2200412'
        )
        # pylint: disable=pointless-statement
        self.checkout.patchset_hash

        session.get.assert_has_calls([
            unittest.mock.call('http://patchwork/patch/2200409/raw/'),
            unittest.mock.call('http://patchwork/patch/2200410/raw/'),
            unittest.mock.call('http://patchwork/patch/2200411/raw/'),
            unittest.mock.call('http://other/patch/2200412'),
        ])

    @responses.activate
    def test_render(self):
        """Test render."""
        self.mock_gitlab()

        self.assertDictEqual(
            {
                'contacts': [],
                'comment': None,
                'start_time': '2020-05-14T20:29:50.757Z',
                'git_repository_branch': 'master',
                'git_commit_hash': 'decd6167bf4f6bec1284006d0522381b44660df3',
                'git_commit_name': None,
                'git_repository_url':
                'https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git',
                'id': 'redhat:567899',
                'log_url': f'endpoint/bucket/prefix/{self.checkout.artifacts_path}/merge.log',
                'message_id': None,
                'misc': {},
                'patchset_files': [{'name': 'mbox', 'url': 'http://patchwork/patch/2200409/mbox/'},
                                   {'name': 'mbox', 'url': 'http://patchwork/patch/2200411/mbox/'}],
                'patchset_hash': 'd42727d9c9affdccbb9180d776fe955a0f7e8017a1a1be55e9513adaff02655e',
                'tree_name': 'mainline.kernel.org',
                'valid': True
            },
            self.checkout._render()
        )


class TestBuild(unittest.TestCase):
    """Test Build methods."""

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(567899))
        g_job = objects.GitlabJob(g_project.jobs.get(841325))
        self.build = objects.Build(g_pipeline, g_job)
        self.build.upload_artifacts()

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841325',
             'gitlab_projects_2_jobs_841325_build.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/jobs',
             'gitlab_projects_2_pipelines_567899_jobs.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/rc',
             'gitlab_projects_2_jobs_841325_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/build%2Elog', None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/config', None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/kernel-mainline'
             '%2Ekernel%2Eorg-x86_64-decd6167bf4f6bec1284006d0522381b44660df3%2Etar%2Egz', None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/kernel-mainline'
             '%2Ekernel%2Eorg-x86_64-decd6167bf4f6bec1284006d0522381b44660df3%2Econfig', None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/tarball%2Efile',
             None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/selftests%2Efile',
             None),
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/artifacts/'
             'selftests%2Ebuildlog', None),
            ('http://patchwork/patch/2200409/raw/', 'patch one'),
            ('http://patchwork/patch/2200411/raw/', 'patch two'),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/841325/artifacts/raw/artifacts/'
             'kernel-mainline.kernel.org-x86_64-decd6167bf4f6bec1284006d0522381b44660df3.config',
             'foobar'),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/841325/artifacts/raw/artifacts/'
             'kernel-mainline.kernel.org-x86_64-decd6167bf4f6bec1284006d0522381b44660df3.tar.gz',
             'barfoo'),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/841325/artifacts/raw/'
             'artifacts/selftests.file', 'a'),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/841325/artifacts/raw/'
             'artifacts/selftests.buildlog', 'b'),
            ('https://a/kernel.tar.gz', None),
            ('https://a/selftests.file', None),
            ('https://a/selftests.buildlog', None),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    def test_artifacts_path(self):
        """Test artifacts path property."""
        self.mock_gitlab()
        self.assertEqual(f'{self.build.checkout.artifacts_path}/'
                         f'build_{self.build.architecture}_{self.build.id}',
                         self.build.artifacts_path)

    @responses.activate
    def test_checkout_id(self):
        """Test checkout_id."""
        self.mock_gitlab()
        self.assertEqual('redhat:567899', self.build.checkout.id)

    @responses.activate
    def test_id(self):
        """Test id."""
        self.mock_gitlab()
        self.assertEqual(f'redhat:{self.build.job.id}', self.build.id)

    @responses.activate
    def test_command(self):
        """Test command."""
        self.mock_gitlab()
        self.assertEqual(self.build.rc["build"]["command"], self.build.command)

    @responses.activate
    def test_log_url(self):
        """Test log_url."""
        self.mock_gitlab()
        self.assertEqual(
            f'endpoint/bucket/prefix/{self.build.artifacts_path}/build.log',
            self.build.log_url)

    @responses.activate
    def test_valid(self):
        """Test valid."""
        self.mock_gitlab()
        self.assertTrue(self.build.valid)

        self.build.rc['state']['stage_build'] = 'failed'
        self.assertFalse(self.build.valid)

    @responses.activate
    def test_config_url(self):
        """Test config_url."""
        self.mock_gitlab()
        self.assertEqual(
            f'endpoint/bucket/prefix/2020/05/14/567899/build_x86_64_{self.build.id}/'
            'kernel-mainline.kernel.org-x86_64-decd6167bf4f6bec1284006d0522381b44660df3.config',
            self.build.config_url)

    @responses.activate
    def test_input_files(self):
        """Test input_files."""
        self.mock_gitlab()
        self.assertEqual(None, self.build.input_files)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def test_output_files(self):
        """Test output_files."""
        self.mock_gitlab()
        self.build.rc['state']['kernel_package_url'] = 'https://a/kernel.tar.gz'
        self.build.rc['state']['selftests_url'] = 'https://a/selftests.file'
        self.build.rc['state']['selftests_buildlog_url'] = 'https://a/selftests.buildlog'
        self.build.output_files = []
        self.build.upload_artifacts()
        self.assertListEqual(
            [
                {'name': 'kernel.tar.gz',
                 'url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/kernel.tar.gz'},
                {'name': 'selftests.file',
                 'url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/selftests.file'},
                {'name': 'selftests.buildlog',
                 'url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/selftests.buildlog'},
            ],
            self.build.output_files
        )

        del self.build.rc['state']['kernel_package_url']
        del self.build.rc['state']['selftests_url']
        del self.build.rc['state']['selftests_buildlog_url']
        self.build.output_files = []
        self.build.upload_artifacts()
        self.assertEqual([], self.build.output_files)

    @responses.activate
    def test_render(self):
        """Test render."""
        self.mock_gitlab()

        self.assertDictEqual(
            {
                'checkout_id': self.build.checkout.id,
                'id': 'redhat:841325',
                'comment': None,
                'start_time': '2020-05-14T20:33:13.096123123Z',
                'duration': 520,
                'architecture': 'x86_64',
                'command': 'make -j30 INSTALL_MOD_STRIP=1 targz-pkg',
                'compiler': 'gcc (GCC) 10.1.1 20200507 (Red Hat 10.1.1-1)',
                'config_name': 'fedora',
                'config_url': (
                    f'endpoint/bucket/prefix/{self.build.artifacts_path}/'
                    'kernel-mainline.kernel.org-x86_64-'
                    'decd6167bf4f6bec1284006d0522381b44660df3.config'
                ),
                'input_files': None,
                'output_files': [
                    {'name': 'kernel-mainline.kernel.org-x86_64-'
                     'decd6167bf4f6bec1284006d0522381b44660df3.tar.gz',
                     'url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/kernel-mainline.'
                            'kernel.org-x86_64-decd6167bf4f6bec1284006d0522381b44660df3.tar.gz'},
                    {'name': 'selftests.file',
                     'url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/selftests.file'},
                    {'name': 'selftests.buildlog',
                     'url':
                     f'endpoint/bucket/prefix/{self.build.artifacts_path}/selftests.buildlog'},
                ],
                'log_url': f'endpoint/bucket/prefix/{self.build.artifacts_path}/build.log',
                'valid': True,
                'misc': {},
            },
            self.build._render()
        )

    @responses.activate
    def test_architecture(self):
        """Test architecture property."""
        self.mock_gitlab()
        self.assertEqual('x86_64', self.build.architecture)

        self.build.rc['state']['kernel_arch'] = 'i386'
        self.assertEqual('i386', self.build.architecture)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def test_reupload_file(self):
        """
        Test _reupload_file method.

        Test it here because Job class has no artifacts_path defined.
        """
        self.mock_gitlab()

        self.build.rc['state']['selftests_url'] = 'https://a/selftests.file'
        self.assertEqual(
            ('selftests.file',
             f'endpoint/bucket/prefix/{self.build.artifacts_path}/selftests.file'),
            self.build._reupload_file('selftests_url')
        )

        del self.build.rc['state']['selftests_url']
        self.assertEqual(
            (None, None),
            self.build._reupload_file('selftests_url')
        )


class TestTest(unittest.TestCase):
    """Test Test methods."""

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(567899))
        g_job = objects.GitlabJob(g_project.jobs.get(841327))

        self.tmpdir = tempfile.TemporaryDirectory()
        self.test_info = {
            'task_id': 109622842,
            'recipe_id': 1234,
            'CKI_UNIVERSAL_ID': 'selinux',
            'CKI_NAME': 'selinux-policy: serge-testsuite',
            'CKI_SELFTESTS_URL': '',
            'CKI_MAINTAINERS': 'Cosme Fulanito <cosme@fulanito.com> / gitlabusername',
            'arch': 'x86_64',
            'duration': '0:05:00',
            'finish_time': '2020-05-15 16:57:04',
            'kernel_version': '3.10.0-1137.el7.cki',
            'result': 'Pass',
            'skipped': False,
            'start_time': '2020-05-15 16:52:04',
            'status': 'Completed',
            'system': 'system.beaker.redhat.com',
            'fetch_url': 'http://test.url',
            'output_files': [
                {'name': 'x86_64_1_harness.log', 'path': 'test_some_stuff_0/x86_64_1_harness.log',
                 'url': f'{self.tmpdir}/recipes/test_some_stuff_0/x86_64_1_harness.log'},
                {'name': 'x86_64_1_sys.log', 'path': 'test_some_stuff_0/x86_64_1_sys.log',
                 'url': f'{self.tmpdir}/recipes/test_some_stuff_0/x86_64_1_sys.log'}
            ],
        }

        self.test = objects.Test(g_pipeline, g_job, self.test_info)
        self.test.upload_artifacts()

    def tearDown(self):
        self.tmpdir.cleanup()

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841325',
             'gitlab_projects_2_jobs_841325_build.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841327',
             'gitlab_projects_2_jobs_841327_test.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/jobs',
             'gitlab_projects_2_pipelines_567899_jobs.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/841325/artifacts/rc',
             'gitlab_projects_2_jobs_841325_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/841327/artifacts/rc',
             'gitlab_projects_2_jobs_841327_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/897068/artifacts/rc',
             'gitlab_projects_2_jobs_897068_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/841327/artifacts/targeted_tests%2Elist',
             'gitlab_projects_2_jobs_841327_targeted_tests.list'),
            ('http://patchwork/patch/2200409/raw/', 'patch one'),
            ('http://patchwork/patch/2200411/raw/', 'patch two'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    def test_build_from_another_pipeline(self):
        """Test finding a test's build using ARTIFACT_URL_{arch}."""
        self.mock_gitlab()
        json_mocks = [
            ('https://gitlab/api/v4/projects/2/pipelines/609216',
             'gitlab_projects_2_pipelines_609216.json'),
            ('https://gitlab/api/v4/projects/2/jobs/925897',
             'gitlab_projects_2_jobs_925897.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/609216/variables',
             'gitlab_projects_2_pipelines_609216_variables.json'),
            ('https://gitlab/api/v4/projects/2/jobs/924647',
             'gitlab_projects_2_jobs_924647.json'),
            ('https://gitlab/api/v4/projects/2/jobs/924646',
             'gitlab_projects_2_jobs_924646.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/609120',
             'gitlab_projects_2_pipelines_609120.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/925897/artifacts/rc',
             'gitlab_projects_2_jobs_925897_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/924647/artifacts/rc',
             'gitlab_projects_2_jobs_924647_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/924646/artifacts/rc',
             'gitlab_projects_2_jobs_924646_rc'),
        ]
        mock_gitlab(body_mocks, is_json=False)

        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(609216))
        g_job = objects.GitlabJob(g_project.jobs.get(925897))

        test = objects.Test(g_pipeline, g_job, self.test_info)

        self.assertEqual('redhat:924647', test.build.id)

        # Debug test should look for debug build.
        del test.build
        test.rc_state['debug_kernel'] = 'true'
        self.assertEqual('redhat:924646', test.build.id)

    @responses.activate
    def test_artifacts_path(self):
        """Test artifacts path property."""
        self.mock_gitlab()
        self.assertEqual(f'{self.test.build.artifacts_path}/tests',
                         self.test.artifacts_path)

    @responses.activate
    def test_build_id(self):
        """Test build id."""
        self.mock_gitlab()
        self.assertEqual('redhat:841325',
                         self.test.build.id)

    @responses.activate
    def test_id(self):
        """Test test id."""
        self.mock_gitlab()
        self.assertEqual('redhat:109622842',
                         self.test.id)

    def test_environment(self):
        """Test environment."""
        self.assertEqual({'comment': 'system.beaker.redhat.com'},
                         self.test.environment)

    def test_status(self):
        """Test status."""
        # This is probably gonna be huge. Or a sepparated module.
        # not implemented

    @responses.activate
    def test_targeted(self):
        """Test targeted property."""
        self.mock_gitlab()
        # No targeted_tests in the pipeline.
        self.test.rc['state']['targeted_tests'] = 0
        self.assertFalse(self.test.targeted)

        # There are targeted_tests in the pipeline.
        self.test.rc['state']['targeted_tests'] = True

        # But this test is not on the list.
        self.test.job.get_artifact.cache_clear()
        self.test.rc['state']['targeted_tests_list'] = 'targeted'
        responses.add(
            responses.GET,
            'https://gitlab/api/v4/projects/2/jobs/841327/artifacts/targeted',
            body=b'test 1\ntest 2\n',
        )
        self.assertFalse(self.test.targeted)

        # Now this test name *IS* on the list of targeted tests.
        self.test.job.get_artifact.cache_clear()
        responses.replace(
            responses.GET,
            'https://gitlab/api/v4/projects/2/jobs/841327/artifacts/targeted',
            body=f'test 1\ntest 2\n{self.test.test_info["CKI_NAME"]}\ntest 3',
        )
        self.assertTrue(self.test.targeted)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.tarfile', unittest.mock.MagicMock())
    @unittest.mock.patch('cki.kcidb.utils.os.walk', return_value=[('something', 'something',
                                                                   ['file'])])
    @unittest.mock.patch('cki.kcidb.utils.ThreadPoolExecutor')
    def test_upload_coverage(self, mock_thread, _):
        """Ensure upload_coverage is called with expected params."""
        executor = unittest.mock.Mock()
        mock_thread.return_value.__enter__.return_value = executor

        self.mock_gitlab()
        self.test.upload_coverage({'url': f'endpoint/bucket/prefix/{self.test.artifacts_path}/',
                                   'name': 'something.tgz'})

        executor.submit.assert_called_with(utils.upload_file, 'private',
                                           '2020/05/14/567899/build_x86_64_redhat:841325/tests',
                                           '', source_path='something/file')

    @responses.activate
    def test_output_files(self):
        """Test output_files."""
        self.mock_gitlab()
        self.assertEqual(
            [{'url': f'endpoint/bucket/prefix/{self.test.artifacts_path}/'
              'test_some_stuff_0/x86_64_1_harness.log',
              'name': 'x86_64_1_harness.log'}],
            self.test.output_files)

    @responses.activate
    def test_misc(self):
        """Test misc."""
        self.mock_gitlab()
        self.assertDictEqual(
            {
                'debug': False,
                'targeted': True,
                'fetch_url': 'http://test.url',
                'beaker': {
                    'recipe_id': 1234,
                    'task_id': 109622842,
                    'finish_time': '2020-05-15T16:57:04+00:00',
                    'retcode': 0,
                },
                'maintainers': [
                    {
                        'name': 'Cosme Fulanito',
                        'email': 'cosme@fulanito.com',
                        'gitlab': 'gitlabusername'
                    }
                ]
            },
            self.test.misc)

    def test_waived(self):
        """Test waived."""
        self.test.test_info['CKI_WAIVED'] = 'true'
        self.assertTrue(self.test.waived)

        # Just the key being present is enough to consider it as waived.
        self.test.test_info['CKI_WAIVED'] = ''
        self.assertTrue(self.test.waived)

        del self.test.test_info['CKI_WAIVED']
        self.assertFalse(self.test.waived)

    @responses.activate
    def test_render(self):
        """Test render."""
        self.mock_gitlab()
        self.assertDictEqual(
            {
                'build_id': self.test.build.id,
                'id': 'redhat:109622842',
                'environment': {'comment': 'system.beaker.redhat.com'},
                'path': 'selinux',
                'comment': 'selinux-policy: serge-testsuite',
                'status': 'PASS',
                'waived': False,
                'start_time': '2020-05-15T16:52:04+00:00',
                'duration': 300,
                'output_files': [
                    {'url': (f'endpoint/bucket/prefix/{self.test.artifacts_path}/'
                             'test_some_stuff_0/x86_64_1_harness.log'),
                     'name': 'x86_64_1_harness.log'},
                ],
                'misc': {
                    'debug': False,
                    'targeted': True,
                    'fetch_url': 'http://test.url',
                    'beaker': {
                        'recipe_id': 1234,
                        'task_id': 109622842,
                        'finish_time': '2020-05-15T16:57:04+00:00',
                        'retcode': 0,
                    },
                    'maintainers': [
                        {
                            'name': 'Cosme Fulanito',
                            'email': 'cosme@fulanito.com',
                            'gitlab': 'gitlabusername'
                        }
                    ]
                },
            },
            self.test._render()
        )

    def test_should_ignore_file(self):
        """Test _should_ignore_file method."""
        test_cases = [
            ({'name': 'foo_bar_anaconda.log'}, True),
            ({'name': 'foo_bar/sys.log'}, True),
            ({'name': 'foo_bar/DeBug_abc123.log'}, True),
            ({'name': 'foo_bar/debug.log'}, False),
            ({'name': 'foo_bar_anaconda_2.log'}, False),
            ({'name': 'console.log'}, False),
        ]

        # Ensure expected file names used in test_cases are in output_files_ignore
        self.assertIn('anaconda.log', objects.Test.output_files_ignore)
        self.assertIn('sys.log', objects.Test.output_files_ignore)
        self.assertIn('DeBug', objects.Test.output_files_ignore)

        for file, expected in test_cases:
            self.assertEqual(
                expected, objects.Test._should_ignore_file(file)
            )


class TestBrewTest(unittest.TestCase):
    """Test the Brew test class."""
    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(16)
        self.g_pipeline = objects.GitlabPipeline(
            g_project.pipelines.get(596980))
        self.g_job = objects.GitlabJob(g_project.jobs.get(897035))
        self.tmpdir = tempfile.TemporaryDirectory()
        self.test_info = {
            'task_id': 109622842,
            'recipe_id': 1234,
            'CKI_UNIVERSAL_ID': 'selinux',
            'CKI_NAME': 'selinux-policy: serge-testsuite',
            'CKI_SELFTESTS_URL': '',
            'CKI_MAINTAINERS': 'Cosme Fulanito <cosme@fulanito.com> / gitlabusername',
            'arch': 'x86_64',
            'duration': '0:05:00',
            'finish_time': '2020-05-15 16:57:04',
            'kernel_version': '3.10.0-1137.el7.cki',
            'result': 'Pass',
            'skipped': False,
            'start_time': '2020-05-15 16:52:04',
            'status': 'Completed',
            'system': 'system.beaker.redhat.com',
            'fetch_url': 'http://test.url',
            'output_files': [
                {'name': 'x86_64_1_harness.log', 'path': 'test_some_stuff_0/x86_64_1_harness.log',
                 'url': f'{self.tmpdir}/recipes/test_some_stuff_0/x86_64_1_harness.log'},
            ],
        }

    def tearDown(self):
        self.tmpdir.cleanup()

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/16',
             'gitlab_projects_16.json'),
            ('https://gitlab/api/v4/projects/16/pipelines/596980',
             'gitlab_projects_16_pipeline_596980.json'),
            ('https://gitlab/api/v4/projects/16/pipelines/596980/variables',
             'gitlab_projects_16_pipeline_596980_variables.json'),
            ('https://gitlab/api/v4/projects/16/jobs/897035',
             'gitlab_projects_16_job_897035.json'),
            ('https://copr.fedoraproject.org/api_2/build_tasks/1234/kernel-rt-rhel8-x86_64',
             'copr_1234.json')
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/16/jobs/897035/artifacts/rc',
             'gitlab_projects_16_job_897035_rc'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': get_mocked_bucket()})
    def test_render(self):
        """Test the object rendering."""
        self.mock_gitlab()

        with unittest.mock.patch('cki.kcidb.objects.BrewJob.brew_task',
                                 new_callable=unittest.mock.PropertyMock) as mock_task:
            mock_task.return_value = {
                'completion_time': '2020-06-09 07:50:39.459602',
                'start_time': '2020-06-09 06:30:41.372504',
                'start_ts': 1591684241.3725,
                'create_time': '2020-06-09 06:28:44.359499',
                'create_ts': 1591684124.3595,
                'owner': 2601,
                'method': 'build',
                'completion_ts': 1591689039.4596,
                'id': 29202630
            }

            checkout = objects.BrewCheckout(
                self.g_pipeline, self.g_job).render()
            build = objects.BrewBuild(self.g_pipeline, self.g_job).render()
            test_object = objects.BrewTest(
                self.g_pipeline, self.g_job, self.test_info)
            test_object.upload_artifacts()
            test = test_object.render()

        job_data = {
            'commit_message_title': None,
            'created_at': '2020-06-09T07:53:05.474Z',
            'duration': 25141.5273,
            'finished_at': '2020-06-09T14:56:23.497Z',
            'id': 897035,
            'kernel_version': '4.0.0.test.cki.kt1.src.rpm',
            'name': 'test x86_64',
            'stage': 'test',
            'started_at': '2020-06-09T07:57:21.969Z',
            'tag': None,
            'test_hash': None
        }
        pipeline_data = {
            'created_at': '2020-06-09T07:53:05.328Z',
            'duration': 25389,
            'finished_at': '2020-06-09T14:56:23.583Z',
            'id': 596980,
            'project': {
                'id': 16,
                'path_with_namespace': 'cki-project/brew-pipeline',
                'instance_url': 'https://gitlab'
            },
            'ref': 'kernel-rt-rhel8',
            'sha': '93a8b317b8eec6cc87c896ce56457824c9ef231c',
            'started_at': '2020-06-09T07:53:08.651Z',
            'variables': {
                'architectures': 'x86_64',
                'brew_task_id': '29202630',
                'nvr': 'kernel-rt-4.0.0-193.8.1.rt13.59.el8_2.dyntick.test.cki.kt1.src.rpm',
                'submitter': 'somebody@redhat.com',
                'server_url': 'https://brewhub.engineering.redhat.com/brewhub',
                'name': 'kernel-rt-rhel8',
            }
        }
        self.assertDictEqual(
            {
                'id': 'redhat:596980',
                'git_commit_hash': '7a04f0ddf80981451c8f4e3acaae74cc6689726f',
                'origin': 'redhat',
                'valid': True,
                'tree_name': 'kernel-rt-rhel8',
                'contacts': ['somebody@redhat.com'],
                'start_time': '2020-06-09T07:53:05.328Z',
                'misc': {'job': job_data, 'pipeline': pipeline_data}
            },
            checkout
        )

        self.assertDictEqual(
            {
                'architecture': 'x86_64',
                'duration': 4798,
                'id': 'redhat:29202630_x86_64',
                'origin': 'redhat',
                'checkout_id': 'redhat:596980',
                'start_time': '2020-06-09T06:30:41.372504+00:00',
                'valid': True,
                'misc': {'job': job_data, 'pipeline': pipeline_data}
            },
            build
        )

        self.assertDictEqual(
            {
                'build_id': 'redhat:29202630_x86_64',
                'id': 'redhat:109622842',
                'origin': 'redhat',
                'comment': 'selinux-policy: serge-testsuite',
                'duration': 300,
                'environment': {'comment': 'system.beaker.redhat.com'},
                'output_files': [
                    {'url': 'endpoint/bucket/prefix/2020/06/09/596980/x86_64/'
                     'test_some_stuff_0/x86_64_1_harness.log',
                     'name': 'x86_64_1_harness.log'},
                ],
                'path': 'selinux',
                'start_time': '2020-05-15T16:52:04+00:00',
                'status': 'PASS',
                'waived': False,
                'misc': {
                    'debug': 0,
                    'targeted': False,
                    'fetch_url': 'http://test.url',
                    'beaker': {
                        'finish_time': '2020-05-15T16:57:04+00:00',
                        'recipe_id': 1234,
                        'retcode': 0,
                        'task_id': 109622842
                    },
                    'job': job_data,
                    'pipeline': pipeline_data,
                    'maintainers': [
                        {
                            'name': 'Cosme Fulanito',
                            'email': 'cosme@fulanito.com',
                            'gitlab': 'gitlabusername'
                        }
                    ]
                },
            },
            test
        )

    @responses.activate
    @unittest.mock.patch('cki.kcidb.objects.COPR_SERVER', 'https://copr.fedoraproject.org')
    def test_checkout_id_copr(self):
        """Test the checkout_id without a brew task."""
        self.mock_gitlab()

        # Turn job into COPR
        del self.g_pipeline.variables['brew_task_id']
        self.g_pipeline.variables['copr_build'] = 1234

        checkout = objects.BrewCheckout(
            self.g_pipeline, self.g_job).render()

        # hashlib.sha1(b'1234').hexdigest() == '7110eda4d09e062aa5e4a390b0a572ac0d2c0220'
        self.assertEqual(checkout['git_commit_hash'], '7110eda4d09e062aa5e4a390b0a572ac0d2c0220')
        self.assertEqual(checkout['id'], 'redhat:596980')

    @responses.activate
    @unittest.mock.patch('cki.kcidb.objects.COPR_SERVER', 'https://copr.fedoraproject.org')
    def test_build_times_copr(self):
        """Test the start and duration times without a brew task."""
        self.mock_gitlab()

        # Turn job into COPR
        del self.g_pipeline.variables['brew_task_id']
        self.g_pipeline.variables['copr_build'] = 1234

        build = objects.BrewBuild(
            self.g_pipeline, self.g_job).render()

        self.assertEqual(build['start_time'], '2021-01-02T16:00:53+00:00')
        self.assertEqual(build['duration'], 8494)


class TestUMBTest(unittest.TestCase):
    """Test the UMB test class."""
    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        self.g_pipeline = objects.GitlabPipeline(
            g_project.pipelines.get(597354))
        self.g_job = objects.GitlabJob(g_project.jobs.get(897873))

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/597354',
             'gitlab_projects_2_pipelines_597354.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/597007',
             'gitlab_projects_2_pipelines_597007.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/597007/jobs',
             'gitlab_projects_2_pipelines_597007_jobs.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/597354/variables',
             'gitlab_projects_2_pipelines_597354_variables.json'),
            ('https://gitlab/api/v4/projects/2/jobs/897873',
             'gitlab_projects_2_jobs_897873_umb.json'),
            ('https://gitlab/api/v4/projects/2/jobs/897068',
             'gitlab_projects_2_jobs_897068_build.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/597007/variables',
             'gitlab_projects_2_pipelines_597007_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/897068/artifacts/rc',
             'gitlab_projects_2_jobs_897068_rc'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    def test_render(self):
        """Test the object rendering."""
        self.mock_gitlab()
        data = {
            'test_name': 'SimpleNetworkRecipeMultiLog',
            'test_description': 'Network performance measurement of {udp_stream} '
                                'x {ipv6} for SimpleNetworkRecipe scenario',
            'test_result': 'FAIL',
            'test_log_url': ['http://server/one.log', 'http://server/another.log'],
            'test_arch': 'x86',
            'test_waived': 'True',
            'test_index': 123,
        }

        test = objects.UMBTest(self.g_pipeline, self.g_job, data)
        self.assertDictEqual(
            {
                'build_id': 'redhat:897068',
                'id': 'redhat:597354_123',
                'comment': 'Network performance measurement of {udp_stream} x {ipv6} for '
                           'SimpleNetworkRecipe scenario',
                'output_files': [
                    {'url': 'http://server/one.log', 'name': 'one.log'},
                    {'url': 'http://server/another.log', 'name': 'another.log'},
                ],
                'path': 'SimpleNetworkRecipeMultiLog',
                'status': 'FAIL',
                'waived': True,
                'start_time': '2020-06-09T08:30:09.453Z',
                'misc': {
                    'debug': False,
                    'maintainers': [{'name': 'LNST', 'email': 'team@redhat.com'}],
                }
            },
            test._render()
        )


class TestKSelfTest(unittest.TestCase):
    """Test the kselftest test class."""
    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        self.g_pipeline = objects.GitlabPipeline(
            g_project.pipelines.get(616568))
        self.g_job = objects.GitlabJob(g_project.jobs.get(1012267))
        self.build = objects.Build(self.g_pipeline, self.g_job)

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/616568',
             'gitlab_projects_2_pipelines_616568.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/616568/jobs',
             'gitlab_projects_2_pipelines_616568_jobs.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/616568/variables',
             'gitlab_projects_2_pipelines_616568_variables.json'),
            ('https://gitlab/api/v4/projects/2/jobs/1012266',
             'gitlab_projects_2_jobs_1012266_merge.json'),
            ('https://gitlab/api/v4/projects/2/jobs/1012267',
             'gitlab_projects_2_jobs_1012267_build.json'),
            ('https://gitlab/api/v4/projects/2/jobs/1012270',
             'gitlab_projects_2_jobs_1012270_publish.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/1012267/artifacts/rc',
             'gitlab_projects_2_jobs_1012267_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/1012270/artifacts/rc',
             'gitlab_projects_2_jobs_1012270_rc'),
            ('https://gitlab/api/v4/projects/2/jobs/1012267/'
             'artifacts/artifacts/build-selftests%2Eyaml',
             'gitlab_projects_2_jobs_1012267_artifacts_build-selftests.yaml'),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/1012267/'
             'artifacts/raw/artifacts/build-selftests.log',
             None),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/1012267/'
             'artifacts/raw/artifacts/kernel.config',
             None),
            ('https://gitlab/api/v4/projects/2/jobs/1012267/artifacts/'
             'artifacts/build%2Elog',
             None),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/1012267/'
             'artifacts/raw/artifacts/kernel.tar.gz',
             None),
            ('https://gitlab/cki-project/cki-pipeline/-/jobs/1012267/'
             'artifacts/raw/artifacts/kselftest.tar.gz',
             None),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'public': get_mocked_bucket()})
    def test_output_files(self):
        """Test the output files."""
        self.mock_gitlab()
        test = self.build.selftests[0]
        self.build.upload_artifacts()

        self.assertDictEqual(
            {
                'build_id': 'redhat:1012267',
                'id': 'redhat:1012267_0',
                'comment': "kselftest build - test_123",
                'output_files': [
                    {'name': 'build-selftests.log',
                     'url':
                     f'endpoint/bucket/prefix/{self.build.artifacts_path}/build-selftests.log'},
                ],
                'path': "kselftest-build.test_123",
                'start_time': '2020-11-02T13:17:23.659020889Z',
                'status': 'FAIL',
                'waived': False,
            },
            test._render()
        )

    @responses.activate
    def test_render(self):
        """Test the object rendering."""
        self.mock_gitlab()
        test_name = "123 test"
        data = {
            "CKI_NAME": f"kselftest build - {test_name}",
            "CKI_UNIVERSAL_ID": f"kselftest-build.{test_name.replace(' ', '_')}",
            "return_code": 0,
            "test_index": 1
        }

        test = objects.KSelfTest(self.g_pipeline, self.g_job, data)
        self.assertDictEqual(
            {
                'build_id': 'redhat:1012267',
                'id': 'redhat:1012267_1',
                'comment': data['CKI_NAME'],
                'output_files': [],
                'path': data['CKI_UNIVERSAL_ID'],
                'start_time': '2020-11-02T13:17:23.659020889Z',
                'status': 'PASS',
                'waived': False,
            },
            test._render()
        )
