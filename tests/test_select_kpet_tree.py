"""Tests for kpet tree selection."""
import contextlib
import io
import unittest

from cki.cki_tools import select_kpet_tree


class TestSelectKpetTree(unittest.TestCase):
    """Tests for select_kpet_tree module."""

    def _test(self, family, nvr, expected):
        stdout = io.StringIO()
        with contextlib.redirect_stdout(stdout):
            if isinstance(expected, Exception):
                self.assertRaises(Exception, select_kpet_tree.main, [family, nvr])
            else:
                select_kpet_tree.main([family, nvr])
                self.assertEqual(stdout.getvalue().strip(), expected)

    def test_main(self):
        """Check that the main function works correctly."""
        self._test('rhel7', '3.10.0-1160.11.1.el7.test', 'rhel79-z')

    def test_strip_kernel_prefix(self):
        """Check that kernel- prefix is stripped."""
        self._test('rhel7', 'kernel-3.10.0-1160.11.1.el7.test', 'rhel79-z')

    def test_strip_kernel_rt_prefix(self):
        """Check that kernel-rt- prefix is stripped."""
        self._test('rhel7', 'kernel-rt-3.10.0-1160.11.1.el7.test', 'rhel79-z')

    def test_unknown_family_passhrough(self):
        """Check that unknown families are passed through."""
        self._test('unknown', 'foobar-3.10.0-1160.11.1.el7.test', 'unknown')

    def test_rhel5(self):
        """Check that rhel5 nvrs are correctly handled."""
        self._test('rhel5', '2.6.18-500.el5', 'rhel5-els')
        self._test('rhel5', '2.6.18-500.el6', Exception())

    def test_rhel6(self):
        """Check that rhel6 nvrs are correctly handled."""
        self._test('rhel6', '2.6.32-754.500.500.el6', 'rhel610-z')
        self._test('rhel6', '2.6.32-0.el7', Exception())

    def test_rhel7(self):
        """Check that rhel7 nvrs are correctly handled."""
        self._test('rhel7', '3.10.0-327.500.500.el7', 'rhel72-z')
        self._test('rhel7', '3.10.0-514.500.500.el7', 'rhel73-z')
        self._test('rhel7', '3.10.0-693.500.500.el7', 'rhel74-z')
        self._test('rhel7', '3.10.0-957.500.500.el7', 'rhel76-z')
        self._test('rhel7', '3.10.0-1062.500.500.el7', 'rhel77-z')
        self._test('rhel7', '3.10.0-1127.500.500.el7', 'rhel78-z')
        self._test('rhel7', '3.10.0-1160.500.500.el7', 'rhel79-z')
        self._test('rhel7', '3.10.0-1161.el7', Exception())
        self._test('rhel7', '3.10.0-0.el8', Exception())
        self._test('rhel7', '3.10.1-0.el7', Exception())

    def test_rhel7_rt(self):
        """Check that rhel7-rt nvrs are correctly handled."""
        self._test('rhel7-rt', '3.10.0-327.500.500.el7', Exception())
        self._test('rhel7-rt', '3.10.0-514.500.500.el7', Exception())
        self._test('rhel7-rt', '3.10.0-693.500.500.el7', Exception())
        self._test('rhel7-rt', '3.10.0-957.500.500.el7', 'rhel76-z-rt')
        self._test('rhel7-rt', '3.10.0-1062.500.500.el7', Exception())
        self._test('rhel7-rt', '3.10.0-1127.500.500.el7', Exception())
        self._test('rhel7-rt', '3.10.0-1160.500.500.el7', 'rhel79-z-rt')
        self._test('rhel7-rt', '3.10.0-1161.el7', Exception())
        self._test('rhel7-rt', '3.10.0-0.el8', Exception())
        self._test('rhel7-rt', '3.10.1-0.el7', Exception())

    def test_rhel8(self):
        """Check that rhel8 nvrs are correctly handled."""
        self._test('rhel8', '4.18.0-80.500.500.el8_0', 'rhel80-z')
        self._test('rhel8', '4.18.0-147.500.500.el8_1', 'rhel81-z')
        self._test('rhel8', '4.18.0-193.500.500.el8_2', 'rhel82-z')
        self._test('rhel8', '4.18.0-240.500.500.el8_3', 'rhel83-z')
        self._test('rhel8', '4.18.0-500.500.500.el8_4', 'rhel84-z')
        self._test('rhel8', '4.18.0-1.500.500.el8', 'rhel8')
        self._test('rhel8', '4.18.0-0.el9', Exception())

    def test_rhel8_rt(self):
        """Check that rhel8-rt nvrs are correctly handled."""
        self._test('rhel8-rt', '4.18.0-80.500.500.el8_0', 'rhel80-z-rt')
        self._test('rhel8-rt', '4.18.0-147.500.500.el8_1', 'rhel81-z-rt')
        self._test('rhel8-rt', '4.18.0-193.500.500.el8_2', 'rhel82-z-rt')
        self._test('rhel8-rt', '4.18.0-240.500.500.el8_3', 'rhel83-z-rt')
        self._test('rhel8-rt', '4.18.0-500.500.500.el8_4', 'rhel84-z-rt')
        self._test('rhel8-rt', '4.18.0-1.500.500.el8', 'rhel8-rt')
        self._test('rhel8-rt', '4.18.0-0.el9', Exception())

    def test_rhel9(self):
        """Check that rhel9 nvrs are correctly handled."""
        self._test('rhel9', '5.9.0-500.500.500.el9_0', 'rhel9')  # until 9.0 zstream exists
        self._test('rhel9', '5.9.0-1.500.500.el9', 'rhel9')
        self._test('rhel9', '5.9.0-0.el10', Exception())

    def test_rhel9_rt(self):
        """Check that rhel9-rt nvrs are correctly handled."""
        self._test('rhel9-rt', '5.9.0-500.500.500.el9_0', 'rhel9-rt')  # until 9.0 zstream exists
        self._test('rhel9-rt', '5.9.0-1.500.500.el9', 'rhel9-rt')
        self._test('rhel9-rt', '5.9.0-0.el10', Exception())
