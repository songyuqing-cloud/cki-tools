"""Mock helpers."""
# pylint: disable=no-member
import json
import os
import pathlib
import unittest

import responses

from cki.cki_tools import _utils

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')


def mock_gitlab(mocks, is_json=True):
    """Given a list of mocks, generate responses."""
    for url, file_name in mocks:
        if file_name:
            try:
                body = pathlib.Path(ASSETS_DIR, file_name).read_text()
            except FileNotFoundError:
                body = file_name
        else:
            body = 'dummy content'

        responses.add(
            responses.GET,
            url,
            json=json.loads(body) if is_json else None,
            body=body if not is_json else None,
        )


def get_mocked_bucket():
    """Get mocked S3Bucket."""
    mocked_bucket_spec = _utils.BucketSpec('endpoint', 'access_key',
                                           'secret_key', 'bucket', 'prefix')
    bucket = _utils.S3Bucket(mocked_bucket_spec)
    bucket.client = unittest.mock.Mock()
    return bucket
