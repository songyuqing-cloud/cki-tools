"""Fake Gitlab classes."""
from unittest import mock


class FakeGitLabCommits():
    """Fake commit."""

    def __init__(self):
        """Initialize."""
        self._commits = []

    def create(self, data):
        """Create a commit."""
        self._commits.append(data)

    def __getitem__(self, index):
        """Get a commit."""
        return self._commits[index]


class FakePipelineJob():
    # pylint: disable=too-few-public-methods
    """Fake pipeline job."""

    def __init__(self, id, name, stage, status):
        """Initialize."""
        self.id = id
        self.name = name
        self.stage = stage
        self.status = status


class FakePipelineVariables():
    # pylint: disable=too-few-public-methods
    """Fake pipeline variables."""

    def __init__(self, variables):
        """Initialize."""
        self._variables = variables

    @staticmethod
    def _as_key_value(variables):
        return [mock.MagicMock(key=key, value=value)
                for key, value in variables.items()]

    def list(self, **kwargs):
        """List all variables."""
        kwargs.pop('as_list', None)
        kwargs.pop('all', None)
        if not kwargs:
            return self._as_key_value(self._variables)

        raise NotImplementedError


class FakePipeline():
    # pylint: disable=too-few-public-methods
    """Fake pipeline."""

    pipeline_counter = 0

    def __init__(self, branch, token, variables, status):
        """Initialize."""
        self.jobs = []
        self.attributes = {'status': status}
        for key, value in variables.items():
            self.attributes[key] = value

        self.branch = branch
        self.token = token
        self.variables = FakePipelineVariables(variables)
        self.id = FakePipeline.pipeline_counter  # pylint: disable=invalid-name
        FakePipeline.pipeline_counter += 1

    def add_job(self, id, name, stage, status):
        """Add a job."""
        self.jobs.append(FakePipelineJob(id, name, stage, status))


class FakeProjectPipelines():
    """Fake project pipelines."""

    def __init__(self):
        """Initialize."""
        self._pipelines = []

    def add_new_pipeline(self, branch, token, variables, status):
        """Add a new pipeline."""
        self._pipelines.append(
            FakePipeline(branch, token, variables, status)
        )

    def __getitem__(self, index):
        """Get a pipeline."""
        return self._pipelines[index]

    def list(self, **kwargs):
        """List all pipelines."""
        kwargs.pop('as_list', None)
        if not kwargs:
            return self._pipelines

        to_return = []
        for pipeline in self._pipelines:
            if pipeline.branch == kwargs.get('ref'):
                to_return.append(pipeline)
            elif pipeline.attributes['status'] == kwargs.get('status'):
                to_return.append(pipeline)

        return to_return


class FakeGitLabProject():
    # pylint: disable=too-few-public-methods
    """Fake project."""

    def __init__(self):
        """Initialize."""
        self.commits = FakeGitLabCommits()
        self.attributes = {'web_url': 'http://web-url'}
        self.pipelines = FakeProjectPipelines()
        self.manager = mock.MagicMock()

    def trigger_pipeline(self, branch, token, variables, status='pending'):
        """Trigger a pipeline."""
        self.pipelines.add_new_pipeline(branch, token, variables, status)


class FakeGitLab():
    # pylint: disable=too-few-public-methods
    """Fake gitlab."""

    def __init__(self):
        """Initialize."""
        self.projects = {}

    def add_project(self, project_name):
        """Add a project."""
        self.projects[project_name] = FakeGitLabProject()
