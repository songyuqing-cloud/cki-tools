"""KCIDB objects tests."""
# pylint: disable=no-member
import unittest

from botocore.exceptions import ClientError
import gitlab
import responses

from cki.kcidb import objects
from cki.kcidb.utils import is_empty
from cki.kcidb.utils import timestamp_to_iso
from cki.kcidb.utils import tzaware_str2utc
from cki.kcidb.utils import upload_file
from tests.mocks import get_mocked_bucket
from tests.mocks import mock_gitlab


class TestUtils(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def setUp(self):
        self.gitlab_url = 'https://gitlab'
        self.gitlab_token = 'token'
        self.gitlab = gitlab.Gitlab(self.gitlab_url, self.gitlab_token)

        self.mock_gitlab()
        g_project = self.gitlab.projects.get(2)
        g_pipeline = objects.GitlabPipeline(g_project.pipelines.get(567899))
        g_job = objects.GitlabJob(g_project.jobs.get(841324))
        self.job = objects.Job(g_pipeline, g_job)

    @staticmethod
    def mock_gitlab():
        """Mock gitlab API responses."""
        json_mocks = [
            ('https://gitlab/api/v4/projects/2',
             'gitlab_projects_2.json'),
            ('https://gitlab/api/v4/projects/2/jobs/841324',
             'gitlab_projects_2_jobs_841324_merge.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899',
             'gitlab_projects_2_pipelines_567899.json'),
            ('https://gitlab/api/v4/projects/2/pipelines/567899/variables',
             'gitlab_projects_2_pipelines_567899_variables.json'),
        ]
        mock_gitlab(json_mocks)

        body_mocks = [
            ('https://gitlab/api/v4/projects/2/jobs/841324/artifacts/rc',
             'gitlab_projects_2_jobs_841324_rc'),
        ]
        mock_gitlab(body_mocks, is_json=False)

    @responses.activate
    def test_upload_empty(self):
        """Test upload_file with empty content and no source_path."""
        self.mock_gitlab()
        self.assertIsNone(self.job.upload_file('file.name'))

    @responses.activate
    @unittest.mock.patch('cki.kcidb.objects.Job.artifacts_path',
                         new_callable=unittest.mock.PropertyMock)
    def test_upload_file_content(self, artifacts_path_mock):
        """
        Test upload_file method via file_content.

        File is not in remote. Uploading from file_content.
        """
        self.mock_gitlab()
        artifacts_path_mock.return_value = 'path-mock'
        mocked_bucket = get_mocked_bucket()

        with unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': mocked_bucket}):
            mocked_bucket.client.head_object.side_effect = ClientError({'Error': {'Code': '404'}},
                                                                       '')

            result_path = self.job.upload_file('file.name', file_content='content')
            self.assertEqual('endpoint/bucket/prefix/path-mock/file.name', result_path)

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            mocked_bucket.client.put_object.assert_called_with(
                Body='content', Bucket='bucket',
                ContentType='text/plain', Key='prefix/path-mock/file.name')

    @unittest.mock.patch('cki.kcidb.objects.Job.artifacts_path',
                         new_callable=unittest.mock.PropertyMock)
    @unittest.mock.patch('cki.kcidb.objects.GitlabPipeline.variables',
                         new={'artifact_visibility': 'private'})
    def test_upload_file_source_path(self, artifacts_path_mock):
        """
        Test upload_file method via source_path.

        File is not in remote. Uploading from source_path.
        """
        artifacts_path_mock.return_value = 'path-mock'
        mocked_bucket = get_mocked_bucket()
        mocked_bucket.client.head_object.side_effect = ClientError({'Error': {'Code': '404'}}, '')
        with unittest.mock.patch('cki.kcidb.utils.BUCKETS',
                                 {'private': mocked_bucket}):

            self.job.upload_file('file.name', source_path='/file/path')

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            mocked_bucket.client.upload_file.assert_called_with(
                Bucket='bucket', Filename='/file/path',
                Key='prefix/path-mock/file.name', ExtraArgs={'ContentType': 'text/plain'})

    @unittest.mock.patch('cki.kcidb.objects.Job.artifacts_path',
                         new_callable=unittest.mock.PropertyMock)
    @unittest.mock.patch('cki.kcidb.objects.GitlabPipeline.variables',
                         new={'artifact_visibility': 'private'})
    def test_upload_file_in_remote(self, artifacts_path_mock):
        """Test upload_file method. File is in remote."""
        artifacts_path_mock.return_value = 'path-mock'
        mocked_bucket = get_mocked_bucket()
        with unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': mocked_bucket}):

            self.job.upload_file('file.name', source_path='/file/path')

            mocked_bucket.client.head_object.assert_called_with(
                Bucket='bucket', Key='prefix/path-mock/file.name')
            self.assertFalse(mocked_bucket.client.put_object.called)
            self.assertFalse(mocked_bucket.client.upload_file.called)

    @unittest.mock.patch('cki.kcidb.utils.BUCKETS', {'private': unittest.mock.Mock()})
    @unittest.mock.patch('cki.kcidb.utils.LOGGER.warning')
    def test_upload_file_fail(self, mock_warning):
        """Ensure upload_file sanitizes its arguments."""
        result = upload_file('private', 'path', None, None)
        self.assertIsNone(result)
        mock_warning.assert_called_with('File "%s" is empty', None)

    def test_is_empty(self):
        """Test is_empty method."""
        test_cases = [
            (None, True),
            ('', False),
            ('something', False),
            ([], True),
            ([''], False),
            ({}, True),
            ({'a': 'b'}, False),
            (False, False),
        ]

        for case, expected in test_cases:
            self.assertEqual(expected, is_empty(
                case), (case, expected))

    def test_timestamp_as_iso(self):
        """Test timestamp_to_iso."""
        test_cases = [
            ('2020-05-18', '2020-05-18T00:00:00+00:00'),
            ('2020-05-18 17:33:59', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18T17:33:59Z', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18 17:33:59 +2', '2020-05-18T17:33:59+02:00'),
            ('2020-05-18T17:33:59+00:00', '2020-05-18T17:33:59+00:00'),
            ('2020-05-18T17:33:59+01:00', '2020-05-18T17:33:59+01:00'),
        ]

        for timestamp, expected in test_cases:
            self.assertEqual(expected, timestamp_to_iso(timestamp))

    def test_tzaware_str2utc(self):
        test_cases = (
            ('2021-04-13T14:33:08-04:00', '2021-04-13T18:33:08+00:00'),
            ('2021-04-13 10:30:07 +0000', '2021-04-13T10:30:07+00:00'),
        )

        for tzaware_str, expected in test_cases:
            self.assertEqual(expected, tzaware_str2utc(tzaware_str))
