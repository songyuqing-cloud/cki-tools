"""Tests for cki_tools.k8s_event_listener."""
import unittest
from unittest import mock

import dateutil.parser
from freezegun import freeze_time
import kubernetes as k8s

from tests.utils import tear_down_registry

tear_down_registry()

# pylint: disable=wrong-import-position
from cki.cki_tools import k8s_event_listener  # noqa: E402


class TestConsumeStream(unittest.TestCase):
    """Test k8s_event_listener.consume_stream."""

    @staticmethod
    @mock.patch('kubernetes.watch.Watch', mock.Mock())
    @mock.patch('cki.cki_tools.k8s_event_listener.KubernetesHelper', mock.Mock())
    def _run(data, pod=None):
        k8s_event_listener.KubernetesHelper().api_corev1().read_namespaced_pod.return_value = pod
        with mock.patch('kubernetes.watch.Watch.return_value.stream', return_value=data):
            k8s_event_listener.EventListener().run()
        return k8s_event_listener.KubernetesHelper().api_corev1().create_namespaced_event

    @mock.patch('builtins.print')
    @freeze_time('2021-01-01T00:00:00.0+00:00')
    def test_consume_new(self, mock_print):
        """Test consume parses messages with date in the future."""
        data = [{'object': k8s.client.V1Event(
            type='type',
            involved_object=k8s.client.V1ObjectReference(kind='some kind', name='some name'),
            message='message',
            reason='reason',
            last_timestamp=dateutil.parser.parse('2021-01-01T00:01:00+00:00'),
            metadata=k8s.client.V1ObjectMeta(),
        )}]

        self._run(data)

        self.assertTrue(mock_print.called)
        mock_print.assert_called_with(
            '2021-01-01T00:01:00 - [type] - some kind - some name - message - (reason)'
        )

    @mock.patch('builtins.print')
    @freeze_time('2021-01-01T00:00:01.0+00:00')
    def test_consume_old(self, mock_print):
        """Test consume ignores messages with date in the past."""
        data = [{'object': k8s.client.V1Event(
            type='type',
            involved_object=k8s.client.V1ObjectReference(kind='some kind', name='some name'),
            message='message',
            reason='reason',
            last_timestamp=dateutil.parser.parse('2021-01-01T00:00:00+00:00'),
            metadata=k8s.client.V1ObjectMeta(),
        )}]

        self._run(data)

        self.assertFalse(mock_print.called)

    @mock.patch('builtins.print')
    @freeze_time('2021-01-01T00:01:00.0+00:00')
    def test_consume_multiple(self, mock_print):
        """Test consume with multiple messages."""
        timestamps = [
            # Past
            '2021-01-01T00:00:00+00:00',
            '2021-01-01T00:00:01+00:00',
            # Future
            '2021-01-01T00:01:01+00:00',
            '2021-01-01T00:01:02+00:00',
        ]
        data = [{'object': k8s.client.V1Event(
            type='type',
            involved_object=k8s.client.V1ObjectReference(kind='some kind', name='some name'),
            message='message',
            reason='reason',
            last_timestamp=dateutil.parser.parse(timestamp),
            metadata=k8s.client.V1ObjectMeta(),
        )} for timestamp in timestamps]

        self._run(data)

        self.assertTrue(mock_print.called)
        self.assertEqual(2, len(mock_print.mock_calls))

    @mock.patch('builtins.print')
    @freeze_time('2021-01-01T00:01:00.0+00:00')
    def test_timestamps_choosing(self, mock_print):
        """Test that it selects the correct timestamp."""
        # last_timestamp is present
        data = [{'object': k8s.client.V1Event(
            type='type',
            involved_object=k8s.client.V1ObjectReference(kind='some kind', name='some name'),
            message='message',
            reason='reason',
            last_timestamp=dateutil.parser.parse('2021-01-01T00:01:00+00:00'),
            metadata=k8s.client.V1ObjectMeta(
                    creation_timestamp=dateutil.parser.parse('2021-01-01T00:01:01+00:0')),
        )}]

        self._run(data)

        # last_timestamp is used
        mock_print.assert_called_with(
            '2021-01-01T00:01:00 - [type] - some kind - some name - message - (reason)'
        )

        mock_print.reset_mock()

        # last_timestamp is not present
        data = [{'object': k8s.client.V1Event(
            type='type',
            involved_object=k8s.client.V1ObjectReference(kind='some kind', name='some name'),
            message='message',
            reason='reason',
            last_timestamp=None,
            metadata=k8s.client.V1ObjectMeta(
                    creation_timestamp=dateutil.parser.parse('2021-01-01T00:02:02+00:0')),
        )}]

        self._run(data)

        # metadata.creation_timestamp is used
        mock_print.assert_called_with(
            '2021-01-01T00:02:02 - [type] - some kind - some name - message - (reason)'
        )

    @mock.patch('builtins.print', mock.Mock())
    @freeze_time('2021-01-01T00:01:00.0+00:00')
    def test_oom_event(self):
        """Test that an oom event gets reported."""
        data = [{'object': k8s.client.V1Event(
            type='Warning',
            involved_object=k8s.client.V1ObjectReference(kind='Pod', name='pod-1'),
            message='message',
            reason='Started',
            last_timestamp=dateutil.parser.parse('2021-01-01T00:02:02+00:0'),
            metadata=k8s.client.V1ObjectMeta(),
        )}]

        pod = k8s.client.V1Pod(
            status=k8s.client.V1PodStatus(container_statuses=[
                k8s.client.V1ContainerStatus(
                    image='image',
                    image_id='image_id',
                    name='name',
                    ready='ready',
                    restart_count='restart_count',
                    last_state=k8s.client.V1ContainerState(
                        terminated=k8s.client.V1ContainerStateTerminated(
                            reason='reason',
                            exit_code='code',
                        )),
                )]),
        )

        create_event_mock = self._run(data, pod=pod)
        create_event_mock.assert_not_called()  # pylint: disable=no-member

        pod.status.container_statuses[0].last_state.terminated.reason = 'OOMKilled'
        create_event_mock = self._run(data, pod=pod)
        create_event_mock.assert_called()  # pylint: disable=no-member
