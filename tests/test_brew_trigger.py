"""Tests for brew trigger."""
import unittest
from unittest import mock

from cki_lib.misc import utc_now_iso
from freezegun import freeze_time

import pipeline_tools.brew_trigger as brew


class TestNVRExtraction(unittest.TestCase):
    """Tests for brew_trigger.get_nvr()."""

    def test_request_match(self):
        """
        Verify a corrent NVR is returned if the request string match is
        detected.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        self.assertEqual(nvr, brew.get_nvr(
            None, {'id': 123, 'request': [request_string]}))

    def test_request_no_match(self):
        """Verify an empty string is returned if there is no match."""
        session = mock.Mock()
        session.listTasks.return_value = []
        self.assertEqual('', brew.get_nvr(
            session, {'id': 123, 'request': ['']}))

    def test_rpm_match(self):
        """
        Verify a corrent NVR is returned if we got an official build from
        dist-git.
        """
        nvr = 'kernel-3.10.0-1006.el7'
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = [{'nvr': nvr}]
        self.assertEqual(nvr, brew.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_rpm_match_scratch(self):
        """
        Verify a corrent NVR is returned if we got a scratch build from
        dist-git.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = [{'request': [f'tasks/123/{nvr}']}]
        self.assertEqual(nvr, brew.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_scratch_from_git(self):
        """
        Verify a corrent NVR is returned if we got a scratch build from git.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = 'git://git.org/users/myuser/linux.git?rh/koji#abcd'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = [{'request': [f'tasks/123/{nvr}']}]
        self.assertEqual(nvr, brew.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_rpm_no_match(self):
        """Verify an empty string is returned if we can't extract the NVR."""
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = []
        self.assertEqual('', brew.get_nvr(
            session, {'id': 123, 'request': [request_string]}))


class TestProcessMessage(unittest.TestCase):
    """Tests for brew_trigger.process_message()."""

    @staticmethod
    def _brew(pipelines=None):
        with freeze_time("2019-01-01"):
            config = {
                '.amqp': {
                    'server_url': 'server_url',
                    'top_url': 'top_url',
                    'web_url': 'web_url',
                },
                '.default': {
                    '.extends': '.amqp',
                    'send_report_to_upstream': 'False',
                    'send_report_on_success': 'False',
                },
            }
            config.update(pipelines or {})
            return brew.get_brew_trigger_variables(123, config, '.amqp')

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_no_request(self, mock_session):
        """
        Check we don't continue if the message doesn't contain a request
        string.
        """
        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {}

        with self.assertLogs(brew.LOGGER, 'INFO') as log:
            variables = self._brew()
            self.assertIsNone(variables)
            self.assertEqual(len(log.output), 1)
            self.assertIn('123: Task doesn\'t have a build request, ignoring',
                          log.output[0])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_scratch_disabled(self, mock_session):
        """
        Check we don't trigger pipeline for scratch build if the scratch build
        testing is disbled (not set).
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {'scratch': True}],
        }

        with self.assertLogs(brew.LOGGER, 'INFO') as log:
            variables = self._brew({'pipeline': {
                'rpm_release': 'el7', 'package_name': 'kernel'
            }})
            self.assertIsNone(variables)
            self.assertIn('Scratch build testing disabled in trigger',
                          log.output[-2])
            self.assertIn(f'123: Pipeline for {nvr} not configured!',
                          log.output[-1])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_wrong_owner(self, mock_session):
        """
        Check we don't trigger pipeline if the trigger is specified only for
        different users than the one that submitted the build.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [request_string, 'tag', {}],
        }
        mock_session_instance.getUser.return_value = {'name': 'bad_user'}

        with self.assertLogs(brew.LOGGER, 'INFO') as log:
            variables = self._brew({'pipeline': {
                'rpm_release': 'el7',
                'package_name': 'kernel',
                '.owners': ['user1', 'another-user'],
            }})
            self.assertIsNone(variables)
            self.assertIn('123: Trigger not configured for user bad_user',
                          log.output[-2])
            self.assertIn(f'123: Pipeline for {nvr} not configured!',
                          log.output[-1])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_no_nvr(self, mock_session):
        """Check we don't continue if no NVR is found."""
        request_string = 'cli-build/1550507696.83405.UHlEWvSP/nothing_here'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }

        with self.assertLogs(brew.LOGGER, 'DEBUG') as log:
            variables = self._brew()
            self.assertIsNone(variables)
            self.assertIn('Can\'t get NVR from task', log.output[-1])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_no_trigger_match(self, mock_session):
        """Check we don't continue if there is no trigger for given build."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }

        with self.assertLogs(brew.LOGGER, 'DEBUG') as log:
            variables = self._brew()
            self.assertIsNone(variables)
            self.assertIn(f'Pipeline for {nvr} not configured', log.output[-1])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_bad_package_name(self, mock_session):
        """Check we don't continue if the package name doesn't match."""
        nvr = 'python-metakernel-0.21.0-1.fc30'
        request_string = 'git://example.org/rpms/python-metakernel#1a2b'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': nvr}]

        with self.assertLogs(brew.LOGGER, 'DEBUG') as log:
            variables = self._brew()
            self.assertIsNone(variables)
            self.assertIn(f'Pipeline for {nvr} not configured', log.output[-1])

    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_scratch_enabled(self, mock_session):
        """
        Check we trigger a pipeline for scratch build if the scratch build
        testing is enabled.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {'scratch': True}]
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]

        variables = self._brew({'pipeline': {
            'rpm_release': 'el7',
            'package_name': 'kernel',
            '.test_scratch': 'True',
        }})
        self.assertIsNotNone(variables)

    @freeze_time("2019-01-01")
    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_pipeline_triggered(self, mock_session):
        """Check we trigger a pipeline."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [
                request_string,
                'tag',
                {'arch_override': 'x86_86 ppc64le'},
            ],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._brew({'pipeline': {
            'rpm_release': 'el7',
            'package_name': 'kernel',
            '.coprs': ['user/repo1', 'user/repo2'],
        }})
        expected_variables = {'name': 'pipeline',
                              'rpm_release': 'el7',
                              'package_name': 'kernel',
                              'nvr': nvr,
                              'brew_task_id': '123',
                              'title': 'Brew: Task 123',
                              'is_scratch': 'False',
                              'submitter': 'name@redhat.com',
                              'architectures': 'x86_86 ppc64le',
                              'server_url': 'server_url',
                              'web_url': 'web_url',
                              'top_url': 'top_url',
                              'cki_pipeline_type': 'brew',
                              'send_report_on_success': 'False',
                              'send_report_to_upstream': 'False',
                              'discovery_time': utc_now_iso()
                              }
        self.assertEqual(variables, expected_variables)

    @freeze_time("2019-01-01")
    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_nvr_with_letters(self, mock_session):
        """
        Check trigger matching works if a kernel NVR contains letter between
        the package name and release. Regression test, as a residue of previous
        match implementation this didn't work but it should.
        """
        nvr = 'kernel-3.10.0.rc8.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [request_string, 'tag', {}],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._brew({'pipeline': {
            'rpm_release': 'el7', 'package_name': 'kernel'
        }})
        expected_variables = {'name': 'pipeline',
                              'rpm_release': 'el7',
                              'package_name': 'kernel',
                              'nvr': nvr,
                              'brew_task_id': '123',
                              'title': 'Brew: Task 123',
                              'is_scratch': 'False',
                              'submitter': 'name@redhat.com',
                              'server_url': 'server_url',
                              'web_url': 'web_url',
                              'top_url': 'top_url',
                              'cki_pipeline_type': 'brew',
                              'send_report_on_success': 'False',
                              'send_report_to_upstream': 'False',
                              'discovery_time': utc_now_iso()
                              }
        self.assertEqual(variables, expected_variables)

    @freeze_time("2019-01-01")
    @mock.patch('pipeline_tools.brew_trigger.koji.ClientSession')
    def test_architectures(self, mock_session):
        """Check architectures is set correctly if the user overrides it."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [
                request_string,
                'tag',
                {'arch_override': 'x86_86 ppc64le'}
            ],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._brew({'pipeline': {
            'rpm_release': 'el7',
            'package_name': 'kernel',
            'architectures': 'ppc64le',
        }})
        expected_variables = {'name': 'pipeline',
                              'rpm_release': 'el7',
                              'package_name': 'kernel',
                              'nvr': nvr,
                              'brew_task_id': '123',
                              'title': 'Brew: Task 123',
                              'is_scratch': 'False',
                              'submitter': 'name@redhat.com',
                              'architectures': 'ppc64le',
                              'server_url': 'server_url',
                              'web_url': 'web_url',
                              'top_url': 'top_url',
                              'cki_pipeline_type': 'brew',
                              'send_report_on_success': 'False',
                              'send_report_to_upstream': 'False',
                              'discovery_time': utc_now_iso()
                              }
        self.assertEqual(variables, expected_variables)
