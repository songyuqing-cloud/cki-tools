"""Test triager."""
import unittest
from unittest.mock import patch

from datawarehouse import Datawarehouse
import responses

from cki.triager.checkers import FailureChecker
from cki.triager.checkers import TestFailureChecker
from cki.triager.settings import FAIL_KICKSTART
from cki.triager.triager import DWObject

MOCK_REGEXES = [
    {
        "id": 2,
        "issue": {
            "id": 88,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description",
            "ticket_url": "https://bug.link",
            "resolved": False,
            "generic": False
        },
        "text_match": "bnx2x .* Direct firmware load for",
        "file_name_match": None,
        "test_name_match": None
    },
    {
        "id": 15,
        "issue": {
            "id": 126,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "BUG: clone.*failing after kernel commit",
            "ticket_url": "http://url.com",
            "resolved": False,
            "generic": False,
            "origin_tree": None
        },
        "text_match": "tag=kcmp03.*FAIL: clone",
        "file_name_match": "syscalls.fail.log",
        "test_name_match": "LTP"
    }
]


class FailureCheckerTest(unittest.TestCase):
    """Test FailureChecker."""

    @responses.activate
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex(self):
        """Test check_logs_with_regex."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': MOCK_REGEXES})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [{'name': 'Bug description', 'id': 88}],
            issues)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')
        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual([], issues)

    @responses.activate
    @patch('cki.triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex_multiple_matches(self):
        """Test check_logs_with_regex. More than one regex matches."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=(b'[   41.451946] tag=kcmp03 error error FAIL: clone'))

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': MOCK_REGEXES})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'ltp',
            'comment': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [
                {'id': 88, 'name': 'Bug description'},
                {'id': 126, 'name': 'BUG: clone.*failing after kernel commit'}
            ],
            issues
        )

    @patch('cki.triager.checkers.FailureChecker.check_logs_with_regex')
    def test_check_check_all(self, check1):
        """Test check_all call."""
        issues = FailureChecker(None)
        issues.check_all()

        self.assertTrue(check1.called)

    @patch('cki.triager.checkers.FailureChecker.check_all')
    def test_check_check(self, check_all):
        """Test check call."""
        FailureChecker.check(None)

        self.assertTrue(check_all.called)


class TestFailureCheckerTest(unittest.TestCase):
    """Test TestFailureChecker."""

    def test_check_kickstart_error(self):
        """Test check_kickstart_error."""
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
        })
        issues = TestFailureChecker(test).check_kickstart_error()

        self.assertEqual([FAIL_KICKSTART], issues)

    def test_check_kickstart_ok(self):
        """Test check_kickstart_error."""
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 10,
        })
        issues = TestFailureChecker(test).check_kickstart_error()

        self.assertEqual([], issues)

    @patch('cki.triager.checkers.TestFailureChecker.check_logs_with_regex')
    @patch('cki.triager.checkers.TestFailureChecker.check_kickstart_error')
    def test_check_check_all(self, check1, check2):
        """Test check_all call."""
        issues = TestFailureChecker(None)
        issues.check_all()

        self.assertTrue(check1.called)
        self.assertTrue(check2.called)
