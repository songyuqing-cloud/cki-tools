# Beaker broken machines updater

`cki.beaker_tools.broken_machines`

This script queries TEIID database and gets a list of broken
machines from [Beaker](https://beaker-project.org).

The produced list of machines is used when submitting testing
jobs so we avoid infrastructure failures which we'd encounter
by running on these machines.

| Environment variable          | Required | Default                     | Description                                                                |
|-------------------------------|----------|-----------------------------|----------------------------------------------------------------------------|
| `TEIID_HOST`                  | Yes      |                             | Hostname of TEIID instance                                                 |
| `TEIID_PORT`                  | Yes      |                             | Port of TEIID instance                                                     |
| `TEIID_DATABASE`              | Yes      |                             | Database name                                                              |
| `TEIID_KEYTAB_PRINCIPAL`      | No       |                             | Obtain a kerberos ticket for the given principal                           |
| `TEIID_KEYTAB_CONTENT_BASE64` | No       |                             | Keytab to authenticate the kerberos principal                              |
| `BUCKET_CONFIG_NAME`          | Yes      |                             | Name of environment variable with S3 bucket spec to use as a backing store |
| `IS_PRODUCTION`               | No       | `False`                     | Define if the environment is staging or production                         |
| `SENTRY_SDN`                  | No       |                             | Sentry SDN                                                                 |
| `LIST_PATH`                   | No       | `/broken-machines-list.txt` | Path for the machines list file in the bucket                              |
| `THRESHOLD_RECIPES_RUN`       | No       | `15`                        | Minimum recipes a machine needs to have run to be included                 |
| `THRESHOLD_BROKEN`            | No       | `0.5`                       | Ratio of failed jobs for machines to be considered broken                  |
| `DAYS_TO_CHECK`               | No       | `7`                         | Days in the past to be included in the checks                              |
| `LIMIT_RESULTS`               | No       | `50 `                       | Number of machines to be included in the resulting list                    |

### IS_PRODUCTION

On staging developments (`IS_PRODUCTION=False`), the changes are only logged but
not uploaded to the S3 bucket.
