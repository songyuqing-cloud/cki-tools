"""Trigger pipelines from brew/koji."""
import argparse
import os
import pathlib
import re

from cki_lib import cki_pipeline
from cki_lib import config_tree
from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from gitlab import Gitlab
import koji
import requests
import yaml

LOGGER = logger.get_logger('cki.pipeline_tools.brew_trigger')
SESSION = session.get_session('cki.pipeline_tools.brew_trigger')


def get_nvr(koji_session, task_data):
    """Try to get the NVR from the task tree."""
    task_id = task_data['id']
    request_string = task_data['request'][0]
    rpm_match = re.search(r'/rpms/(.*)(?:.git)?\??#', request_string)
    # Handle official builds from dist-git
    if rpm_match:
        LOGGER.info('%s: Build from dist-git found!', task_id)
        # These don't contain full NVR in the request, just "kernel", and
        # we need to get it from the task itself.
        build_info = koji_session.listBuilds(taskID=task_id)
        if build_info:
            return build_info[0]['nvr']

    # Handle scratch builds from SRPM
    nvr_match = re.search(r'.*/(.*\.rpm)', request_string)
    if nvr_match:
        return nvr_match.group(1)

    # Handle all the rest -- scratch builds from dist git or git trees
    for child in koji_session.listTasks(
            opts={'parent': task_id, 'method': 'buildArch', 'decode': True},
            queryOpts={'limit': 1}):
        return os.path.basename(child['request'][0])

    LOGGER.warning('%s: Can\'t get NVR from task: %s', task_id, request_string)
    return ''


def get_brew_trigger_variables(task_id, brew_config, server_section):
    # pylint: disable=too-many-locals
    """Determine the trigger variables for a brew task."""
    server_url = brew_config[server_section]['server_url']
    koji_session = koji.ClientSession(server_url)

    task_data = koji_session.getTaskInfo(task_id, request=True)

    if not task_data.get('request'):
        LOGGER.info('%s: Task doesn\'t have a build request, ignoring',
                    task_id)
        return None
    task_options = task_data['request'][2]

    nvr = get_nvr(koji_session, task_data)
    if not nvr:
        return None

    LOGGER.info('%s: Build %s found!', task_id, nvr)

    for pipeline_name, pipeline_config in \
            config_tree.process_config_tree(brew_config).items():
        if 'result_pipe' in pipeline_config:
            continue
        if not re.search(r'^{}-\d+[.-](\S+[.-])+{}'.format(
                pipeline_config['package_name'],
                pipeline_config['rpm_release']), nvr):
            continue

        variables = pipeline_config.copy()

        LOGGER.info('%s: Found possible trigger', task_id)
        if task_options.get('scratch') and \
                not misc.strtobool(variables.get('.test_scratch', 'False')):
            LOGGER.info('%s: Scratch build testing disabled in trigger',
                        task_id)
            continue

        if 'owner' in task_data:
            owner_name = koji_session.getUser(task_data['owner'])['name']
            configured_owners = variables.get('.owners', [])
            if not configured_owners or owner_name in configured_owners:
                variables['submitter'] = f'{owner_name}@redhat.com'
            else:
                LOGGER.info('%s: Trigger not configured for user %s',
                            task_id, owner_name)
                continue

        variables['is_scratch'] = misc.booltostr(task_options.get('scratch'))

        brew_arches = task_options.get('arch_override')
        if brew_arches:
            if 'architectures' in variables:
                # Override brew's override by user's, but only for arches that
                # were actually built. Split out the code to multiple lines so
                # we actually understand what's going on...
                brew_arch_set = set(brew_arches.split())
                user_arch_set = set(variables['architectures'].split())
                new_arches = user_arch_set & brew_arch_set
                variables['architectures'] = ' '.join(new_arches)
            else:
                variables['architectures'] = brew_arches

        variables['nvr'] = nvr
        variables['brew_task_id'] = str(task_id)
        variables['title'] = f'Brew: Task {task_id}'
        variables['cki_pipeline_type'] = 'brew'
        variables['name'] = pipeline_name
        variables['discovery_time'] = misc.utc_now_iso()

        return config_tree.clean_config(variables)

    # We iterated over all triggers and didn't find a match
    LOGGER.info('%s: Pipeline for %s not configured!', task_id, nvr)
    return None


def main():
    """Command line interface to the triggering brew pipelines."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gitlab-url', default=os.environ.get('GITLAB_URL'),
        help='GitLab URL (default: value of GITLAB_URL environment variable)')
    parser.add_argument(
        '--private-token', default='GITLAB_PRIVATE_TOKEN',
        help='Name of the env variable that contains the GitLab private token'
        ' (default: GITLAB_PRIVATE_TOKEN)')
    config = parser.add_mutually_exclusive_group()
    config.add_argument(
        '-c', '--config', default='brew.yaml',
        help='YAML configuration file to use (default: brew.yaml)')
    config.add_argument(
        '--config-url',
        help='URL to YAML configuration file to use.')
    parser.add_argument(
        '--server-section', default='.amqp',
        help='YAML section with server configuration (default: .amqp)')
    parser.add_argument(
        "--variables", action=misc.StoreNameValuePair, default={},
        metavar='KEY=VALUE', help='Override trigger variables')
    parser.add_argument(
        'task_id',
        help='Brew task ID')
    args = parser.parse_args()

    if args.config_url:
        config_content = requests.get(args.config_url).content
    else:
        config_content = pathlib.Path(args.config).read_text()

    config = yaml.safe_load(config_content)
    variables = get_brew_trigger_variables(args.task_id, config,
                                           args.server_section)
    if not args.gitlab_url:
        raise Exception(
            'GitLab instance URL required via --gitlab-url/GITLAB_URL')
    gl_instance = Gitlab(args.gitlab_url,
                         private_token=os.getenv(args.private_token),
                         session=SESSION)
    gl_project = cki_pipeline.pipeline_project(gl_instance, variables)
    gl_pipeline = cki_pipeline.trigger(gl_project, variables,
                                       variable_overrides=args.variables,
                                       interactive=True,
                                       non_production_delay_s=0)  # interactive use
    print(f'Pipeline: {gl_pipeline.web_url}')


if __name__ == '__main__':
    main()
