"""Pipeline tools."""
from cki_lib.logger import get_logger

from . import cancel_pipeline
from . import get_last_successful_pipeline
from . import retrigger

LOGGER = get_logger(__name__)

TOOLS = {
    'cancel_pipeline': cancel_pipeline,
    'get_last_successful_pipeline': get_last_successful_pipeline,
    'retrigger': retrigger
}
