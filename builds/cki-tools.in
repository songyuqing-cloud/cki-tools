#include "setup-base"

/* deployment */
RUN dnf -y install origin-clients skopeo

/* git-cache-updater */
RUN dnf -y install jq tar awscli

/* git-s3-sync */
RUN dnf -y install git-lfs awscli

/* datawarehouse-backuper */
RUN dnf -y install diffutils patch awscli postgresql

/* brew trigger */
RUN dnf -y install python3-koji python3-gssapi

/* cki_deployment_acme */
RUN dnf -y install dehydrated jq tar origin-clients python3-dns-lexicon+route53 toilet lolcat

/* kinit dependency for kerberos authentication in cki.beaker_tools.broken_machines */
RUN dnf -y install krb5-workstation

/* Add dependencies of CI/CD jobs */
RUN dnf install -y --nobest \
    ansible \
    ansible-collection-community-general \
    ansible-collection-community-kubernetes \
    awscli \
    cpp \
    diffutils \
    findutils \
    gcc \
    hadolint \
    hugo \
    jq \
    krb5-devel \
    lolcat \
    npm \
    openssl-devel \
    origin-clients \
    pre-commit \
    python3-ansible-lint \
    python3-boto \
    python3-boto3 \
    python3-devel \
    python3-ldap \
    python3-netaddr \
    python3-openshift \
    python3-openstackclient \
    python3-pyyaml \
    python3-tox \
    ShellCheck \
    shyaml \
    toilet \
    yamllint \
    zip

/* Install Muffet from tarball, not packaged */
RUN set -o pipefail \
    && curl --silent --location "https://github.com/raviqqe/muffet/releases/download/v2.3.2/muffet_2.3.2_Linux_x86_64.tar.gz" \
    | tar --extract --gzip --file - --directory /usr/local/bin muffet

/* Install markdownlint */
RUN npm install --global markdownlint-cli

/* Install hugo dependencies */
RUN npm install --global postcss postcss-cli autoprefixer

/* Helpers to allow entrypoint mangling for promtail logging */
RUN pip install dumb-init
RUN dnf install -y --nobest busybox

/* Install cki-tools */
#define _PYTHON_REQUIREMENTS_EXTRAS brew,deployment_tools,beaker_broken_machines
#include "python-requirements"

#include "cleanup"
