# K8S Event Listener

`cki.cki_tools.k8s_event_listener`

Listen for [Kubernetes Events](https://kubernetes.io/docs/reference/kubernetes-api/cluster-resources/event-v1/) in the namespace and print them to stdout.

This service is intended to run together with some other service such as
[Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) to capture the output and forward it to a persistent storage.

## Configuration

A valid Kubernetes context is required, either from a [kubeconfig file](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) or via [service account credentials](https://kubernetes.io/docs/tasks/access-application-cluster/access-cluster/#accessing-the-api-from-a-pod).

No extra configuration is necessary.
