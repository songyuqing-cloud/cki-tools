# CKI Tools

All the command line tools the CKI team uses.

## Tests

To run the tests, just type `tox`. That should take care of installing
all dependencies needed for it. However, you have to make sure you
install the package `krb5-devel` (with `dnf`, `yum`, or similar).

You can also run tests in a podman container via

```shell
podman run --rm -it --volume .:/code --workdir /code registry.gitlab.com/cki-project/containers/python tox
```
## Optional dependencies

To use the `brew_trigger` module, use the `brew` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[brew]
```

To use the `kcidb` module, use the `kcidb` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[kcidb]
```

To use the `beaker-respin` CLI, use the `beaker_respin` extra like

```shell
python3 -m pip install git+https://gitlab.com/cki-project/cki-tools.git/#egg=cki_tools[beaker_respin]
```

## `shell-scripts/cki_deployment_clean_docker_images.sh`

Remove old docker images on the runners. A list of images to keep is built, and
all other images are removed.

The following images are kept:
- all :latest images
- all :production images
- the last three :mr-123 images
- the last three :p-123 images
- the last three :20210101.1 images
- all gitlab-runner-helper images

```shell
cki_deployment_clean_docker_images.sh
```

### Environment variables

| Field           | Type   | Required | Description                            |
|-----------------|--------|----------|----------------------------------------|
| `IS_PRODUCTION` | string | no       | Images are only removed if set to true |

## `shell-scripts/cki_deployment_grafana_backup.sh`

Backup a Grafana instance by creating dumps via `cki.deployment_tools.grafana`.

```shell
cki_deployment_grafana_backup.sh <group> <count>
```

The last `<count>` backups per group `<group>` are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_grafana_backup.sh daily    7
@weekly  cki_deployment_grafana_backup.sh weekly   4
@monthly cki_deployment_grafana_backup.sh monthly 12
@yearly  cki_deployment_grafana_backup.sh yearly   2
```

### Environment variables

| Field                | Type   | Required | Description                                               |
|----------------------|--------|----------|-----------------------------------------------------------|
| `GRAFANA_URL`        | string | yes      | Grafana instance URL                                      |
| `GRAFANA_TOKEN`      | string | yes      | Grafana secret token                                      |
| `BUCKET_CONFIG_NAME` | string | yes      | Name of an environment variable with bucket configuration |

## `shell-scripts/cki_deployment_grafana_mr.sh`

Create MRs of changed data in Grafana instances via `cki.deployment_tools.grafana`.

```shell
cki_deployment_grafana_mr.sh
```

### Environment variables

| Field                | Type   | Required | Description                                        |
|----------------------|--------|----------|----------------------------------------------------|
| `GRAFANA_URL`        | string | yes      | Grafana instance URL                               |
| `GRAFANA_TOKEN`      | string | yes      | Grafana secret token                               |
| `GITLAB_PROJECT_URL` | string | yes      | Git repo URL without protocol                      |
| `GITLAB_FORK_URL`    | string | yes      | URL of the fork of the Git repo without protocol   |
| `GITLAB_TOKEN`       | string | yes      | GitLab private token with access to the Git repo   |
| `BRANCH_NAME`        | string | yes      | Branch name in the fork of the git repo            |
| `GRAFANA_DATA_DIR`   | string | yes      | Directory in the git repo to hold the Grafana data |
| `GIT_USER_NAME`      | string | yes      | Git user name                                      |
| `GIT_USER_EMAIL`     | string | yes      | Git user email                                     |

Changes need to be backwards-compatible as this script is also used by the OSCI
and TFT teams at
<https://gitlab.cee.redhat.com/osci/monitoring/-/blob/master/.gitlab-ci.yml#L265>!

## `shell-scripts/cki_deployment_acme.sh`

See the [detailed documentation](README.acme.md).

## `shell-scripts/cki_deployment_osp_backup.sh`

Backup an OpenStack instance by creating snapshots.

```shell
cki_deployment_osp_backup.sh <name> <group> <count>
```

For each instance `<name>`, the last `<count>` images per backup group `<group>`
are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_osp_backup.sh instance-name daily    7
@weekly  cki_deployment_osp_backup.sh instance-name weekly   4
@monthly cki_deployment_osp_backup.sh instance-name monthly 12
@yearly  cki_deployment_osp_backup.sh instance-name yearly   2
```

### Environment variables

| Field                    | Type   | Required | Description                     |
|--------------------------|--------|----------|---------------------------------|
| `OS_PROJECT_NAME`        | string | yes      | OpenStack project name          |
| `OS_PROJECT_DOMAIN_NAME` | string | yes      | OpenStack project domain name   |
| `OS_AUTH_URL`            | string | yes      | OpenStack Keystone endpoint URL |
| `OS_USERNAME`            | string | yes      | OpenStack user name             |
| `OS_USER_DOMAIN_NAME`    | string | yes      | OpenStack user domain name      |
| `OS_PASSWORD`            | string | yes      | OpenStack password              |

## `shell-scripts/cki_deployment_pgsql_backup.sh`

Backup a PostgreSQL database to an S3 bucket.

```shell
cki_deployment_pgsql_backup.sh <group> <count>
```

The last `<count>` backups per group `<group>` are kept.

The following `crontab` schedule would perform daily, weekly, monthly and
yearly backups:

```plain
@daily   cki_deployment_pgsql_backup.sh daily    7
@weekly  cki_deployment_pgsql_backup.sh weekly   4
@monthly cki_deployment_pgsql_backup.sh monthly 12
@yearly  cki_deployment_pgsql_backup.sh yearly   2
```

### Environment variables

| Field                 | Type   | Required | Description                                               |
|-----------------------|--------|----------|-----------------------------------------------------------|
| `BUCKET_CONFIG_NAME`  | string | yes      | Name of an environment variable with bucket configuration |
| `POSTGRESQL_USER`     | no     | yes      | PostgreSQL user name                                      |
| `POSTGRESQL_PASSWORD` | yes    | yes      | PostgreSQL password                                       |
| `POSTGRESQL_HOST`     | no     | yes      | PostgreSQL host                                           |
| `POSTGRESQL_DATABASE` | no     | yes      | PostgreSQL database                                       |

## `beaker_tools.respin`

```shell
python3 -m cki.beaker_tools.respin [options]
```

See the command line help for details.

## `brew_trigger`

```shell
python3 -m pipeline_tools.brew_trigger [-c CONFIG] [options] task_id
```

Trigger a CKI pipeline for a kernel from Brew/Koji. The same configuration
files as for the [brew-trigger pipeline-trigger module] can be used. If
`IS_PRODUCTION` variable is not present or set to `False`, retriggered pipelines
are created instead of production.

[brew-trigger pipeline-trigger module]: https://gitlab.com/cki-project/pipeline-trigger/-/blob/main/triggers/brew_trigger.py

## `cki.monitoring_tools.aws_cost_explorer`

Get a short summary of AWS costs.

```shell
Usage: python3 -m cki.monitoring_tools.aws_cost_explorer
    [--threshold COST]
    [--max-items COUNT]
    [--item-cutoff COST]
    [--filter FILTER...]
    [GROUP]
```

The following groups are available:

| Group           | Description                                         |
|-----------------|-----------------------------------------------------|
| `daily`         | Daily costs for the last week, most recent first    |
| `component`     | normalized 7-day average per `ServiceComponent` tag |
| `owner`         | normalized 7-day average per `ServiceOwner` tag     |
| `service`       | normalized 7-day average per AWS Service            |
| `usage_type`    | normalized 7-day average per AWS Usage Type         |
| `instance_type` | normalized 7-day average per AWS Instance Type      |
| `operation`     | normalized 7-day average per AWS API Operation      |

The exit code will be non-zero if the threshold set with `--threshold` is
exceeded for the previous day.

Filters specified with `--filter` can be used to limit the results to certain
tags, e.g.

```shell
python3 -m cki.monitoring_tools.aws_cost_explorer \
    --filter ServiceOwner=CkiProject \
    component
```

The number of shown items can be further limited by cost (`--item-cutoff`) or
count (`--max-items`).

## `cki.monitoring_tools.check_s3_bucket_size`

Summarize size and number of files on an S3 bucket.

```shell
Usage: python3 -m cki.monitoring_tools.check_s3_bucket_size
    [--quota SIZE]
    [--threshold SIZE]
    [--ignore-prefix]
    [BUCKET_SPEC]
```

The Bucket specification needs to be given as the name of an environment variable like

```
BUCKET_SOFTWARE="http://localhost:9000|cki_temporary|super_secret|software|subpath/"
```

The exit code will be non-zero if the total size of all objects in the bucket
(in GB) exceeds the given threshold.

When a quota (in GB) is specified, the output includes the relative usage.

## `cki.deployment_tools.gitlab_runner_config`

See the [detailed documentation](README.gitlab_runner_config.md).

## `cki.cki_tools.scratch_cleaner`

Check whether the GitLab jobs for directories on the scratch volume are still
running. Scratch directories of already finished jobs are deleted.

```shell
python3 -m cki.cki_tools.scratch_cleaner [--scratch-volume PATH]
```

#### Environment variables

| Field               | Type   | Required | Description                                                           |
|---------------------|--------|----------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`     | dict   | yes      | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`      | string | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                |
| `IRCBOT_URL`        | no     | no       | IRC bot endpoint                                                      |
| `URL_SHORTENER_URL` | no     | no       | URL shortener endpoint to link to GitLab jobs in IRC messages         |

## `cki.cki_tools.select_kpet_tree`

Determine the correct kpet tree for a tree family and the kernel NVR.

```shell
python3 -m cki.cki_tools.select_kpet_tree tree_family nvr
```

For some tree families like `rhel7`, multiple kpet trees with specific
requirements exist. The correct tree is selected by mapping tree family and
kernel NVR to a kpet tree as defined in the `mapping` variable inside
`select_kpet_tree`.

## `cki.cki_tools.url_shortener`

See the [detailed documentation](README.url_shortener.md).

## `cki.cki_tools.webhook_receiver`

The webhook receiver can post events received from GitLab and Sentry.io to an
AMQP message bus.

| Environment variable                       | Description                                         |
|--------------------------------------------|-----------------------------------------------------|
| `RABBITMQ_HOST`                            | AMQP host                                           |
| `RABBITMQ_PORT`                            | AMQP port, TLS is used for port 443                 |
| `RABBITMQ_USER`                            | AMQP user                                           |
| `RABBITMQ_PASSWORD`                        | AMQP password                                       |
| `RABBITMQ_EXCHANGE`                        | AMQP exchange for the posted messages               |
| `RABBITMQ_CAFILE`                          | AMQP CA file path                                   |
| `RABBITMQ_CERTFILE`                        | AMQP certificate + private key file path            |
| `RABBITMQ_KEEPALIVE_S`                     | Time to keep AMQP connection alive between messages |
| `WEBHOOK_RECEIVER_WEBSECRET`               | GitLab webhook secret                               |
| `WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` | Sentry.io webhook client secret                     |

### GitLab integration

Go to <https://gitlab.com/your-project/hooks>, and add a new webhook for
<https://webhook-receiver-host/>, the secret from the `WEBHOOK_RECEIVER_WEBSECRET`
environment variable and all required trigger types.

Messages posted to the exchange will use a routing key of
`hostname.project.event`, e.g.
`gitlab.com.cki-project.kernel-ark.merge_request`.

### Sentry.io integration

Go to Settings -> Organization -> Developer Settings, create a new internal
integration with a webhook URL of <https://webhook-receiver-host/sentry>, and
enable `Alert Rule Action` there. Save the client secret in the
`WEBHOOK_RECEIVER_SENTRY_IO_CLIENT_SECRET` environment variable.

For each project where events should be forwarded to the message bus, go to the
Alerts page and Create an alert rule with `Issue Alert`, when `an event is
seen` and `send a notification via an integration` via the integration defined
above.

Messages posted to the exchange will use a routing key of
`sentry.io.project.resource.action`, e.g.
`sentry.io.webhook-receiver.event_alert.triggered`.

## KCIDB Image.

An extra image is built to run the KCIDB module.
This image is used on OpenShift pods, and contains `kcidb` extra dependencies.

`registry.gitlab.com/cki-project/cki-tools/kcidb`

## `cki.deployment_tools.grafana`

```shell
python3 -m cki.deployment_tools.grafana download|upload [--path PATH]
```

Tool to backup and restore Grafana data. Download configuration into locally stored
JSON files, which can be later be modified and re uploaded.

- `download` option uses Grafana API to download datasources, notification channels and
dashboards and saves them to PATH.

- `upload` takes the json files stored in PATH and restores them to the Grafana server.

`GRAFANA_URL` and `GRAFANA_TOKEN` environment variables need to be configured.
Follow the instructions on the [Grafana Docs] to learn how to get an API Token.

[Grafana Docs]: https://grafana.com/docs/grafana/latest/http_api/auth/#create-api-token

## `cki.deployment_tools.render` - rendering templates

The `render` tool renders text files via `jinja2`.

```text
$ python3 -m cki.deployment_tools.render --help
usage: render.py [-h] [--data NAME=PATH]  [--raw-data NAME=PATH] [--output OUTPUT] [template]

Render templates

positional arguments:
  template              template file to render

optional arguments:
  -h, --help            show this help message and exit
  --data NAME=PATH      JSON/YAML data file(s) to expose as NAME
  --raw-data NAME=PATH  raw data file(s) to expose as NAME
  --include-path INCLUDE_PATH
                        path for template includes
  --output OUTPUT       Output path for the rendered file.
```

The data files will be exposed as variables that can be used via something like
`{{ name.data }}`. For `--data`, the file contents of yml/yaml/json files will
be parsed. For `--raw-data`, the raw text contents is provided. If a file name
ends in `.j2`, the file contents will be treated as a template.

As an example, the following files are present:

```yaml
# data-file.yml
foo: bar1

# data-dir/file1.yml
foo: bar2

# data-dir/dir1/file2.yml
foo: bar3

# raw-data.txt
bar4

# template.txt
{{ data1.foo }}
{{ data2.file1.foo }}
{{ data2.dir1.file2.foo }}
{{ data3["raw-data.txt"] }}
```

The template can be rendered via

```shell
python3 -m cki.deployment_tools.render \
    template.txt \
    --data data1=data-file.yml \
    --data data2=data-dir \
    --raw-data data3=raw-data.txt
```

The resulting output will be

```plain
bar1
bar2
bar3
bar4
```

Besides normal jinja2 templating, several custom globals/filters are provided:

- The `env` filter allows to replace variables following the bash notation
  (`${VAR_NAME}` or `$VAR_NAME`) with environment variables.

  If a variable is defined on the data file as `foo: ${BAR}`, and it's used
  with the `env` filter `{{ data.foo | env }}` it will be replaced with the
  content of the `BAR` environment variable. If `BAR` is not defined, a
  `KeyError` exception will be raised.

- The `is_true` filter converts a string to a boolean.

- The `from_yaml` filter converts a YAML/JSON string to an object.

- The `env` global variable exposes `os.environ` to the template.

- The `url(binary=False, json=False)` global function allows to call into
  `requests` to retrieve an external file.

<!-- vi: set spell spelllang=en: -->
