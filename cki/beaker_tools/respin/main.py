"""Create a clone (XML output) of a Beaker that re-runs the failed tasks."""
import collections
from importlib import resources
import pathlib
import re
import subprocess

from bs4 import BeautifulSoup as BS
from cki_lib import logger
from cki_lib import misc
from cki_lib.retrying import retrying_on_exception
import requests
import yaml

from . import parse_args

LOGGER = logger.get_logger(__name__)


class MultiRecipeTask:
    """Class to manage multirecipe tasks with roles (CLIENTS/SERVERS)."""

    def __init__(self, recipe, task, task_position):
        """Create an instance."""
        self.recipe_id = recipe['id']
        self.task_position = task_position
        self.role = task['role']

        self.task = task
        self.recipe = recipe

        cki_suite_description = None
        try:
            task_selector = 'params param[name="CKI_SUITE_DESCRIPTION"]'
            cki_suite_description = task.select(task_selector)[0]
        except IndexError:
            pass

        self.descr = cki_suite_description['value'] if cki_suite_description \
            else task['name']

    @classmethod
    def has_failed_tasks(cls, lst):
        """Check if there are any items with failed tasks in the list."""
        for item in lst:
            if item.task['result'] != 'Pass':
                return True

        return False

    @classmethod
    def is_complementary_role_list(cls, lst):
        """Check if the list has tasks with both CLIENTS/SERVERS roles."""
        has_servers = False
        has_clients = False
        for item in lst:   # type: MultiRecipeTask
            if item.role == 'SERVERS':
                has_servers = True
            elif item.role == 'CLIENTS':
                has_clients = True

            if has_servers and has_clients:
                return True

        return has_clients and has_servers

    def __str__(self):
        """Return custom string representation."""
        task_name = self.task['name']

        return f'{task_name}[{self.task_position}]: {self.recipe_id} ' \
               f'{self.role}'

    def __repr__(self):
        """Return custom string representation."""
        return self.__str__()


class BeakerRespin:
    # pylint: disable=too-many-public-methods
    """Modify Beaker XML to re-run specific tasks with given settings."""

    def __init__(self, args, extra_args):
        """Create the object.

        Attributes:
            args: all parsed command line arguments (argparse.Namespace)
            extra_args: pyyaml.parse()['arguments'] output
        """
        self.args = args
        self.extra_args = extra_args

        # task that should be present in each job; we don't copy these
        self.ci_tasks = ["SELinux Custom Module Setup",
                         "/distribution/check-install",
                         "Boot test",
                         "/test/misc/machineinfo"]

    @classmethod
    def conditional_cancel_job(cls, task_spec):
        """Cancel a job or recipeset identified by task_spec if it is a respin job."""
        xml = cls.get_bkr_results(task_spec)
        soup = BS(xml, 'xml')
        results = soup.find_all('whiteboard')
        if results and 'respin_job_' in results[0].text:
            return cls.cancel_job(task_spec)

        raise RuntimeError('The job does not exist or does not have "respin_job_" in its '
                           'whiteboard, so it is not considered to be a respin job that you are '
                           'allowed to cancel.')

    @classmethod
    def cancel_job(cls, task_spec):
        """Cancel a Beaker job using bkr.

        Args:
            taskspec: str, unique id of the job or recipeset, something like 'J:1234' or 'RS:1234'
        """
        cmd = ['bkr', 'job-cancel', task_spec]

        stdout, _, returncode = misc.safe_popen(cmd, stdout=subprocess.PIPE)
        LOGGER.info(stdout.strip())
        if returncode:
            raise RuntimeError(f'cancelling {task_spec} failed!')

        return task_spec

    @classmethod
    @retrying_on_exception(RuntimeError)
    def get_bkr_results(cls, task_spec):
        """Get Beaker job results for resource_id."""
        stdout, stderr, returncode = misc.safe_popen(['bkr', 'job-results', task_spec],
                                                     stdout=subprocess.PIPE,
                                                     stderr=subprocess.PIPE)
        if 'Invalid taskspec' in stderr:
            raise ValueError(f'Failed, please check if {task_spec} is valid Beaker job or '
                             f'recipeset id.')

        if returncode:
            print(stdout)
            raise RuntimeError(f'Failed getting {task_spec}')

        return stdout

    def submit_job(self, xml):
        """Submit job to Beaker using bkr or does dryrun for it.

        Args:
            xml: XML string of the resulting job we created
        """
        args = ["bkr", "job-submit"]

        if self.args.dryrun:
            args += ["--dry-run"]

        if self.args.jobowner is not None:
            args += ["--job-owner=%s" % self.args.jobowner]

        args += ["-"]
        err_strings = ["connection to beaker.engineering.redhat.com failed",
                       "Can't connect to MySQL server on"]
        xml = xml.encode('utf-8')
        stdout, stderr, retcode = misc.retry_safe_popen(
            err_strings, args, stdin_data=xml, stdin=subprocess.PIPE,
            stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        jobid = None
        for line in stdout.split("\n"):
            match = re.match(r"^Submitted: \['([^']+)'\]$", line)
            if match:
                jobid = match.group(1)
                break

        if not jobid and not self.args.dryrun:
            LOGGER.info('retcode=%s, stderr=%s', retcode, stderr)
            LOGGER.info(stdout)
            raise Exception(stderr)

        LOGGER.info('Submitted job %s (dryrun=%s)', jobid, self.args.dryrun)

        return jobid

    def filter_job_necessary_tasks(self, recipe, recipe_tasks):
        """Modify XML to only include certain tasks.

        "SELinux Custom Module Setup", "/distribution/check-install",
        "Boot test" and "/test/misc/machineinfo"
        tasks are always kept. Task named self.args.s is kept and copied
        repeat_count times. Tasks with parameter CKI_CI_ESSENTIAL set to True
        are kept.

        Arguments:
            recipe:       job recipe returned by soup.findAll('recipe')
            recipe_tasks: all tasks that were found using bs4 .findAll('task')

        Returns: None

        """
        for task in recipe_tasks:
            task_name = task['name']
            task_result = task['result']
            is_ci_essential = self.is_ci_essential_task(task)
            is_waived = self.is_task_waived(task)

            if task_name in self.ci_tasks:
                # don't remove necessary tasks like "boot test"
                pass
            elif task_result != 'Pass' and not is_waived and self.args.repeat\
                    and not is_ci_essential:
                LOGGER.info('Copying task "%s" %sx', task_name,
                            self.args.repeat)
                # if --repeat is used, then repeat failed tasks X times
                self.copy_task(task, recipe, self.args.repeat)
            else:
                # destroy any task with param "CKI_CI_ESSENTIAL" NOT true
                if not is_ci_essential:
                    task.decompose()

    @classmethod
    def copy_task(cls, task, to_where, count):
        """Copy XML task somewhere to BS soup <count> times."""
        task_copy = BS(str(task), 'xml')
        for elem in task_copy.findAll():
            if elem.name not in ['task', 'fetch', 'param', 'params']:
                elem.decompose()

        for _ in range(0, count - 1):
            to_where.append(BS(str(task_copy), 'xml'))

    @classmethod
    def _have_task_with_value(cls, task, name, value):
        """Check if task has <name> param, which is set to value."""
        for param in task.findAll('param'):
            par_name, par_value = param['name'], param['value']
            if par_name == name and par_value == value:
                # jump-out from param-loop
                return True
        return False

    @classmethod
    def is_task_waived(cls, task):
        """Check if task has CKI_WAIVED param, which is set to True."""
        return cls._have_task_with_value(task, 'CKI_WAIVED', 'True')

    @classmethod
    def is_ci_essential_task(cls, task):
        """Check if task has CKI_CI_ESSENTIAL param, which is set to True."""
        return cls._have_task_with_value(task, 'CKI_CI_ESSENTIAL', 'True')

    @classmethod
    def make_ci_essential_task(cls, task):
        """Modify an element to have CKI_CI_ESSENTIAL param set True."""
        append_soup = BS('<param name="CKI_CI_ESSENTIAL" value="True"></param>', 'xml')
        params = task.find('params')
        if params:
            params.append(append_soup)
        else:
            params = BS('<params/>', 'xml').find('params')
            params.append(append_soup)
            task.append(params)

    @classmethod
    def get_beaker_xml(cls, jobid):
        """Download Beaker XML job using HTTP GET.

        Params:
            jobid: job id to download (int)
        Returns:
            Beaker XML (str)
        """
        url = f'https://beaker.engineering.redhat.com/to_xml?taskid=J:{jobid}'

        headers = {'Private-Accept': 'application/xml'}
        response = requests.get(url, headers=headers)

        return response.content

    def repeat_manual_tasks(self, soup):
        """Copy tasks specified by -s, each recipe gets --mr copies total."""
        for recipe in soup.findAll('recipe'):
            for task in recipe.findAll('task'):
                task_name = task['name']

                if task_name in self.args.s:
                    # copy the fixed-name task X times
                    self.make_ci_essential_task(task)
                    self.copy_task(task, recipe, self.args.mr)

    def main(self):
        """Do everything according to parsed cmd-line params."""
        soup = self.read_input()

        # log down original whiteboard for tracking purposes
        w_elem = soup.find('whiteboard')
        white = w_elem.text if w_elem else ''
        LOGGER.info('Original job whiteboard is: "%s"', white)

        self.repeat_manual_tasks(soup)

        self.modify_multirecipe_tasks(soup)

        # modify all recipes to include the tasks we want only
        recipes = soup.findAll('recipe')
        for recipe in recipes:
            tasks = recipe.findAll('task')
            self.filter_job_necessary_tasks(recipe, tasks)

        # modify XML using arguments created from argument_table.yaml
        self.modify_specified_args(soup)

        # determine what host to run on
        self.select_host(recipes)

        # add reservesys task at the end
        if not self.args.noreservesys:
            self.addreservesys_task(recipes)

        # select only 1 recipe, if --recipe was used
        self.select_recipe(soup)

        data2output = soup.prettify()
        # write output and possibly submit
        self.create_output_artifact(data2output)
        if self.args.submit:
            return self.submit_job(data2output)
        return None

    @classmethod
    def addreservesys_task(cls, recipes):
        """Add reserversys task at the end of recipe."""
        reservesys = """<task name="/distribution/reservesys" role="STANDALONE">
         <params>
           <param name="RESERVE_IF_FAIL" value="True"/>
         </params>
        </task>
        """
        for recipe in recipes:
            recipe.append(BS(reservesys, 'xml'))

    def select_recipe(self, soup):
        """Remove all recipes, except the one selected."""
        if self.args.recipe:
            allowed_recipes = [int(x) for x in self.args.recipe]
            for recipe in soup.findAll('recipe'):
                # remove all recipes, except the one selected
                if int(recipe['id']) not in allowed_recipes:
                    recipe.decompose()

            # cleanup recipesets that may have ended-up empty
            for recipeset in soup.findAll('recipeSet'):
                if not recipeset.findAll('recipe'):
                    recipeset.decompose()

    @classmethod
    def destroy_hostrequires(cls, recipe):
        """Destroy hostRequires element in a given recipe."""
        try:
            recipe.findAll('hostRequires')[0].decompose()
        except IndexError:
            pass

    def select_host(self, recipes):
        """Run on the same host or force hostRequires to the host specified."""
        recipe_ids_forced = [int(arg.split(':')[0]) for arg in self.args.host]
        recipe_hosts_forced = [arg.split(':')[1] for arg in self.args.host]

        for recipe in recipes:
            if int(recipe['id']) in self.args.samehost:
                # destroy the element if it exists
                self.destroy_hostrequires(recipe)

                host = recipe['system']
            elif int(recipe['id']) in recipe_ids_forced:
                # destroy the element if it exists
                self.destroy_hostrequires(recipe)

                idx = recipe_ids_forced.index(int(recipe['id']))
                host = recipe_hosts_forced[idx]
            else:
                # don't set anything
                continue

            LOGGER.info('setting host to %s', host)
            recipe.append(BS(f'<hostRequires force="{host}">'
                             f'</hostRequires>', 'xml'))

    @classmethod
    def partition(cls, seq, key):
        """Split iterable into partitions using a key."""
        dict_result = collections.defaultdict(list)
        for item in seq:
            dict_result[key(item)].append(item)
        return dict_result

    def modify_multirecipe_tasks(self, soup):
        """Find failed complementary SERVERS/CLIENTS tasks, copy X times."""
        parts = []
        for recipeset in soup.findAll('recipeSet'):
            for recipe in recipeset.findAll('recipe'):
                for i, task in enumerate(recipe.findAll('task')):
                    # skip over tasks like Boot test
                    if task['name'] not in self.ci_tasks:
                        parts.append(MultiRecipeTask(recipe, task, i))

        parts = self.partition(parts, key=lambda x: (x.descr, x.task_position))
        for key, item_lst in parts.items():
            if not MultiRecipeTask.is_complementary_role_list(item_lst):
                LOGGER.debug('complementary tasks not found for %s', key)
                continue
            if MultiRecipeTask.has_failed_tasks(item_lst):
                for item in item_lst:
                    # add essential param, so it's copied only here
                    self.make_ci_essential_task(item.task)

                    LOGGER.info('copy %s to %s %sx',
                                key, item.recipe_id, self.args.repeat)
                    # copy all complementary tasks X times
                    self.copy_task(item.task, item.recipe, self.args.repeat)

    def modify_specified_args(self, soup):
        """Use cmd-line args constructed by .yaml to modify attrs/elements."""
        # delete <job >'s group attribute if not set
        if not self.args.group:
            path = self.extra_args['group']['css_selector']
            items = soup.select(path)
            for i, _ in enumerate(items):
                del items[i]['group']

        for arg in self.extra_args:
            value = getattr(self.args, arg, None)
            if value is not None:
                path = self.extra_args[arg]['css_selector']
                try:
                    attr_name = self.extra_args[arg]['attr_name']
                except KeyError:
                    attr_name = None

                LOGGER.info('Setting table-arg "%s" to "%s" in "%s"',
                            arg, value, path)

                # direct element text
                items = soup.select(path)
                if not attr_name:
                    for i, _ in enumerate(items):
                        items[i].string = value
                else:
                    for i, _ in enumerate(items):
                        items[i][attr_name] = value

    def read_input(self):
        """Read input (XML local or remote file) and parse it."""
        if self.args.jobid:
            # download Beaker XML using http GET
            data = self.get_beaker_xml(self.args.jobid)
        else:
            data = pathlib.Path(self.args.input).read_text('utf8')

        # parse using bs4
        return BS(data, "xml")

    def create_output_artifact(self, data2output):
        """Write output artifact."""
        if self.args.stdout:
            print(data2output)
        if self.args.output:
            pathlib.Path(self.args.output).write_text(data2output, 'utf8')


def main(args):
    """Respin a Beaker task."""
    data = yaml.safe_load(resources.read_binary(__package__,
                                                'argument_table.yaml'))
    extra_args = data['arguments']
    args = parse_args.parse_args(args, extra_args)

    beaker_respin = BeakerRespin(args, extra_args)
    return beaker_respin.main()
