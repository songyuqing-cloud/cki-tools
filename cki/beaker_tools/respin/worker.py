"""Entrypoint for the celery worker."""
from celery import Celery

from . import main

app = Celery('beaker-respin',
             broker='redis://localhost',
             backend='redis://localhost')


@app.task
def respin_beaker(args):
    """Respin a Beaker task."""
    return main.main(args)


@app.task
def cancel_job(args):
    """Cancel a Beaker job."""
    return main.BeakerRespin.conditional_cancel_job(args[0])
