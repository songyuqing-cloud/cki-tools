#! /usr/bin/python3
"""
Script to download logs from beaker recipesets.

This is a temporary script that download all the logs to process them on
the pipeline. It should not be necessary once the Restraint runner is
merged into the pipeline.
"""
import argparse
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import os
import pathlib
import xml.etree.ElementTree as ET

from cki_lib.logger import get_logger
from cki_lib.session import get_session
from rcdefinition.rc_data import parse_config_data

LOGGER = get_logger('cki.beaker_tools.download_logs')
SESSION = get_session('cki.beaker_tools.download_logs')

DOWNLOAD_WORKERS = 20

BEAKER_URL = os.environ.get('BEAKER_URL')


class Beaker:
    """Beaker interface."""

    def __init__(self, url):
        """Initialize a Beaker interface."""
        self.url = url

    def get_recipe_ids(self, recipeset_id):
        """Get a list of recipe_id from a recipeset."""
        url = f'{self.url}/recipesets/{recipeset_id}'
        response = SESSION.get(url)
        recipes = response.json()['machine_recipes']

        return [recipe['id'] for recipe in recipes]

    def get_recipe_xml(self, recipe_id, save_dir=None):
        """Fetch and return the recipe XML.

        The XML is returned as an ElementTree. When save_dir is given,
        additionally store the XML as recipe_id.xml at that location.
        """
        url = f'{self.url}/recipes/{recipe_id}.xml'
        response = SESSION.get(url)

        if save_dir:
            path = f'{save_dir.rstrip("/")}/{recipe_id}.xml'
            pathlib.Path(os.path.dirname(path)).mkdir(
                parents=True, exist_ok=True)
            pathlib.Path(path).write_bytes(response.content)

        return ET.fromstring(response.content)


class File:
    """Downloadable Beaker file."""

    def __init__(self, url, destination_path):
        """Initialize the object.

        url: download URL
        destination path: base directory for saving the file, the complete file
          path will also include the path component from the URL
        """
        self.url = url
        self.destination_path = destination_path
        self.content = None

    def download(self, dry_run=False):
        """Download. Handles retries."""
        LOGGER.debug('Downloading %s', self.url)

        if dry_run:
            return

        response = SESSION.get(self.url)
        self.content = response.content

    def save(self, dry_run=False):
        """Save file."""
        LOGGER.debug('Saving %s on %s', self.url, self.path)

        if dry_run:
            return

        pathlib.Path(self.directory).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.path).write_bytes(self.content)

    @property
    def path(self):
        """Return the complete destination file path."""
        return os.path.join(self.destination_path, self.url.lstrip(BEAKER_URL))

    @property
    def directory(self):
        """Return the directory component of the destination file path."""
        return os.path.dirname(self.path)

    def __str__(self):
        """Return the URL as the string representation of the object."""
        return self.url


def process_rc(rc_data, destination_path, dry_run=False):
    # pylint: disable=too-many-locals
    """Process RC file and download the test logs."""
    beaker = Beaker(BEAKER_URL)
    save_dir = destination_path if not dry_run else None
    logs_destination = os.path.join(destination_path, 'recipes')

    recipesets = rc_data['state'].get('recipesets', '').split()

    if not recipesets:
        LOGGER.error('No recipesets found')
        return

    with ThreadPoolExecutor(max_workers=DOWNLOAD_WORKERS) as executor:
        future_to_file = {}

        for recipeset in recipesets:
            recipeset_id = recipeset.split(':')[1]
            recipe_ids = beaker.get_recipe_ids(recipeset_id)
            for recipe_id in recipe_ids:
                recipe_xml = beaker.get_recipe_xml(
                    recipe_id, save_dir=save_dir)
                for log in recipe_xml.iter('log'):
                    file = File(log.get('href'), logs_destination)
                    future_to_file[executor.submit(
                        file.download, dry_run)] = file

        for future in as_completed(future_to_file):
            file = future_to_file[future]
            try:
                future.result()
                file.save(dry_run=dry_run)
            # pylint: disable=broad-except
            except Exception as exc:
                LOGGER.error('Failed: %s - %s', exc, file)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--rc', default='rc',
                        help='rc file path.')
    parser.add_argument('--destination-path', default='beaker_logs',
                        help='Where to store the logs.')
    parser.add_argument('--dry-run', action='store_true',
                        help='Do not download files, just print file names.')

    return parser.parse_args()


def main():
    """Run CLI interface."""
    arguments = parse_args()

    rc_data = parse_config_data(pathlib.Path(arguments.rc).read_text())

    if arguments.dry_run:
        LOGGER.warning('DRY RUN. Will not download the files.')

    process_rc(rc_data, arguments.destination_path, dry_run=arguments.dry_run)


if __name__ == '__main__':
    main()
