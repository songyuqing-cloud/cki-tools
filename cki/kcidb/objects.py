# pylint: disable=too-many-lines
"""KCIDB Objects."""
import datetime
from email.utils import parseaddr
from functools import lru_cache
import hashlib
import os
import pathlib
import re
from urllib.parse import urlparse

from cached_property import cached_property
from cki_lib.logger import get_logger
from cki_lib.misc import strtobool
from cki_lib.session import get_session
import dateutil
import dateutil.parser
import gitlab.exceptions
import gitlab.v4.objects
import koji
from rcdefinition.rc_data import parse_config_data
import requests.exceptions

from cki.beaker_tools import utils as beaker_utils
from cki.kcidb.utils import _get_original_pipeline
from cki.kcidb.utils import is_empty
from cki.kcidb.utils import parse_selftests
from cki.kcidb.utils import timestamp_to_iso
from cki.kcidb.utils import upload_coverage
from cki.kcidb.utils import upload_file

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)

COPR_SERVER = os.environ.get('COPR_SERVER_URL')


class GitlabPipeline:
    """Gitlab Pipeline class with cache."""

    def __init__(self, pipeline):
        """Create the object."""
        self.pipeline = pipeline

    def __getattr__(self, name):
        """Try getting attributes from here, fallback to the Pipeline instance."""
        try:
            return self.__dict__[name]
        except KeyError:
            return getattr(self.pipeline, name)

    @cached_property
    def variables(self):
        """Return dict with Pipeline variables."""
        return {var.key: var.value for var in self.pipeline.variables.list(all=True)}

    @cached_property
    def jobs(self):
        """Return pipeline jobs."""
        return self.pipeline.jobs.list(all=True, include_retried=True)

    @lru_cache(maxsize=None)
    def get_job(self, stage=None, arch=None, name=None, job_id=None, status='success',
                reverse=True):
        # pylint: disable=too-many-arguments
        """
        Return pipeline job matching the requested parameters.

        Always return the last one in case there are more than one executions
        of a job that match the search.

        Parameters:
            - stage: Stage where the job is located.
            - arch: Architecture of the job. Will be matched against the job name.
            - name: Exact job name.
            - status: Job status.
            - reverse: Return the latest job
        """
        jobs = sorted(self.jobs, key=lambda job: job.id, reverse=reverse)
        for job in jobs:
            if job_id and job.id != job_id:
                continue
            if stage and job.stage != stage:
                continue
            if name and job.name != name:
                continue
            if arch and arch not in job.name:
                continue
            if status and job.status != status:
                continue

            return GitlabJob(job)

        return None

    @cached_property
    def project(self):
        """Get pipeline's project."""
        return self.pipeline.manager.gitlab.projects.get(self.pipeline.project_id)

    @cached_property
    def pipeline_data(self):
        """Return the pipeline data."""
        self.pipeline.refresh()
        return {
            'id': self.pipeline.id,
            'variables': self.variables,
            'started_at': self.pipeline.started_at,
            'created_at': self.pipeline.created_at,
            'finished_at': self.pipeline.finished_at,
            'duration': self.pipeline.duration,
            'ref': self.pipeline.ref,
            'sha': self.pipeline.sha,
            'project': {
                'id': self.project.id,
                'path_with_namespace': self.project.path_with_namespace,
                'instance_url': self.instance_url,
            },
        }

    @property
    def instance_url(self):
        """Return the URL of the Gitlab instance."""
        url = urlparse(self.web_url)
        return f'{url.scheme}://{url.netloc}'

    @property
    def artifacts_path(self):
        """
        Get the artifacts path for this pipeline.

        Returns a relative path that should be prepended to the bucket destination path.
        """
        date = dateutil.parser.parse(self.created_at)
        return f'{date.year}/{date.month:02d}/{date.day:02d}/{self.id}'


class GitlabJob:
    """Gitlab Job class with cache."""

    def __init__(self, job):
        """Create the object."""
        self.job = self.as_project_job(job)

    def __getattr__(self, name):
        """Try getting attributes from here, fallback to the Job instance."""
        try:
            return self.__dict__[name]
        except KeyError:
            return getattr(self.job, name)

    @lru_cache(maxsize=None)
    def get_artifact(self, name):
        """Return an artifact."""
        return self.job.artifact(name)

    @staticmethod
    def as_project_job(job):
        """
        Turn ProjectPipelineJob into ProjectJob.

        ProjectJob does not have the artifact atribute.
        """
        if isinstance(job, gitlab.v4.objects.ProjectPipelineJob):
            project = job.manager.gitlab.projects.get(job.project_id, lazy=True)
            return project.jobs.get(job.id)

        return job


class Job:
    """Base Job class."""

    def __init__(self, pipeline: GitlabPipeline, job: GitlabJob):
        """Create the object."""
        self.pipeline = pipeline
        self.job = job

    @property
    def visibility(self):
        """Return the artifact visibility."""
        return self.pipeline.variables.get('artifacts_visibility', 'private')

    @cached_property
    def rc(self):  # pylint: disable=invalid-name
        """Return rc file content as a dict."""
        rc_content = self.job.get_artifact('rc')
        return parse_config_data(rc_content)

    @cached_property
    def rc_state(self):
        """Return the state section of the rc file."""
        try:
            return self.rc['state']
        except (gitlab.exceptions.GitlabGetError, requests.exceptions.ChunkedEncodingError):
            return {}

    def upload_file(self, file_name, file_content=None, source_path=None):
        """Call cki.kcidb.utils.upload_file with correct bucket."""
        return upload_file(self.visibility, self.artifacts_path, file_name,
                           file_content, source_path)

    def _download_file(self, url):
        """
        Download a file.

        If url points to Gitlab artifact, use Gitlab client to reuse access token.
        Otherwise just a plain requests.get call.
        """
        # Relative path to job artifacts, needs to be on the same job.
        if not re.match(r'^http(s)?://.*', url):
            return self.job.artifact(url)

        # Absolute path to job artifacts, could be pointing to a different job.
        gitlab_artifacts_regex = (
            r'^https://gitlab.com/api/v4/projects/'
            r'\d+/jobs\/(\d+)\/artifacts(?:\/raw)?\/(.*)'
        )
        if re.match(gitlab_artifacts_regex, url):
            job_id, file_name = re.findall(gitlab_artifacts_regex, url)[0]
            job = self.pipeline.get_job(job_id=int(job_id), status=None)
            return job.artifact(file_name)

        # Fallback to generic URL.
        request = SESSION.get(url)
        request.raise_for_status()
        return request.content

    def _reupload_file(self, key):
        """Download a file from the rc_state and upload it to s3 storage."""
        file_url = self.rc_state.get(key)
        if not file_url:
            LOGGER.debug('Key %s not found in the rc file', key)
            return None, None

        name = pathlib.Path(file_url).name
        content = self._download_file(file_url)
        uploaded_url = self.upload_file(name, file_content=content)

        return name, uploaded_url

    def upload_artifacts(self):
        # pylint: disable=no-self-use
        """Upload all artifact files and update instance variables."""
        return NotImplementedError()

    @cached_property
    def job_data(self):
        """Return the job data."""
        tag = self.rc_state.get('tag')
        self.rc_state.get('tag')
        return {
            'id': self.job.id,
            'name': self.job.name,
            'stage': self.job.stage,
            'started_at': self.job.started_at,
            'created_at': self.job.created_at,
            'finished_at': self.job.finished_at,
            'duration': self.job.duration,
            'test_hash': self.rc_state.get('test_hash'),
            'tag': str(tag) if tag else None,
            'commit_message_title': self.rc_state.get('commit_message_title'),
            'kernel_version': self.rc_state.get('kernel_version'),
        }

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return NotImplementedError()

    def _render(self):
        # pylint: disable=no-self-use
        """Return object data as json."""
        return NotImplementedError()

    def render(self):
        # pylint: disable=no-member
        """Return object data removing empty keys."""
        data = self._render().copy()
        misc = {
            'job': self.job_data,
            'pipeline': self.pipeline.pipeline_data,
        }
        data.setdefault('misc', {}).update(misc)
        data['origin'] = 'redhat'
        return {
            key: value for key, value in data.items() if not is_empty(value)
        }


class Checkout(Job):
    """KCIDB Checkout."""

    resource_name = 'checkout'

    def __init__(self, pipeline, job):
        """Create the object."""
        super().__init__(pipeline, job)
        self.log_url = None

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return self.pipeline.artifacts_path

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'id': self.id,
            'tree_name': self.pipeline.variables['name'],
            'git_repository_url': self.pipeline.variables['git_url'],
            'git_repository_branch': self.pipeline.variables['branch'],
            'git_commit_hash': self.pipeline.variables['commit_hash'],
            'git_commit_name': None,
            'patchset_files': self.patchset_files,
            'patchset_hash': self.patchset_hash,
            'message_id': self.pipeline.variables.get('message_id'),
            'comment': None,
            'start_time': self.pipeline.created_at,
            'contacts': self.contacts,
            'log_url': self.log_url,
            'valid': self.valid,
            'misc': self.misc,
        }

    def upload_artifacts(self):
        """Upload all artifacts.

        Log url is the URL of the log file of the attempt to construct this
        checkout.
        """
        log_url = self.rc_state.get('mergelog')
        if not log_url:
            return
        try:
            log_content = self.job.artifact(log_url)
        except gitlab.exceptions.GitlabGetError:
            # No mergelog even though it's in the rc file
            return

        self.log_url = self.upload_file('merge.log', file_content=log_content)

    @property
    def patchset_files(self):
        """List of mboxes containing patches applied."""
        return [
            {
                'url': url,
                'name': re.search("([^/]+)/?$", urlparse(url).path)[1]
            } for url in self.pipeline.variables.get('patch_urls', '').split()
        ]

    @property
    def contacts(self):
        """List of e-mail addresses of contacts concerned with this checkout."""
        return []

    @property
    def valid(self):
        """Return true if the pieces could be combined."""
        return self.rc_state['stage_merge'] == 'pass'

    @property
    def misc(self):
        """Miscellaneous extra data about the checkout."""
        return {}

    @cached_property
    def id(self):
        # pylint: disable=invalid-name
        """Checkout ID."""
        return f'redhat:{self.pipeline.id}'

    @cached_property
    def patchset_hash(self):
        """Return patchset_hash value."""
        patch_hash_list = []
        for patch_url in self.pipeline.variables.get('patch_urls', '').split():
            # Transform /mbox/ url into /raw/ to get the patch diff only.
            # Patchwork mbox includes headers that can change after people reply to the patches.
            patch_url = re.sub(r'/mbox/?$', '/raw/', patch_url)

            patch = SESSION.get(patch_url)
            patch_hash_list.append(
                hashlib.sha256(patch.content).hexdigest()
            )

        if not patch_hash_list:
            return None

        merged_hashes = '\n'.join(patch_hash_list) + '\n'
        return hashlib.sha256(merged_hashes.encode('utf-8')).hexdigest()


class Build(Job):
    """KCIDB Build."""

    resource_name = 'build'

    def __init__(self, pipeline, job):
        """Create the object."""
        super().__init__(pipeline, job)
        self.output_files = []
        self.log_url = None
        self.config_url = None

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.checkout.artifacts_path}/build_{self.architecture}_{self.id}'

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'checkout_id': self.checkout.id,
            'id': self.id,
            'comment': None,
            'start_time': self.start_time,
            'duration': self.rc_state.get('build_time'),
            'architecture': self.architecture,
            'command': self.command,
            'compiler': self.rc_state['compiler'],
            'input_files': self.input_files,
            'output_files': self.output_files,
            'config_name': 'fedora',
            'config_url': self.config_url,
            'log_url': self.log_url,
            'valid': self.valid,
            'misc': self.misc,
        }

    @cached_property
    def start_time(self):
        """Return the start time."""
        return self.rc.get('build', {}).get('start_time')

    def upload_artifacts(self):
        """Upload build artifacts and populate the object fields."""
        # Upload and populate output_files
        file_url_keys = (
            'kernel_package_url',
            'selftests_url',
            'selftests_buildlog_url'
        )

        for key in file_url_keys:
            if not self.rc_state.get(key):
                continue
            try:
                name, url = self._reupload_file(key)
            except requests.exceptions.HTTPError:
                LOGGER.exception("Error while uploading key %s", key)
                continue

            output_file = {'name': name, 'url': url}
            self.output_files.append(output_file)
            # For selftest logs, also add this output file to every selftest
            # object's output_files so that it only gets reuploaded once
            # This is because all selftests share a single log file
            if key == 'selftests_buildlog_url':
                for selftest in self.selftests:
                    selftest.output_files.append(output_file)

        # Upload and populate log_url
        _, self.log_url = self._reupload_file('buildlog')

        # Upload and populate config_url
        _, self.config_url = self._reupload_file('kernel_config_url')

    @cached_property
    def selftests(self):
        """Parse selftests into KCIDB tests if present."""
        selftests = []
        selftest_url = self.rc_state.get('selftest_subsets_retcodes_url')
        if not selftest_url:
            return []

        job_id, file_name = re.findall(r'jobs\/(\d+)\/artifacts(?:\/raw)?\/(.*)', selftest_url)[0]
        job = self.pipeline.get_job(job_id=int(job_id), status=None)
        selftest_results = job.artifact(file_name).decode()

        for test_data in parse_selftests(selftest_results, self.architecture):
            test = KSelfTest(self.pipeline, self.job, test_data)
            selftests.append(test)

        return selftests

    @cached_property
    def checkout(self):
        """Get the built checkout."""
        merge_job = self.pipeline.get_job(stage='merge', status='success')
        if not merge_job:
            raise Exception('No merge job found for this pipeline')

        checkout = Checkout(self.pipeline, merge_job)
        return checkout

    @property
    def architecture(self):
        """Build architecture."""
        return self.rc_state['kernel_arch']

    @property
    def id(self):
        # pylint: disable=invalid-name
        """Return the build ID."""
        job_id = self.rc["build"]["job_id"]
        return f'redhat:{job_id}'

    @property
    def command(self):
        """Full shell command line used to make the build."""
        return self.rc["build"].get("command")

    @property
    def input_files(self):
        """Return the input files."""
        return None

    @property
    def valid(self):
        """Return true if it could be built."""
        return self.rc_state['stage_build'] == 'pass'

    @property
    def misc(self):
        """Miscellaneous extra data about the build."""
        return {}


class Test(Job):
    """KCIDB Test."""

    resource_name = 'test'
    output_files_ignore = [
        'anaconda.log',
        'boot.log',
        'DeBug',
        'dnf.librepo.log',
        'hawkey.log',
        'ifcfg.log',
        'journal.xml',
        'ks.cfg',
        'lvm.log',
        'messages',
        'packaging.log',
        'program.log',
        'storage.log',
        'sys.log',
        'systemd_journal.log',
        'zipl.conf',
    ]

    def __init__(self, pipeline, job, test_info):
        """Create the object."""
        super().__init__(pipeline, job)
        self.test_info = test_info
        self.output_files = []

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.build.artifacts_path}/tests'

    def _render(self):
        """Return the KCIDB compatible data."""
        # put coverage files on the root path
        if strtobool(self.test_info.get('CKI_COVERAGE_TGZ', 'false')):
            return {
                'build_id': self.build.id,
                'id': self.id,
                'path': '',
                'output_files': self.output_files,
            }

        return {
            'build_id': self.build.id,
            'id': self.id,
            'environment': self.environment,
            'path': self.test_info.get('CKI_UNIVERSAL_ID'),
            'comment': self.test_info.get('CKI_NAME'),
            'status': self.status,
            'waived': self.waived,
            'start_time': self.start_time,
            'duration': self.duration,
            'output_files': self.output_files,
            'misc': self.misc,
        }

    @staticmethod
    def _should_ignore_file(file):
        """
        Return True if it should not be uploaded.

        Using self.output_files_ignore, try to match the file name with
        the files listed to ignore.
        """
        return any(name in file['name'] for name in Test.output_files_ignore)

    def upload_artifacts(self):
        """Upload a list of test outputs: logs, dumps, etc."""
        for file in self.test_info['output_files']:
            if self._should_ignore_file(file):
                LOGGER.debug('Not uploading file, ignored: %s', file['name'])
                continue

            url = None
            if strtobool(self.test_info.get('CKI_COVERAGE_TGZ', 'false')):
                if file['name'].endswith('.tgz'):
                    url = self.upload_coverage(file)
            else:
                url = self.upload_file(file['path'], source_path=file['url'])

            if url:
                self.output_files.append(
                    {'name': file['name'],
                     'url': url}
                )

    def upload_coverage(self, file):
        """Unpack coverage tgz and return url of index.html."""
        return upload_coverage(self.visibility, self.artifacts_path, file)

    @property
    def start_time(self):
        """Start start time as ISO."""
        # Test aborted
        if not self.test_info.get('start_time'):
            return None
        return timestamp_to_iso(self.test_info['start_time'])

    @property
    def duration(self):
        """Return duration in seconds."""
        start_time = self.test_info.get('start_time')
        finish_time = self.test_info.get('finish_time')

        if not finish_time:
            # Test is still running.
            return None

        return (
            dateutil.parser.parse(finish_time) -
            dateutil.parser.parse(start_time)
        ).total_seconds()

    @cached_property
    def build(self):
        """Get the tested build."""
        # Beaker arch is 'arch', UMB results arch is 'test_arch'
        arch = self.test_info.get('arch') or self.test_info.get('test_arch')

        # Beaker uses i386 to test i686 builds.
        # Replace it with i686 so it can be linked to the build job.
        if arch == 'i386':
            arch = 'i686'

        if self.is_debug:
            arch += '_debug'

        # If 'ARTIFACT_URL_{arch}' is present, we're using a build from another pipeline.
        artifacts_url = self.pipeline.variables.get(f'ARTIFACT_URL_{arch}')
        if artifacts_url:
            job_id = int(re.findall(r'jobs/(\d+)/artifacts', artifacts_url)[0])

            job = GitlabJob(self.pipeline.project.jobs.get(job_id))
            pipeline = GitlabPipeline(self.pipeline.project.pipelines.get(job.pipeline['id']))

            return Build(pipeline, job)

        job = self.pipeline.get_job(stage='build', arch=arch, status='success')
        if not job:
            raise Exception(f'Build for {arch} not found.')

        return Build(self.pipeline, job)

    @property
    def id(self):
        # pylint: disable=invalid-name
        """ID of the test run."""
        return f'redhat:{self.test_info["task_id"]}'

    @property
    def environment(self):
        """Return the environment the test ran in."""
        return {'comment': self.test_info['system']}

    @property
    def status(self):
        """Return the test status, one of the following.

        ("ERROR", "FAIL", "PASS", "DONE", "SKIP").
        """
        return beaker_utils.decode_test_result(
            self.test_info['status'],
            self.test_info['result'],
            self.test_info['skipped']
        )

    @property
    def waived(self):
        """Is this test waived?. Look for CKI_WAIVED key."""
        return self.test_info.get('CKI_WAIVED') is not None

    @property
    def targeted(self):
        """Is this test targeted?."""
        if not self.rc_state.get('targeted_tests'):
            return False
        list_path = self.rc_state['targeted_tests_list']
        tests = self.job.get_artifact(list_path)
        tests_list = tests.decode().splitlines()

        return self.test_info.get('CKI_NAME') in tests_list

    @property
    def finish_time(self):
        """Test finish_time."""
        finish_time = self.test_info.get('finish_time')

        # Test is still running.
        if not finish_time:
            return None

        return timestamp_to_iso(finish_time)

    @property
    def is_debug(self):
        """Return true if this test ran on a debug kernel."""
        return strtobool(self.rc_state.get('debug_kernel', 'false'))

    @property
    def maintainers(self):
        """Return list of maintainers."""
        if not self.test_info.get('CKI_MAINTAINERS'):
            return []

        maintainers = []
        for maintainer in self.test_info['CKI_MAINTAINERS'].split(','):
            if '/' in maintainer:
                name_and_email, gitlab_username = maintainer.split(' / ')
            else:
                name_and_email, gitlab_username = maintainer, None

            name, email = parseaddr(name_and_email)
            maintainers.append(
                {
                    'name': name, 'email': email, 'gitlab': gitlab_username
                }
            )

        return maintainers

    @property
    def misc(self):
        """Miscellaneous extra data about the test."""
        return {
            'debug': self.is_debug,
            'targeted': self.targeted,
            'fetch_url': self.test_info['fetch_url'],
            'maintainers': self.maintainers,
            'beaker': {
                'task_id': self.test_info['task_id'],
                'recipe_id': self.test_info['recipe_id'],
                'finish_time': self.finish_time,
                'retcode': self.rc_state['retcode'],
            }
        }


class BrewJob(Job):
    """Brew job base class."""

    @property
    def checkout_id(self):
        """Checkout ID."""
        return f'redhat:{self.pipeline.id}'

    @property
    def git_commit_hash(self):
        """Get fake git_commit_hash."""
        task_hash = hashlib.sha1(str(self.task_id).encode()).hexdigest()
        return task_hash

    @property
    def task_id(self):
        """Return task id."""
        # Brew task
        if self.brew_task:
            return self.brew_task['id']

        # COPR task
        if self.copr_task:
            return self.copr_task['build_id']

        raise Exception('Unhandled brew job')

    @cached_property
    def brew_task(self):
        """Get brew task info."""
        if not self.pipeline.variables.get('brew_task_id'):
            # Not a Brew task
            return None

        session = koji.ClientSession(self.pipeline.variables['server_url'])
        return session.getTaskInfo(self.pipeline.variables['brew_task_id'], request=True)

    @cached_property
    def copr_task(self):
        """Get COPR task info."""
        if not self.pipeline.variables.get('copr_build'):
            # Not a COPR task
            return None

        build_id = self.pipeline.variables['copr_build']
        name = self.pipeline.variables['name']
        # COPR pipelines always have a single arch, architectures should not be a list
        arch = self.pipeline.variables['architectures']

        url = f'{COPR_SERVER}/api_2/build_tasks/{build_id}/{name}-{arch}'
        return SESSION.get(url).json()['build_task']


class BrewCheckout(BrewJob):
    """Fake checkout for Brew Job."""

    resource_name = 'checkout'

    @property
    def submitter(self):
        """Return the brew task submitter."""
        return self.pipeline.variables.get('submitter')

    def _render(self):
        """Return json data."""
        return {
            'id': self.checkout_id,
            'git_commit_hash': self.git_commit_hash,
            'valid': True,
            'contacts': [
                self.submitter
            ],
            'start_time': self.pipeline.created_at,
            'tree_name': self.pipeline.variables['name'],
        }


class BrewBuild(BrewJob):
    """Fake build for Brew Job."""

    resource_name = 'build'

    def _render(self):
        """Return json data."""
        return {
            'checkout_id': self.checkout_id,
            'id': self.id,
            'architecture': self.architecture,
            'start_time': self.start_time,
            'duration': self.duration,
            'valid': True,
        }

    @property
    def is_debug(self):
        """Return true if this is a debug kernel."""
        return strtobool(self.rc_state.get('debug_kernel', 'false'))

    @property
    def id(self):
        # pylint: disable=invalid-name
        """Build ID."""
        return f'redhat:{self.task_id}_{self.architecture}'

    @property
    def architecture(self):
        """Build architecture."""
        arch = self.rc_state['kernel_arch']

        if self.is_debug:
            arch += '_debug'

        return arch

    @property
    def start_time(self):
        """Build start timestamp."""
        if self.brew_task:
            return timestamp_to_iso(self.brew_task['start_time'])

        if self.copr_task:
            return datetime.datetime.fromtimestamp(
                self.copr_task['started_on'],
                dateutil.tz.UTC
            ).isoformat()

        return None

    @property
    def duration(self):
        """Build duration."""
        if self.brew_task:
            return int(self.brew_task['completion_ts'] - self.brew_task['start_ts'])

        if self.copr_task:
            return self.copr_task['ended_on'] - self.copr_task['started_on']

        return None


class BrewTest(BrewJob, Test):
    """Brew Test."""

    @property
    def artifacts_path(self):
        """Relative path where to put the files in the storage."""
        return f'{self.pipeline.artifacts_path}/{self.rc_state["kernel_arch"]}'

    @property
    def build(self):
        # blub pylint: disable=invalid-overridden-method
        """Return the build."""
        return BrewBuild(self.pipeline, self.job)


class KSelfTest(Test):
    """KSelfTest result."""

    def __init__(self, pipeline, job, test_info):
        """Create the object."""
        super().__init__(pipeline, job, test_info)
        self.output_files = []

    def _render(self):
        """Return the KCIDB compatible data."""
        return {
            'build_id': self.build.id,
            'id': self.id,
            'path': self.test_info.get('CKI_UNIVERSAL_ID'),
            'comment': self.test_info.get('CKI_NAME'),
            'status': self.status,
            'start_time': self.build.start_time,
            'output_files': self.output_files,
            'waived': False,
        }

    @property
    def id(self):
        """ID of the kselftest run."""
        return f'{self.build.id}_{self.test_info.get("test_index")}'

    @property
    def status(self):
        """Return the kselftest status, either FAIL or PASS."""
        return "PASS" if self.test_info['return_code'] == 0 else "FAIL"


class UMBTest(Test):
    """UMB test result."""

    def __init__(self, pipeline, job, test_info):
        """Create the object."""
        super().__init__(pipeline, job, test_info)
        self.output_files = []
        for file in self.test_info['test_log_url']:
            self.output_files.append(
                {'name': pathlib.Path(file).name,
                 'url': file}
            )

        # Replace pipeline with original_pipeline.
        original_pipeline = _get_original_pipeline(pipeline)
        self.umb_pipeline = self.pipeline
        self.pipeline = GitlabPipeline(original_pipeline)

    def _render(self):
        """Return json data."""
        return {
            'build_id': self.build.id,
            'id': self.id,
            'path': self.test_info['test_name'],
            'comment': self.test_info['test_description'],
            'status': self.status,
            'waived': strtobool(self.test_info['test_waived']),
            'output_files': self.output_files,
            'start_time': self.pipeline.started_at,
            'misc': self.misc,
        }

    @property
    def status(self):
        """Return sanitized status."""
        status = self.test_info['test_result']

        # UMB results may contain ABORT statuses, which is not kcidb compliant
        if status == 'ABORT':
            status = 'ERROR'

        return status

    def upload_artifacts(self):
        """Upload the artifacts."""

    @property
    def id(self):
        """Return the id."""
        return f'redhat:{self.umb_pipeline.id}_{self.test_info["test_index"]}'

    @property
    def is_debug(self):
        """Return true if this test runs on a debug kernel."""
        return self.test_info.get('is_debug', False)

    @property
    def misc(self):
        """Return the misc fields."""
        misc = {
            'debug': self.is_debug,
            'maintainers': [
                {
                    'name': self.umb_pipeline.variables['team_name'],
                    'email': self.umb_pipeline.variables['team_email']
                }
            ],
        }
        return misc
