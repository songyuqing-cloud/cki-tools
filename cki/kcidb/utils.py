"""Misc utility functions for kcidb."""
from concurrent.futures import ThreadPoolExecutor
import mimetypes
import os
import tarfile
import tempfile

from botocore.exceptions import ClientError
from cki_lib.gitlab import parse_gitlab_url
from cki_lib.logger import get_logger
import dateutil

from cki.cki_tools import _utils

LOGGER = get_logger(__name__)

BUCKET_PUBLIC_SPEC = os.environ.get('BUCKET_PUBLIC_SPEC')
BUCKET_PRIVATE_SPEC = os.environ.get('BUCKET_PRIVATE_SPEC')
BUCKETS = {
    'public': _utils.S3Bucket.from_bucket_string(
        BUCKET_PUBLIC_SPEC) if BUCKET_PUBLIC_SPEC else None,
    'private': _utils.S3Bucket.from_bucket_string(
        BUCKET_PRIVATE_SPEC) if BUCKET_PRIVATE_SPEC else None
}


def upload_file(visibility, artifacts_path, file_name, file_content=None,
                source_path=None):
    """
    Upload file to final storage.

    This method should decide where to put the file, and return the url
    where to access that file.
    """
    LOGGER.debug('Uploading %s', file_name)

    bucket = BUCKETS.get(visibility)
    if not bucket:
        LOGGER.debug('S3 bucket not configured. Skipping upload.')
        return None

    if not (file_content or source_path):
        LOGGER.warning('File "%s" is empty', file_name)
        return None

    file_path = os.path.join(bucket.spec.prefix, artifacts_path,
                             file_name)
    file_url = os.path.join(bucket.spec.endpoint, bucket.spec.bucket,
                            file_path)

    content_type, _ = mimetypes.guess_type(file_name, strict=False)
    content_type = content_type or 'text/plain'

    try:
        bucket.client.head_object(Bucket=bucket.spec.bucket,
                                  Key=file_path)
        LOGGER.debug('File %s already exists in remote', file_name)
    except ClientError:
        if file_content:
            bucket.client.put_object(Bucket=bucket.spec.bucket,
                                     Key=file_path, Body=file_content,
                                     ContentType=content_type)
        else:
            bucket.client.upload_file(Bucket=bucket.spec.bucket,
                                      Filename=source_path,
                                      Key=file_path,
                                      ExtraArgs={"ContentType": content_type})

    LOGGER.debug('File correctly uploaded to %s', file_url)
    return file_url


def upload_coverage(visibility, artifacts_path, file):
    """Unpack coverage tgz and return url of index.html."""
    with tempfile.TemporaryDirectory() as html_dir:
        with tarfile.open(file['url']) as tgz_file:
            tgz_file.extractall(html_dir)

        with ThreadPoolExecutor(max_workers=_utils.MAX_POOL_CONNECTIONS) as executor:
            future_index_result = None
            # need to remove html_dir and /
            strip_len = len(html_dir) + 1
            for dir_path, _, filenames in os.walk(html_dir):
                for filename in filenames:
                    source_path = os.path.join(dir_path, filename)
                    dest_path = source_path[strip_len:]
                    future = executor.submit(upload_file, visibility, artifacts_path,
                                             dest_path, source_path=source_path)
                    # expecting path like kcov_merged.YYYY-MM-DD/kcov.comb/index.html
                    if len(dest_path.split('/')) == 3 and filename == 'index.html':
                        future_index_result = future

    if future_index_result:
        return future_index_result.result()

    return None


def is_empty(value):
    """Check if value is not empty."""
    return (
        value is None or
        value == [] or
        value == {}
    )


def timestamp_to_iso(timestamp):
    """Format timestamp as ISO. Adds tzinfo=UTC if no tzinfo present."""
    date = dateutil.parser.parse(timestamp)
    if not date.tzinfo:
        date = date.replace(tzinfo=dateutil.tz.UTC)
    return date.isoformat()


def tzaware_str2utc(str_date):
    """Convert tz-aware date string to UTC zulu.

    Arguments:
        str_date: str, date string like "2021-04-13T14:33:08-04:00" or "2021-04-13 10:30:07 +0000"
    Returns:
        str, e.g. 2021-04-13T18:33:08+00:00
    """
    return dateutil.parser.parse(str_date).astimezone(dateutil.tz.UTC).isoformat()


def parse_selftests(selftest_results, architecture):
    """Get test_data for kselftests if present."""
    selftests = []
    for index, line in enumerate(selftest_results.strip().split('\n')):
        test_name, return_code = line.split(': ')
        sanitized_test_name = test_name.replace(' ', '_')
        test_data = {"CKI_NAME": f"kselftest build - {test_name}",
                     "CKI_UNIVERSAL_ID":
                     f"kselftest-build.{sanitized_test_name}",
                     "return_code": int(return_code),
                     "test_index": index,
                     "arch": architecture}
        selftests.append(test_data)

    return selftests


def _get_original_pipeline(pipeline):
    """Get the original pipeline this result points to."""
    original_pipeline_url = pipeline.variables.get('original_pipeline_url')
    if original_pipeline_url:
        _, original_pipeline = parse_gitlab_url(original_pipeline_url)
    else:
        original_pipeline = pipeline.project.pipelines.get(
            pipeline.variables['original_pipeline_id']
        )

    return original_pipeline
