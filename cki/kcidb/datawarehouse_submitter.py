#!/usr/bin/python3
"""Parse pipeline or job and get KCIDB schemed data."""
import argparse
import base64
from collections import defaultdict
import json
import lzma
import os
import pathlib
import tempfile
import zipfile

from cki_lib import gitlab
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.session import get_session
import datawarehouse
from gitlab.exceptions import GitlabGetError
from kcidb_wrap import v4
import sentry_sdk

from . import objects
from . import utils
from ..beaker_tools import process_logs

LOGGER = get_logger('cki.kcidb.datawarehouse_submitter')
SESSION = get_session('cki.kcidb.datawarehouse_submitter')

GITLAB_URL = os.environ.get('GITLAB_URL')
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN')

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGE = os.environ.get('RABBITMQ_EXCHANGE')
RABBITMQ_QUEUE = os.environ.get('RABBITMQ_QUEUE')


def download_and_unzip_artifacts(job, tmpdirname):
    """Download artifacts.zip of a job, unpack it to tmpdirname and remove downloaded file."""
    artifacts_file = pathlib.Path(f'{tmpdirname}/artifacts.zip')

    # Couldn't do this with pathlib.Path.write_bytes
    with open(artifacts_file.absolute(), 'wb') as file:
        LOGGER.info('Downloading artifacts zip.')
        job.job.artifacts(streamed=True, action=file.write)
        LOGGER.info('Finished downloading.')

    with zipfile.ZipFile(artifacts_file.absolute(), 'r') as zip_file:
        zip_file.extractall(tmpdirname)

    artifacts_file.unlink()


def get_tests_from_test_job(job):
    """Process test jobs and extract tests.

    Download job artifacts into a temporary directory, parse
    the logs and yield tests' data.
    """
    if not job.rc['state'].get('recipesets'):
        LOGGER.info('No recipesets found for job %i', job.job.id)
        return

    if not job.rc['state'].get('test_tasks_path'):
        LOGGER.info('Job %i has no test_tasks_path key in rc.', job.job.id)
        return

    LOGGER.info('Getting tests from test job.')
    with tempfile.TemporaryDirectory() as tmpdirname:
        download_and_unzip_artifacts(job, tmpdirname)

        tasks_path = os.path.join(tmpdirname, job.rc['state']['test_tasks_path'])
        tests_data = process_logs.process(tasks_path)

        for test in tests_data:
            yield test


def json2kcidb_schema(content, pipeline):
    # pylint: disable=protected-access
    """Convert json-encoded strings, fixup variables and yield kcidb schema."""
    kcidb_data = json.loads(content)
    # WORKAROUND: remove once kcidb v3->v4 migration is complete.
    checkouts = kcidb_data.get('checkouts') or kcidb_data.get('revisions', [])

    for data_dict in checkouts + kcidb_data['builds'] + kcidb_data['tests']:
        variables = pipeline.variables
        # For umb tests.
        if variables['name'] == 'umb-results':
            original_pipeline = utils._get_original_pipeline(pipeline)
            variables = objects.GitlabPipeline(original_pipeline).variables

        # WORKAROUND: we layover / shadow pipeline variables for now, because we can't
        # get everything in the pipeline.
        if 'pipeline' not in data_dict['misc'].keys():
            data_dict['misc']['pipeline'] = {}
        data_dict['misc']['pipeline']['variables'] = variables

    return kcidb_data


def process_upt_objects(pipeline, job):
    """Process kcidb pipeline objects."""
    # Don't try to get rc file from umb pipeline.
    with tempfile.TemporaryDirectory() as tmpdirname:
        try:
            download_and_unzip_artifacts(job, tmpdirname)
            # NOTE: kcidb_testplan.json is skipped / not loaded for now, we're not doing
            # incremental results yet.
            content = pathlib.Path(tmpdirname, 'artifacts/kcidb_all.json').read_text()
            yield json2kcidb_schema(content, pipeline)
        except (GitlabGetError, FileNotFoundError):
            LOGGER.debug('Could not handle job %i %s', job.job.id, job.stage)


def get_upt_objects(pipeline, jobs):
    """Process brew and non-brew jobs and return kcidb objects."""
    allowed_stages = ['merge', 'createrepo', 'build', 'build-tools', 'umb-results', 'publish',
                      'test']

    # Check only certain stages and only 'success' / 'failed' status.
    jobs = list(filter(lambda job: job.stage in allowed_stages and job.status in
                       ('success', 'failed'), jobs))
    jobs = sorted(jobs, key=lambda job: allowed_stages.index(job.stage), reverse=True)

    handled_arches = set()
    for job in jobs:
        arch = job.name.split(' ')[1] if ' ' in job.name else None
        if job.stage == 'merge' and not handled_arches:
            # We've hit the last (merge) job and no arches have submitted "checkout" type info.
            yield from process_upt_objects(pipeline, job)
        elif arch is not None and arch not in handled_arches:
            handled_arches.add(arch)
            # NOTE: setup stage may be ignored. Either there's a kcidb_all.json, or not.
            yield from process_upt_objects(pipeline, job)
        elif job.stage == 'umb-results':
            yield from process_upt_objects(pipeline, job)


def get_cki_objects(pipeline, jobs):
    """Process cki jobs and return kcidb objects."""
    if not isinstance(jobs, list):
        jobs = [jobs]

    for job in jobs:
        if job.stage == 'merge':
            checkout = objects.Checkout(pipeline, job)
            checkout.upload_artifacts()
            yield checkout
        elif job.stage in ('build', 'publish'):
            build = objects.Build(pipeline, job)
            build.upload_artifacts()
            yield build

            for selftest in build.selftests:
                yield selftest
        elif job.stage == 'test':
            # One single test job contains multiple tests.
            test_job = objects.Job(pipeline, job)
            for test_data in get_tests_from_test_job(test_job):
                test = objects.Test(pipeline, job, test_data)
                test.upload_artifacts()
                yield test

        elif job.stage == 'umb-results':
            test_job = objects.Job(pipeline, job)
            test_list = json.loads(
                lzma.decompress(
                    base64.b64decode(test_job.pipeline.variables['data'])
                ).decode()
            )
            for index, test_data in enumerate(test_list):
                test_data['test_index'] = index
                yield objects.UMBTest(pipeline, job, test_data)
        else:
            LOGGER.debug('No kcidb equivalent for %s', job.stage)


def get_brew_objects(pipeline, jobs):
    """Process brew jobs and return kcidb objects."""
    if not isinstance(jobs, list):
        jobs = [jobs]

    for job in jobs:
        if job.stage == 'test':
            # Generate BrewCheckout object from the first successful job
            # This job must not change for a pipeline, i.e. even job retries must not affect it
            first_job = pipeline.get_job(status='success', reverse=False)
            yield objects.BrewCheckout(pipeline, first_job)

            # Generate BrewBuild from setup job
            arch = job.name.split(' ')[1]
            job_setup = pipeline.get_job(name=f'setup {arch}')
            yield objects.BrewBuild(pipeline, job_setup)

            # One single test job contains multiple tests.
            test_job = objects.Job(pipeline, job)
            for test_data in get_tests_from_test_job(test_job):
                test = objects.BrewTest(pipeline, job, test_data)
                test.upload_artifacts()
                yield test


def process_jobs(pipeline, jobs, raise_on_error=True):
    """Process the jobs and return kcidb json."""
    pipeline = objects.GitlabPipeline(pipeline)
    jobs = [objects.GitlabJob(job) for job in jobs]
    kcidb_data = {'version': {'major': 4, 'minor': 0}}

    is_upt = pipeline.variables.get('test_mode') != 'skt'
    if is_upt:
        return v4.merge_kcidb_data(get_upt_objects(pipeline, jobs))

    # hack: ignore pipelines in https://gitlab.com/cki-project/aws/
    # the artifacts are not present
    is_aws = 'aws' in pipeline.project.path_with_namespace
    if is_aws:
        return kcidb_data

    is_brew_pipeline = any([
        pipeline.variables.get('brew_task_id'),
        pipeline.variables.get('copr_build'),
    ])
    if is_brew_pipeline:
        get_objects = get_brew_objects
    else:
        get_objects = get_cki_objects

    processed_data = defaultdict(list)
    for obj in get_objects(pipeline, jobs):
        try:
            data = obj.render()
        except Exception:  # pylint: disable=broad-except
            if raise_on_error:
                raise
            LOGGER.exception('Error parsing %s', obj.job.id)
            continue

        # obj.resource_name needs to be plural in the submitted data.
        processed_data[f'{obj.resource_name}s'].append(data)

    kcidb_data.update(processed_data)

    return kcidb_data


def callback(args, body):
    """Process one received job."""
    LOGGER.debug('Message Received: %s', body)
    LOGGER.info('Got message for %s - %i', body['project'], body['job_id'])

    with gitlab.get_instance(f'https://{body["gitlab_url"]}') as gitlab_instance:
        project = gitlab_instance.projects.get(body['project'])

    job = project.jobs.get(body['job_id'])
    pipeline = project.pipelines.get(job.pipeline['id'])

    kcidb_objects = process_jobs(pipeline, [job])
    handle_output(args, kcidb_objects)


def process_queue(args):
    """Handle jobs for action=queue argument choice.

    Listen to RabbitMQ queue and process received jobs.
    """
    queue = MessageQueue(
        host=args.rabbitmq_host, port=args.rabbitmq_port,
        user=args.rabbitmq_user, password=args.rabbitmq_password,
        connection_params={
            'blocked_connection_timeout': 300, 'heartbeat': 600})

    queue.consume_messages(args.rabbitmq_exchange, ['#'],
                           lambda r, b: callback(args, b),
                           queue_name=args.rabbitmq_queue,
                           prefetch_count=5)


def process_single(args):
    """Process given job or pipeline id."""
    with gitlab.get_instance(f'https://{args.gitlab_url}') as gitlab_instance:
        project = gitlab_instance.projects.get(args.project)

    if args.kind == 'pipeline':
        pipeline = project.pipelines.get(args.id)
        jobs = pipeline.jobs.list(all=True)
    else:
        job = project.jobs.get(args.id)
        pipeline = project.pipelines.get(job.pipeline['id'])
        jobs = [job]

    kcidb_objects = process_jobs(pipeline, jobs, raise_on_error=False)
    handle_output(args, kcidb_objects)


def handle_output(args, data):
    """Handle generated data according to args."""
    if not data:
        return

    json_objects = json.dumps(data)

    if args.output_file:
        pathlib.Path(args.output_file).write_text(json_objects)

    if args.output_stdout:
        print(json_objects)

    if args.push:
        dw_api = datawarehouse.Datawarehouse(args.datawarehouse_url, args.datawarehouse_token)
        dw_api.kcidb.submit.create(data=data)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Parse a Pipeline or Job and get KCIDB schemed data.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    group_output = parser.add_argument_group('Generated data destination.')
    group_output.add_argument(
        '--push', action='store_true', help='Push output data to Datawarehouse.',
        default=misc.is_production()
    )
    group_output.add_argument(
        '--datawarehouse-url', default=DATAWAREHOUSE_URL,
        help='URL to the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument(
        '--datawarehouse-token', default=DATAWAREHOUSE_TOKEN,
        help='Token for the Datawarehouse instance. Defaults to env DATAWAREHOUSE_URL.')
    group_output.add_argument('--output-file', help='Save output to a file.')
    group_output.add_argument('--output-stdout', action='store_true',
                              default=(not misc.is_production()),
                              help='Echo output to stdout.')

    subparser_action = parser.add_subparsers(dest='action', help='Action to perform.')

    parser_queue = subparser_action.add_parser(
        'queue', help='Monitor queue for new finished jobs.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_queue.add_argument('--rabbitmq-exchange', default=RABBITMQ_EXCHANGE,
                              help='Defaults to env RABBITMQ_EXCHANGE.')
    parser_queue.add_argument('--rabbitmq-queue', default=RABBITMQ_QUEUE,
                              help='Defaults to env RABBITMQ_QUEUE.')
    parser_queue.add_argument('--rabbitmq-host', default=RABBITMQ_HOST,
                              help='Defaults to env RABBITMQ_HOST.')
    parser_queue.add_argument('--rabbitmq-port', type=int, default=RABBITMQ_PORT,
                              help='Defaults to env RABBITMQ_PORT.')
    parser_queue.add_argument('--rabbitmq-user', default=RABBITMQ_USER,
                              help='Defaults to env RABBITMQ_USER.')
    parser_queue.add_argument('--rabbitmq-password', default=RABBITMQ_PASSWORD,
                              help='Defaults to env RABBITMQ_PASSWORD.')

    parser_single = subparser_action.add_parser(
        'single', help='Parse a single job or pipeline.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser_single.add_argument('kind', help='pipeline or job?', choices=('pipeline', 'job'))
    parser_single.add_argument('gitlab_url', help='Gitlab host URL.')
    parser_single.add_argument('project', help='Gitlab project. Id or url-encoded path.')
    parser_single.add_argument('id', help='Gitlab pipeline or job id.')

    return parser.parse_args()


def main():
    """CLI Interface."""
    args = parse_args()

    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()

    if args.action == 'queue':
        process_queue(args)
    elif args.action == 'single':
        process_single(args)


if __name__ == '__main__':
    main()
