"""
Monitor queues and scale up/down services.

This script is inteded to monitor services consuming rabbitmq queues and
scale up or down the replicas depending on the amount of queued messages.
"""
from cki_lib import messagequeue
from cki_lib import misc
from cki_lib.logger import get_logger
import openshift as oc

LOGGER = get_logger(__name__)
RABBITMQ_KEEPALIVE_S = misc.get_env_int('RABBITMQ_KEEPALIVE_S', 60)


class Queue:
    # pylint: disable=too-few-public-methods
    """Represents a service queue."""

    messagequeue = messagequeue.MessageQueue(keepalive_s=RABBITMQ_KEEPALIVE_S)

    def __init__(self, name):
        """Initialize queue."""
        self.name = name

        self.message_count = None
        self.consumer_count = None

    def update_data(self):
        """Update data from server."""
        LOGGER.debug('%s: Updating queue', self.name)
        with self.messagequeue.connect() as channel:
            declare = channel.queue_declare(self.name, passive=True)

        self.message_count = declare.method.message_count
        self.consumer_count = declare.method.consumer_count
        LOGGER.debug(
            '%s: %d messages, %d consumers',
            self.name, self.message_count, self.consumer_count
        )


class DeploymentConfig:
    # pylint: disable=too-few-public-methods
    """Deployment config."""

    def __init__(self, name, namespace):
        """Init."""
        self.name = name
        self.namespace = namespace

    def _setup(self):
        """Config oc client."""
        LOGGER.debug('%s: setting up oc', self.name)
        oc.project(self.namespace)
        oc.timeout(60)

    def scale(self, count):
        """Scale deployment config."""
        LOGGER.debug('%s: scaling to %d', self.name, count)
        oc.invoke('scale', [f'dc/{self.name}', f'--replicas={count}'])

    @property
    def exists(self):
        """Return True if this dc exists in the current cluster."""
        try:
            oc.invoke('get', [f'dc/{self.name}'])
        except Exception:  # pylint: disable=broad-except
            return False
        return True

    @property
    def replicas(self):
        """Return the number of replicas in the DC."""
        return oc.selector(f'dc/{self.name}').object().model.spec.replicas


class Application:
    """Application to monitor."""

    def __init__(self, name, config):
        """Initialize service."""
        self.name = name
        self.config = config
        self.queue = Queue(self.config['queue'])
        self.deployment_config = DeploymentConfig(self.config['deployment_config'],
                                                  self.config['namespace'])

    def evaluate(self, message_count, consumer_count):
        """Decide if scaling up or down is necessary."""
        current_capacity = consumer_count * self.config['messages_per_replica']
        LOGGER.debug('%s: current_capacity: %d', self.name, current_capacity)

        # Decide new replicas count
        if message_count > current_capacity:
            LOGGER.debug('%s: should scale up', self.name)
            new_replicas_count = min(
                # Expected number of replicas according to the number of messages
                message_count // self.config['messages_per_replica'] + 1,
                # Max number of replicas according to the max_scale_up_step
                consumer_count + self.config['max_scale_up_step'],
            )
        elif message_count < current_capacity:
            LOGGER.debug('%s: should scale down', self.name)
            new_replicas_count = max(
                # Expected number of replicas according to the number of messages
                message_count // self.config['messages_per_replica'] + 1,
                # Min number of replicas according to the max_scale_down_step
                consumer_count - self.config['max_scale_down_step'],
            )
        else:
            new_replicas_count = consumer_count

        # These are hard limits, check them totally at the end in case anything
        # goes wrong in the control loop
        if new_replicas_count > self.config['replicas_max']:
            new_replicas_count = self.config['replicas_max']
        if new_replicas_count < self.config['replicas_min']:
            new_replicas_count = self.config['replicas_min']

        return new_replicas_count

    def check(self):
        """Run the checks and scale up/down if necessary."""
        if not self.deployment_config.exists:
            LOGGER.info('dc/%s does not exist', self.deployment_config.name)
            return

        self.queue.update_data()
        consumer_count = self.deployment_config.replicas

        new_replicas_count = self.evaluate(
            self.queue.message_count, consumer_count
        )

        if new_replicas_count == consumer_count:
            LOGGER.debug('%s: replica count not changed', self.name)
            return

        LOGGER.info(
            'scaling %s application=%s new_replicas=%d messages_count=%d current_replicas=%d',
            'up' if new_replicas_count > consumer_count else 'down',
            self.name,
            new_replicas_count,
            self.queue.message_count,
            consumer_count
        )
        if misc.is_production():
            self.deployment_config.scale(new_replicas_count)
