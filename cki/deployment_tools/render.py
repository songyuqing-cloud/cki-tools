"""Render templates."""
import argparse
import itertools
import os
import pathlib
import re
import sys

from cki_lib import misc
from cki_lib import session
import jinja2
import yaml

SESSION = session.get_session('cki.deployment_tools.render')


class Render:
    """Render a template via jinja2."""

    def __init__(self, template_file, *, data_paths=None, raw_data_paths=None, include_path=None):
        """Create an instance."""
        self.data_paths = data_paths
        self.raw_data_paths = raw_data_paths
        self.include_path = include_path

        self.env = jinja2.Environment(loader=jinja2.FunctionLoader(self.load_template),
                                      undefined=jinja2.StrictUndefined,
                                      keep_trailing_newline=True)
        self.env.filters['env'] = Render.filter_env
        self.env.filters['is_true'] = misc.strtobool
        self.env.filters['from_yaml'] = yaml.safe_load
        self.env.globals['url'] = Render.global_url
        self.env.globals['env'] = os.environ

        self.data_file_contents = {
            '__template': template_file.read(),
        }
        self.data = {}

    @staticmethod
    def global_url(url, binary=False, json=False):
        """Try to get a file via requests."""
        response = SESSION.get(url)
        response.raise_for_status()
        return response.content if binary else response.json() if json else response.text

    @staticmethod
    def filter_env(value):
        """Try to get a variable from env if the value matches ${*}."""
        if not isinstance(value, str):
            return value

        # Try to match ${*} with or without brackets.
        return re.sub(
            r'\$(\w+|\{([^}]*)\})',
            lambda m: os.environ[m.group(2) or m.group(1)],
            value
        )

    def load_template(self, name):
        """Feed a template into jinja2."""
        if name in self.data_file_contents:
            return self.data_file_contents[name]
        for path in (self.include_path or []):
            template_path = pathlib.Path(path) / name
            if template_path.exists():
                return template_path.read_text()
        return None

    @staticmethod
    def _clean_path(path, parse):
        return re.sub(r'\..*', '', path) if parse else re.sub(r'\.j2$', '', path)

    def _read_file(self, file_path, parse):
        contents = file_path.read_text()
        if file_path.suffix == '.j2':
            data_contents_key = '__' + file_path.as_posix()
            self.data_file_contents[data_contents_key] = contents
            contents = self.env.get_template(data_contents_key).render(self.data)
        return yaml.safe_load(contents) if parse else contents

    def _read_dir(self, dir_path, parse):
        data = {}
        extensions = ['.yml', '.yml.j2', '.yaml', '.yaml.j2',
                      '.json', '.json.j2'] if parse else ['']
        for file_path in itertools.chain(*[dir_path.glob(f'**/*{t}') for t in extensions]):
            if not file_path.is_file():
                continue
            parts = file_path.relative_to(dir_path).parts
            sub_data = data
            for part in parts[:-1]:
                sub_data = sub_data.setdefault(Render._clean_path(part, parse), {})
            sub_data[Render._clean_path(parts[-1], parse)] = self._read_file(file_path, parse)
        return data

    def _read(self, path_string, parse):
        path = pathlib.Path(path_string)
        return self._read_file(path, parse) if path.is_file() else self._read_dir(path, parse)

    def render(self):
        """Render the template."""
        for name, path in (self.data_paths or {}).items():
            self.data[name] = self._read(path, parse=True)
        for name, path in (self.raw_data_paths or {}).items():
            self.data[name] = self._read(path, parse=False)

        return self.env.get_template('__template').render(self.data)


def main(args):
    """Run the main CLI interface."""
    parser = argparse.ArgumentParser(
        description='Render templates',
    )
    parser.add_argument('template', type=argparse.FileType('r'), nargs='?', default=sys.stdin,
                        help='template file to render')
    parser.add_argument('--data', action=misc.StoreNameValuePair,
                        default={}, metavar='NAME=PATH',
                        help='JSON/YAML data file(s) to expose as NAME')
    parser.add_argument('--raw-data', action=misc.StoreNameValuePair,
                        default={}, metavar='NAME=PATH',
                        help='raw data file(s) to expose as NAME')
    parser.add_argument('--include-path', action='append', default=[],
                        help='path for template includes')
    parser.add_argument('--output', type=argparse.FileType('w'), default=sys.stdout,
                        help='Output path for the rendered file.')
    parsed_args = parser.parse_args(args)

    with parsed_args.template as template_file, parsed_args.output as output_file:
        result = Render(template_file,
                        data_paths=parsed_args.data,
                        raw_data_paths=parsed_args.raw_data,
                        include_path=parsed_args.include_path).render()
        output_file.write(result)


if __name__ == '__main__':
    main(sys.argv[1:])
