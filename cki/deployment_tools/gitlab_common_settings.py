"""Check CKI repos for the recommended configuration."""

import argparse
import sys

from cki_lib import gitlab
from gitlab import exceptions as gl_exceptions


class CkiChecker:
    """Check CKI repos for the recommended configuration."""

    # external projects not under the control of CKI
    ignored_projects = ['kernel-ark', 'kernel-ark-ci', 'gitlab-runner']
    # projects that use ff instead of merge
    # - kpet, kpet-db: preferred by nkondrashov
    ff_projects = ['kpet', 'kpet-db']
    # custom branches
    branches = {'datawarehouse': ['authorization'],
                'deployment-all': ['grafana-updates']}
    push_branches = {'deployment-all': ['grafana-updates']}
    merge_access_level = 30
    push_access_level = 0

    def __init__(self, cee=False, fix=False, project=None):
        """Create a new checker."""
        self._cee = cee
        self._fix = fix
        self._project = project

        url = 'https://gitlab.cee.redhat.com' if cee else 'https://gitlab.com'
        self._gl_instance = gitlab.get_instance(url, env_name='GITLAB_TOKENS')
        self._gl_group = self._gl_instance.groups.get('cki-project')

    def check_fork_ci(self, gl_project):
        # pylint: disable=no-self-use
        """Check that forks have CI disabled."""
        for fork in gl_project.forks.list(as_list=False, owned=True):
            gl_fork = self._gl_instance.projects.get(fork.id)
            if gl_fork.jobs_enabled:
                print(f'  unexpected enabled CI in fork {gl_fork.path_with_namespace}')
                if self._fix:
                    print('  fixing')
                    gl_fork.jobs_enabled = False
                    gl_fork.save()

    def check_project_creation(self):
        """Check that the default branch is called main."""
        if self._gl_group.project_creation_level != 'noone':
            print(f'  unexpected project creation level {self._gl_group.project_creation_level}')
            if self._fix:
                print('  fixing')
                self._gl_group.project_creation_level = 'noone'
                self._gl_group.save()

    def check_default_branch(self, gl_project):
        # pylint: disable=no-self-use
        """Check that the default branch is called main."""
        if gl_project.default_branch != 'main':
            print(f'  unexpected default branch {gl_project.default_branch}')

    def check_merge_method(self, gl_project):
        """Check that the merge method is merge/ff."""
        expected_merge_method = 'ff' if gl_project.path in self.ff_projects else 'merge'
        if gl_project.merge_method != expected_merge_method:
            print(f'  unexpected merge method {gl_project.merge_method}')
            if self._fix:
                print('  fixing')
                gl_project.merge_method = expected_merge_method
                gl_project.save()

    def check_required_resolved_discussions(self, gl_project):
        """Check that the discussions need to be resolved before merging."""
        if not gl_project.only_allow_merge_if_all_discussions_are_resolved:
            print('  unexpected not only_allow_merge_if_all_discussions_are_resolved')
            if self._fix:
                print('  fixing')
                gl_project.only_allow_merge_if_all_discussions_are_resolved = True
                gl_project.save()

    def check_branches(self, gl_project):
        # pylint: disable=no-self-use
        """Check that there are no branches but the default branch."""
        branches = set(b.name for b in gl_project.branches.list(as_list=False))
        branches.remove(gl_project.default_branch)
        if branches != set(self.branches.get(gl_project.path, [])):
            print(f'  unexpected branches {branches}')

    def check_auto_cancel_pipelines(self, gl_project):
        # pylint: disable=no-self-use
        """Check that redundant pipelines are not cancelled."""
        # this is problematic for dependent pipelines, where not all pipelines
        # for a ref are the same; a dependent pipeline does not regenerate the
        # container images, and might cancel an actual deployment
        if gl_project.auto_cancel_pending_pipelines != 'disabled':
            print('  unexpected auto_cancel_pending_pipelines')
            if self._fix:
                print('  fixing')
                gl_project.auto_cancel_pending_pipelines = 'disabled'
                gl_project.save()

    def check_branch_protection(self, gl_project):
        """Check that branch protection is configured correctly."""
        protectedbranches = [{
            'name': pb.name,
            'code_owner_approval_required': pb.attributes.get('code_owner_approval_required'),
            'push_access_level': pb.push_access_levels[0]['access_level'],
            'merge_access_level':pb.merge_access_levels[0]['access_level'],
        } for pb in gl_project.protectedbranches.list(as_list=False)]
        approval = None if self._cee else True
        expected_protectedbranches = [{
            'name': '*',
            'code_owner_approval_required': approval,
            'push_access_level': self.push_access_level,
            'merge_access_level': self.merge_access_level,
        }] + [{
            'name': branch,
            'code_owner_approval_required': approval,
            'push_access_level': self.merge_access_level,
            'merge_access_level': self.merge_access_level,
        } for branch in self.push_branches.get(gl_project.path, [])]
        if protectedbranches != expected_protectedbranches:
            print(f'  unexpected branch protections {protectedbranches}')
            if self._fix:
                print('  fixing')
                for protectedbranch in gl_project.protectedbranches.list(as_list=False):
                    protectedbranch.delete()
                for expected_protectedbranch in expected_protectedbranches:
                    gl_project.protectedbranches.create(expected_protectedbranch)

    def check_codeowner(self, gl_project):
        """Check that codeowners and approvals are configured correctly."""
        try:
            gl_project.files.get('CODEOWNERS', ref='main')
        except gl_exceptions.GitlabGetError:
            return
        expected = {
            'approvals_before_merge': 1,
            'reset_approvals_on_push': True,
            'disable_overriding_approvers_per_merge_request': False,
            'merge_requests_author_approval': False,
            'merge_requests_disable_committers_approval': False,
            'require_password_to_approve': False,
        }

        gl_approvals = gl_project.approvals.get()
        if {k: v for k, v in gl_approvals.attributes.items() if k in expected.keys()} != expected:
            print('  unexpected approval settings')
            if self._fix:
                print('  fixing')
                gl_project.approvals.update(**expected)

        for gl_rule in gl_project.approvalrules.list(as_list=False):
            if gl_rule.rule_type == 'any_approver':
                if gl_rule.approvals_required > 0:
                    print('  too many approvers required')
                    if self._fix:
                        print('  fixing')
                        gl_rule.approvals_required = 0
                        gl_rule.save()
            else:
                print('  superfluous approval rule')
                if self._fix:
                    print('  fixing')
                    gl_rule.delete()

    def run(self):
        """Run all checkers."""
        self.check_project_creation()
        for gl_project in self._gl_group.projects.list(as_list=False):
            if gl_project.archived or gl_project.path in self.ignored_projects:
                continue
            if self._project and self._project != gl_project.path:
                continue
            gl_project = self._gl_instance.projects.get(gl_project.path_with_namespace)
            print(gl_project.path)
            if gl_project.path == 'infrastructure':
                continue  # no git repo

            self.check_default_branch(gl_project)
            self.check_merge_method(gl_project)
            self.check_required_resolved_discussions(gl_project)
            self.check_branch_protection(gl_project)
            self.check_branches(gl_project)
            self.check_auto_cancel_pipelines(gl_project)
            self.check_fork_ci(gl_project)
            self.check_codeowner(gl_project)


def main(args):
    """Run the main CLI interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--fix', action='store_true',
                        help='Fix detected problems')
    parser.add_argument('--cee', action='store_true',
                        help='Analyze cki-project at gitlab.cee instead of gitlab.com')
    parser.add_argument('--project',
                        help='Only run the checker for one project')
    args = parser.parse_args(args)

    checker = CkiChecker(cee=args.cee, fix=args.fix, project=args.project)
    checker.run()


if __name__ == '__main__':
    main(sys.argv[1:])
