"""Print AWS cost explorer summaries."""
import argparse
import collections
import datetime
import enum
import itertools
import re
import sys

import boto3
from cki_lib import misc

_Cost = collections.namedtuple('_CostType', 'type grouping clean_fn')


class Cost(_Cost, enum.Enum):
    """Cost type enumeration."""

    DAILY = _Cost('daily', None, None)
    COMPONENT = _Cost(
        'grouped',
        {'Type': 'TAG', 'Key': 'ServiceComponent'},
        lambda key: re.sub(r'.*\$', '', key))
    OWNER = _Cost(
        'grouped',
        {'Type': 'TAG', 'Key': 'ServiceOwner'},
        lambda key: {
            'ServiceOwner$': 'Unknown',
        }.get(key, re.sub(r'.*\$', '', key)))
    SERVICE = _Cost(
        'grouped',
        {'Type': 'DIMENSION', 'Key': 'SERVICE'},
        lambda key: {
            'Amazon Elastic Compute Cloud - Compute': 'EC2',
            'Amazon Simple Storage Service': 'S3',
            'EC2 - Other': 'EC2-Other',
        }.get(key, key))
    USAGE_TYPE = _Cost(
        'grouped',
        {'Type': 'DIMENSION', 'Key': 'USAGE_TYPE'},
        lambda key: {
            'EBS:VolumeUsage.gp2': 'EBS',
        }.get(key, re.sub('BoxUsage:|Usage|-ByteHrs', '', key)))
    INSTANCE_TYPE = _Cost(
        'grouped',
        {'Type': 'DIMENSION', 'Key': 'INSTANCE_TYPE'},
        lambda key: key)
    OPERATION = _Cost(
        'grouped',
        {'Type': 'DIMENSION', 'Key': 'OPERATION'},
        lambda key: re.sub(r':SV\d\d\d', '', key))


class AwsCostExplorer:
    """Interface with the AWS Cost Explorer API."""

    def __init__(self, days=7, filters=None):
        """Create a new Cost Explorer client."""
        self.client = boto3.client('ce')

        now = datetime.datetime.utcnow()
        end = now.date()
        start = end - datetime.timedelta(days=days)
        one_day = end - datetime.timedelta(days=1)
        self.end = end.strftime('%Y-%m-%d')
        self.start = start.strftime('%Y-%m-%d')
        self.one_day = one_day.strftime('%Y-%m-%d')

        filters = [{'Tags': {'Key': t[0], 'Values': [t[1]]}}
                   for t in (filters or {}).items()]
        if len(filters) > 1:
            filters = {'Filter': {'And': filters}}
        elif len(filters) == 1:
            filters = {'Filter': filters[0]}
        else:
            filters = {}
        self.common_args = dict({
            'Granularity': 'DAILY',
            'Metrics': ['BlendedCost']}, **filters)

    def costs(self, cost_type: Cost):
        """Return a list with cost statistics."""
        if cost_type.type == 'daily':
            return self.daily_costs()
        return self.grouped_costs(
            cost_type.grouping, cost_type.clean_fn)

    def daily_costs(self):
        """Return a list with daily costs."""
        response = self.client.get_cost_and_usage(
            TimePeriod={'Start': self.start, 'End':  self.end},
            **self.common_args)
        amounts = sorted(response['ResultsByTime'],
                         key=lambda r: r['TimePeriod']['Start'],
                         reverse=True)
        return [(r['TimePeriod']['Start'][5:].replace('-', '/'),
                 float(r['Total']['BlendedCost']['Amount']))
                for r in amounts]

    def total_costs(self):
        """Return the last total daily costs."""
        response = self.client.get_cost_and_usage(
            TimePeriod={'Start': self.one_day, 'End':  self.end},
            **self.common_args)
        return float(response['ResultsByTime'][0]
                     ['Total']['BlendedCost']['Amount'])

    def grouped_costs(self, groups, clean_fn=None):
        """Return a list with grouped averaged costs."""
        response = self.client.get_cost_and_usage(
            TimePeriod={'Start': self.start, 'End':  self.end},
            GroupBy=[groups],
            **self.common_args)
        amounts = {}
        for result in response['ResultsByTime']:
            for group in result['Groups']:
                key = group['Keys'][0]
                amount = float(group['Metrics']['BlendedCost']['Amount']) / 7
                if clean_fn:
                    key = clean_fn(key)
                amounts.setdefault(key, 0)
                amounts[key] += amount
        return sorted(amounts.items(), key=lambda r: r[1], reverse=True)

    @staticmethod
    def summarize(amounts, item_cutoff, max_items):
        """Format the cost list into a simple string."""
        if item_cutoff:
            amounts = (r for r in amounts if r[1] >= item_cutoff)
        if max_items:
            amounts = itertools.islice(amounts, max_items)
        return ' '.join(f'{r[0]}=${r[1]:.2f}' for r in amounts)


def main(args):
    """Handle command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--threshold', type=float, metavar='COST',
                        help='Non-zero exit code if costs exceed threshold')
    # pylint: disable=no-member  # check broken in pylint 2.7.0?
    parser.add_argument('group', nargs='?', default='daily',
                        help=', '.join(Cost.__members__.keys()).lower())
    parser.add_argument('--filter', action=misc.StoreNameValuePair,
                        default={}, help='Filter costs by tag')
    parser.add_argument('--max-items', type=int, metavar='COUNT',
                        help='Limit the number of items to show')
    parser.add_argument('--item-cutoff', type=float, metavar='COST',
                        help='Only show items with higher costs')
    args = parser.parse_args(args)

    explorer = AwsCostExplorer(filters=args.filter)

    if args.threshold and explorer.total_costs() > args.threshold:
        message = 'Cost threshold exceeded: '
        exit_code = 1
    else:
        message = ''
        exit_code = 0
    costs = explorer.costs(Cost[args.group.upper()])
    print(message + explorer.summarize(costs, args.item_cutoff,
                                       args.max_items))
    return exit_code


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
