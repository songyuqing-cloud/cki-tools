"""Periodically clean the scratch volume."""
import argparse
import os
import pathlib
import shutil
import sys

from cki_lib import gitlab
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session

from . import _utils

LOGGER = get_logger('cki.cki_tools.scratch_cleaner')
SESSION = get_session('cki.cki_tools.scratch_cleaner')


def notify_bot(gl_job):
    """Send a message to the IRC bot."""
    if 'IRCBOT_URL' not in os.environ:
        return
    with misc.only_log_exceptions():
        status = gl_job.status
        name = gl_job.name
        job_id = gl_job.id
        pipeline_id = gl_job.pipeline['id']
        url = misc.shorten_url(gl_job.web_url)
        message = (f'👻 P{pipeline_id} J{job_id} {name} {status}:'
                   f' Deleting abandoned scratch directory {url}')
        SESSION.post(os.environ['IRCBOT_URL'], json={'message': message}).raise_for_status()


def main(args):
    """Periodically clean the scratch volume."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--scratch-volume', default='/tmp/scratch', metavar='PATH',
                        help='Root path for the scratch volume')
    parsed_args = parser.parse_args(args)

    result_code = 0
    for job_dir in pathlib.Path(parsed_args.scratch_volume).glob('*/*/*'):
        try:
            _utils.cprint(_utils.Colors.YELLOW, f'Checking {job_dir}...')
            gl_instance = gitlab.get_instance(f'https://{job_dir.parts[-3]}')
            gl_project = gl_instance.projects.get(int(job_dir.parts[-2]), lazy=True)
            gl_job = gl_project.jobs.get(int(job_dir.parts[-1]))
            print(f'  {gl_job.web_url} ', end='')
            if gl_job.status in ('canceled', 'failed', 'success'):
                _utils.cprint(_utils.Colors.RED, gl_job.status)
                if misc.is_production():
                    notify_bot(gl_job)
                    print('  Cleaning recursively')
                    shutil.rmtree(job_dir)
                else:
                    print('  Not cleaning as not in production mode')
            else:
                _utils.cprint(_utils.Colors.GREEN, gl_job.status)
                print('  Not cleaning as not finished')
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('Unable to clean %s', job_dir)
            result_code = 1

    return result_code


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
