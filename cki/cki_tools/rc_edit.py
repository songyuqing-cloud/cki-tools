#!/usr/bin/env python3
"""Shell interface for pipeline rc data."""
import argparse
from ast import literal_eval
import pathlib
import sys

from cki_lib.misc import strtobool
from rcdefinition.rc_data import SKTData


def parse_args():
    """Parse cmd-line arguments."""
    parser = argparse.ArgumentParser(
        description='Edit the rc file'
    )

    parser.add_argument(
        '--stdin',
        help='Read data from stdin.',
        action='store_true',
        default=False,
    )
    parser.add_argument(
        'rcfile',
        type=str,
        help='A path to the rc-file to read/write.',
        default=None,
    )
    parser.add_argument(
        'function',
        type=str,
        help='A function to use like "rc_state_get", "rc_state_set".',
        default=None,
    )

    args, remaining = parser.parse_known_args()

    return args, remaining


class RCEdit:
    """Wrapper for default shell way of editing rc-file."""

    def __init__(self, skt_data):
        """Initialize object with the given data."""
        self.skt_data = skt_data

        # there is data to be written out
        self.dirty = False

    def convert_primitive(self, section, name, value):
        """Cast value to bool or int, or leave as str."""
        actual_type = getattr(self.skt_data, section).__annotations__.get(name)
        if actual_type == bool:
            value = strtobool(value)
        elif actual_type == int:
            value = int(value)
        elif actual_type == list:
            value = literal_eval(value)
        elif actual_type is None:
            obj_sect = getattr(self.skt_data, section)
            obj_sect.__annotations__[name] = type(value)
        else:
            # Leave as str.
            pass

        return value

    def rc_set(self, section, name, value):
        """Set an rc parameter.

        Args: section name value
        """
        if value is not None:
            value = self.convert_primitive(section, name, value)
        # Section exists and must exist; it is an object nested in SKTData (like RunnerData, ...).
        obj_section = getattr(self.skt_data, section)
        setattr(obj_section, name, value)

        self.dirty = True

    def rc_get(self, section, name):
        """Get an rc parameter.

        Args: section name
        Output: value
        """
        section = getattr(self.skt_data, section, None)
        value = getattr(section, name, None)
        return value

    def rc_del(self, section, name):
        """Delete an rc parameter.

        Args: section name
        """
        # delete an attribute from section
        self.rc_set(section, name, None)

        self.dirty = True

    def rc_state_set(self, name, value):
        """Set an rc state parameter.

        Args: name value
        """
        self.rc_set('state', name, value)
        self.dirty = True

    def rc_state_get(self, name):
        """Get an rc state parameter.

        Args: name
        Output: value
        """
        return self.rc_get('state', name)

    def rc_state_del(self, name):
        """Delete an rc state parameter.

        Args: name
        """
        self.rc_del('state', name)
        self.dirty = True


def main():
    # pylint: disable=W0632
    """Read args to see what function is to be run; delete/set/get var."""
    args, remaining = parse_args()
    if args.stdin:
        remaining += [sys.stdin.read()]

    pathlib.Path(args.rcfile).touch()

    with open(args.rcfile, 'r+') as fhandle:
        rc_edit = RCEdit(SKTData.deserialize(fhandle.read()))

        func = getattr(rc_edit, args.function)
        result_value = func(*remaining)
        if result_value is not None:
            # print to stdout, so it can be used to set vars in bash
            print(result_value)

    # if data was changed, write changes to the file
    if rc_edit.dirty:
        with open(args.rcfile, 'w') as fhandle:
            fhandle.write(rc_edit.skt_data.serialize())


if __name__ == '__main__':
    main()
