#! /bin/bash

export START_PYTHON_SERVICE_METRICS="cki.cki_tools.service_metrics"
export LOG_NAME="service_metrics"

if [ -r ${KRB_KEYTAB:-/keytab} ]; then
    kinit -t ${KRB_KEYTAB:-/keytab} ${KRB_USER} -l 7d
fi

exec cki_entrypoint.sh
