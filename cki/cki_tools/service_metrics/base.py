"""Base metric classes."""
import threading
import time

from cki_lib.logger import get_logger

LOGGER = get_logger(__name__)


class Metric:
    """Metric base class."""

    update_interval_s = 5

    def __init__(self):
        """Initialize."""
        self.last_run_ts = 0
        self._running = threading.Event()
        LOGGER.info('Initialized %s', self.__class__.__name__)

    def update(self):
        """
        Update the metrics.

        This method is called every time the metric is updated and should
        perform all the necessary steps to update the prometheus metrics
        this class exposes.
        """
        raise NotImplementedError

    def _update(self):
        """Update and clear the _running flag."""
        LOGGER.info('Updating %s', type(self).__name__)
        try:
            self.update()
        finally:
            self._running.clear()
        LOGGER.debug('Finished updating %s', type(self).__name__)

    def check_and_update(self):
        """Check the time since the last update and, if necessary, call update()."""
        now = time.monotonic()
        time_since_last_update = now - self.last_run_ts

        if time_since_last_update > self.update_interval_s:
            if self._running.is_set():
                LOGGER.error("Task '%s' scheduled to run but it's still running.",
                             type(self).__name__)
                return

            self.last_run_ts = now
            self._running.set()
            threading.Thread(target=self._update, daemon=True).start()
