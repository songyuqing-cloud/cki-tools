"""Beaker metrics via TEIID."""
from collections import defaultdict
import os
import re

from cki_lib.psql import PSQLHandler
import prometheus_client
import yaml

from .. import base

BEAKER_CONFIG = yaml.safe_load(os.environ.get('BEAKER_CONFIG', ''))


class TEIIDBeakerMetrics(base.Metric):
    """Calculate Beaker metrics from TEIID."""

    update_interval_s = 60 * 5

    metric_recipes_in_queue = prometheus_client.Gauge(
        'cki_beaker_recipes_in_queue',
        'Number of jobs in a Beaker queue',
        ['queue']
    )

    def __init__(self):
        """Initialize."""
        super().__init__()
        self.db_handler = PSQLHandler(
            os.environ.get('TEIID_HOST', 'localhost'),
            os.environ.get('TEIID_PORT', '5433'),
            os.environ.get('TEIID_DATABASE', 'public'),
            require_ssl=True
        )

    def get_queued_recipes_constraints(self):
        """Get distro_requires for the queued recipes."""
        query = """
            SELECT
                "r._distro_requires"
            FROM
                beaker.recipe AS r
            JOIN
                beaker.recipe_set AS rs ON rs.id = r.recipe_set_id
            JOIN
                beaker.job AS j ON j.id = rs.job_id
            JOIN
                beaker.tg_user AS u ON u.user_id = j.owner_id
            WHERE
                r.status = 'Queued'
            AND
                u.user_name = 'beaker/cki-team-automation';
        """
        return [r[0] for r in self.db_handler.execute(query)]

    @staticmethod
    def _recipe_matches_queue(distro_requires, architectures):
        """Check if the distro_requires matches all the specified contraints."""
        required_architectures = re.findall(r'distro_arch op="=" value="(\S+)"', distro_requires)
        return set(required_architectures) == set(architectures)

    def update_cki_beaker_recipes_in_queue(self):
        """Update cki_beaker_recipes_in_queue metric."""
        queues = defaultdict(int)
        queued_recipes = self.get_queued_recipes_constraints()

        for queue_name, architectures in BEAKER_CONFIG.get('queues', {}).items():
            architectures = architectures or [queue_name]
            for recipe_distro_required in queued_recipes:
                if self._recipe_matches_queue(recipe_distro_required, architectures):
                    queues[queue_name] += 1

            self.metric_recipes_in_queue.labels(queue_name).set(queues[queue_name])

    def update(self):
        """Update all metrics."""
        self.update_cki_beaker_recipes_in_queue()
