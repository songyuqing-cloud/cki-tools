"""Disk metrics."""
import os
import shutil

import prometheus_client
import yaml

from .. import base

VOLUMES_LIST = yaml.safe_load(os.environ.get('VOLUMES_LIST', '[]'))


class VolumeMetrics(base.Metric):
    # pylint: disable=too-few-public-methods
    """Calculate Volume metrics."""

    update_interval_s = 60 * 5

    metric_usage = prometheus_client.Gauge(
        'cki_volume_usage',
        'The number of used bytes on a volume',
        ['name']
    )

    def update(self):
        """Update the metrics."""
        for volume in VOLUMES_LIST:
            usage = shutil.disk_usage(volume['path'])
            self.metric_usage.labels(volume['name']).set(usage.used)
