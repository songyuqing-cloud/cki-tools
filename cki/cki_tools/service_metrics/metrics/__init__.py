"""Actual metrics."""

from .beaker import BeakerMetrics
from .datawarehouse import DatawarehouseSQLMetrics
from .kubernetes import KubernetesMetrics
from .s3buckets import S3BucketMetrics
from .sentry import SentryMetrics
from .teiidbeaker import TEIIDBeakerMetrics
from .volume import VolumeMetrics

ALL_METRICS = [
    DatawarehouseSQLMetrics,
    BeakerMetrics,
    S3BucketMetrics,
    TEIIDBeakerMetrics,
    SentryMetrics,
    KubernetesMetrics,
    VolumeMetrics,
]
