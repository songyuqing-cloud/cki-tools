"""Kubernetes metrics."""
import json

from cki_lib import kubernetes
from cki_lib.logger import get_logger
from kubernetes.utils.quantity import parse_quantity
import prometheus_client

from .. import base

LOGGER = get_logger(__name__)


class KubernetesMetrics(base.Metric):
    """Calculate Kubernetes metrics."""

    update_interval_s = 60

    metric_usage = prometheus_client.Gauge(
        'cki_k8s_resource_usage',
        'The number of used resources by a container',
        ['pod', 'container', 'resource']
    )

    def __init__(self):
        """Initialize."""
        super().__init__()
        self._k8s = None

    @property
    def k8s(self):
        """
        Return or initialize k8s api.

        Not done in initialization so importing this module in tests do not explode.
        """
        if not self._k8s:
            self._k8s = kubernetes.KubernetesHelper()
            self._k8s.setup()

        return self._k8s

    def update_cluster(self):
        """Update the metrics for a single cluster."""
        response = self.k8s.api_client.call_api(
            f'/apis/metrics.k8s.io/v1beta1/namespaces/{self.k8s.namespace}/pods',
            'GET',
            auth_settings=['BearerToken'],
            response_type='json',
            _preload_content=False
        )
        data = json.loads(response[0].data.decode('utf-8'))

        for pod in data['items']:
            for container in pod['containers']:
                labels = (pod['metadata']['name'], container['name'])

                self.metric_usage.labels(*labels, 'memory').set(
                    float(parse_quantity(container['usage']['memory']))
                )

                self.metric_usage.labels(*labels, 'cpu').set(
                    float(parse_quantity(container['usage']['cpu']))
                )

        self._delete_missing_containers(data['items'])

    def _delete_missing_containers(self, pods):
        """Delete metrics for containers that are not running anymore."""
        current_containers = {
            (pod['metadata']['name'], container['name'])
            for pod in pods
            for container in pod['containers']
        }

        # pylint: disable=protected-access
        to_delete = {(p, c) for (p, c, _) in self.metric_usage._metrics} - current_containers

        for container in to_delete:
            LOGGER.debug('Deleting metrics for: %s', container)
            self.metric_usage.remove(*container, 'memory')
            self.metric_usage.remove(*container, 'cpu')

    def update(self):
        """Update the metrics."""
        self.update_cluster()
