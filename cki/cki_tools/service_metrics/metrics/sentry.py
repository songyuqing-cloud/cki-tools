"""Sentry statistics."""
from collections import defaultdict
from dataclasses import dataclass
import os

from cached_property import cached_property
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import prometheus_client
import yaml

from .. import base

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)

SENTRY_INSTANCES = yaml.safe_load(os.environ.get('SENTRY_INSTANCES', '[]'))


class SentryAPI:
    """Sentry API handler."""

    def __init__(self, config):
        """Initialize."""
        self.config = config

    def _get(self, endpoint):
        """Do get request to endpoint."""
        headers = {
            'Authorization': 'Bearer ' + os.environ[self.config['token_name']]
        }
        url = self.config['url'] + endpoint.format(**self.config)
        return SESSION.get(url, headers=headers).json()

    @cached_property
    def projects(self):
        """Return the team projects."""
        return self._get('/api/0/teams/{org}/{team}/projects/')

    def get_project_stats(self, project):
        """Return the stats for a project."""
        project_slug = project['slug']
        return self._get(f'/api/0/projects/{{org}}/{project_slug}/stats/')


@dataclass
class EventsCount:
    """Representation of events count object."""

    timestamp: float
    count: int


class SentryMetrics(base.Metric):
    """Calculate Sentry metrics."""

    update_interval_s = 60 * 5

    metric_sentry_events = prometheus_client.Gauge(
        'cki_sentry_events',
        'Number of events in sentry',
        ['instance', 'project']
    )

    def __init__(self):
        """Initialize."""
        super().__init__()

        self._apis = {instance['url']: SentryAPI(instance)
                      for instance in SENTRY_INSTANCES}
        self._accumulated_value = {instance['url']: defaultdict(int)
                                   for instance in SENTRY_INSTANCES}
        self._previous_stat = {instance['url']: defaultdict(dict)
                               for instance in SENTRY_INSTANCES}

    def update_project(self, instance, project):
        """Update a single project metrics."""
        project_slug = project['slug']
        instance_url = instance['url']

        # Grab the newest time slice
        new_stat = EventsCount(*self._apis[instance_url].get_project_stats(project)[-1])
        previous_stat = self._previous_stat[instance_url][project_slug]

        if not previous_stat or new_stat.timestamp != previous_stat.timestamp:
            # First run or counter reset, need to add it directly.
            self._accumulated_value[instance_url][project_slug] += new_stat.count
        else:
            # Need to increment by the difference between the previous value and the new one.
            self._accumulated_value[instance_url][project_slug] += (
                new_stat.count - previous_stat.count
            )

        self._previous_stat[instance_url][project_slug] = new_stat

        self.metric_sentry_events.labels(instance_url, project_slug).set(
            self._accumulated_value[instance_url][project_slug]
        )

    def update(self):
        """Update the sentry metrics."""
        for instance in SENTRY_INSTANCES:
            for project in self._apis[instance['url']].projects:
                self.update_project(instance, project)
