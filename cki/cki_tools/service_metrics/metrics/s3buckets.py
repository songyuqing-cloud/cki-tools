"""S3 bucket statistics."""
import os

from cki_lib.logger import get_logger
import prometheus_client
import yaml

from cki.cki_tools import _utils
from cki.monitoring_tools.check_s3_bucket_size import get_bucket_size

from .. import base

LOGGER = get_logger(__name__)
S3_CONFIG = yaml.safe_load(os.environ.get('S3_CONFIG', ''))


class S3BucketMetrics(base.Metric):
    """Calculate S3 bucket metrics."""

    update_interval_s = 60 * 60 * 24

    metric_file_count = prometheus_client.Gauge(
        'cki_s3_bucket_file_count',
        'Number of files in the S3 bucket',
        ['name', 'description']
    )

    metric_bytes = prometheus_client.Gauge(
        'cki_s3_bucket_bytes',
        'Size of the bucket in bytes',
        ['name', 'description']
    )

    def update(self):
        """Update the bucket metrics."""
        for bucket in S3_CONFIG.get('buckets', []):
            bucket_spec = _utils.parse_bucket_spec(os.environ[bucket['name']])
            try:
                size, count = get_bucket_size(bucket_spec)
            except Exception:  # pylint: disable=broad-except
                LOGGER.exception("Error while getting data for %s", bucket['name'])
                continue
            self.metric_file_count.labels(bucket['name'], bucket['description']).set(count)
            self.metric_bytes.labels(bucket['name'], bucket['description']).set(size)
