"""DataWarehouse metrics."""
import os

from cki_lib.logger import get_logger
from cki_lib.psql import PSQLHandler
from cki_lib.session import get_session
import prometheus_client
import yaml

from .. import base

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)
BUCKETS_RANGE = [0.1, 0.2, 0.4, 1, 2, 4]
BEAKER_CONFIG = yaml.safe_load(os.environ.get('BEAKER_CONFIG', ''))


class DatawarehouseSQLMetrics(base.Metric):
    """Calculate metrics via SQL."""

    update_interval_s = 60 * 15

    metric_time_to_build = prometheus_client.Histogram(
        'cki_time_to_build',
        'Time between a checkout is started and a build has finished',
        # Target: 1h
        buckets=[3600 * r for r in BUCKETS_RANGE]
    )
    metric_time_to_report = prometheus_client.Histogram(
        'cki_time_to_report',
        'Time between a checkout is detected and the results are reported',
        # Target: 24hs
        buckets=[3600 * 24 * r for r in BUCKETS_RANGE]
    )
    metric_time_checkout_without_build = prometheus_client.Histogram(
        'cki_time_checkout_without_build',
        'How long a checkout has no build results',
        # Target: 1h
        buckets=[3600 * r for r in BUCKETS_RANGE]
    )

    metric_dw_table_size_bytes = prometheus_client.Gauge(
        'cki_dw_table_size_bytes',
        'Size of Datawarehouse table size in bytes',
        ['table']
    )

    metric_dw_table_size_rows = prometheus_client.Gauge(
        'cki_dw_table_size_rows',
        'Size of Datawarehouse table size in rows',
        ['table']
    )

    def __init__(self):
        """Initialize."""
        super().__init__()
        self.db_handler = PSQLHandler(
            os.environ.get('DATAWAREHOUSE_PSQL_HOST', 'localhost'),
            os.environ.get('DATAWAREHOUSE_PSQL_PORT', '5432'),
            os.environ.get('DATAWAREHOUSE_PSQL_DATABASE', 'datawarehouse'),
            user=os.environ.get('DATAWAREHOUSE_PSQL_USER', 'datawarehouse'),
            password=os.environ.get('DATAWAREHOUSE_PSQL_PASSWORD'),
        )

    def update_time_to_build(self):
        """
        Update the time_to_build metric.

        Calculate the time between a checkout was started and the build job
        finished.
        """
        get_builds = self.db_handler.execute(
            'SELECT '
            '  EXTRACT (EPOCH FROM '
            '    (b.start_time - r.start_time + interval \'1 sec\' * b.duration) '
            '  ) '
            'FROM '
            '  datawarehouse_kcidbbuild AS b '
            'INNER JOIN datawarehouse_kcidbcheckout AS r '
            '  ON r.iid = b.checkout_id '
            'WHERE '
            '  (b.start_time + interval \'1 sec\' * b.duration) >= '
            f' (NOW() - INTERVAL \'{self.update_interval_s} sec\') '
            # The checkout can be updated after the builds run.
            # Exclude the builds for an old checkout to avoid low/negative results.
            'AND '
            '  b.start_time > r.start_time'
        )

        for result in get_builds:
            time_to_build = result[0]

            LOGGER.debug("Found build: time_to_build=%d", time_to_build)
            self.metric_time_to_build.observe(time_to_build)

    def update_time_to_report(self):
        """
        Update the time_to_report metric.

        Calculate the time between a checkout was started and we reported
        the results.
        """
        get_reports = self.db_handler.execute(
            'SELECT '
            '  EXTRACT (EPOCH FROM (report.sent_at - r.start_time)) '
            'FROM '
            '  datawarehouse_report AS report '
            'INNER JOIN datawarehouse_kcidbcheckout AS r '
            '  ON r.iid = report.kcidb_checkout_id '
            'WHERE '
            f' report.sent_at >= NOW() - INTERVAL \'{self.update_interval_s} sec\' '
            # The checkout can be updated after the report was sent.
            # Exclude the reports for an old checkout to avoid low/negative results.
            'AND '
            '  report.sent_at > r.start_time'
        )

        for result in get_reports:
            time_to_report = result[0]

            LOGGER.debug("Found report: time_to_report=%d", time_to_report)
            self.metric_time_to_report.observe(time_to_report)

    def update_time_checkout_without_build(self):
        """
        Update the time_checkout_without_build metric.

        Calculate how many checkouts there are without build results.
        Exclude checkouts newer than 1 hour.
        """
        get_checkouts = self.db_handler.execute(
            'SELECT '
            '  EXTRACT (EPOCH FROM (NOW() - r.start_time)) '
            'FROM '
            '  datawarehouse_kcidbcheckout AS r '
            'LEFT OUTER JOIN '
            '  datawarehouse_kcidbbuild as b ON r.iid = b.checkout_id '
            'WHERE '
            '  b.id IS NULL '
            'AND '
            ' r.start_time <= NOW() - INTERVAL \'1 hour\' '
            'AND '
            ' r.start_time >= NOW() - INTERVAL \'24 hour\' '
        )

        for result in get_checkouts:
            time_checkout_without_build = result[0]
            if not time_checkout_without_build:
                continue

            LOGGER.debug("Found checkout: time_checkout_without_build=%d",
                         time_checkout_without_build)
            self.metric_time_checkout_without_build.observe(time_checkout_without_build)

    def update_psql_tables(self):
        """Update psql table size metrics."""
        table_sizes = self.db_handler.execute("""
            SELECT
                table_name,
                pg_total_relation_size('"' || table_schema || '"."' || table_name || '"')
                    AS table_size
            FROM information_schema.tables
            WHERE
                table_schema = 'public';
        """)
        for result in table_sizes:
            name, size = result
            self.metric_dw_table_size_bytes.labels(name).set(size)

        # This method is approximated and can vary slightly from the real size.
        # https://stackoverflow.com/a/2611745
        table_rows = self.db_handler.execute("""
            SELECT
                relname,
                n_live_tup
            FROM
                pg_stat_user_tables
            WHERE
                schemaname = 'public';
        """)

        for result in table_rows:
            name, size = result
            self.metric_dw_table_size_rows.labels(name).set(size)

    def update(self):
        """Update the metrics."""
        self.update_psql_tables()
        self.update_time_to_report()
        self.update_time_to_build()
        self.update_time_checkout_without_build()
