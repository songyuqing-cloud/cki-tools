"""Beaker metrics."""
import os

from cki_lib.session import get_session
import prometheus_client
import yaml

from .. import base

SESSION = get_session(__name__)
BEAKER_CONFIG = yaml.safe_load(os.environ.get('BEAKER_CONFIG', ''))


class BeakerMetrics(base.Metric):
    """Calculate Beaker metrics."""

    update_interval_s = 60 * 5

    metric_pool_count = prometheus_client.Gauge(
        'cki_beaker_pool_count',
        'Number of systems in a Beaker pool',
        ['pool']
    )

    metric_system = prometheus_client.Info(
        'cki_beaker_system',
        'Status of a system in Beaker',
        ['system']
    )

    @staticmethod
    def _beaker_get(endpoint):
        """Do the Beaker get request."""
        headers = {"Accept": "application/json"}
        return SESSION.get(
            BEAKER_CONFIG['beaker_url'] + endpoint,
            headers=headers
        ).json()

    def update_pool_count(self):
        """Update the cki_beaker_pool_count metric."""
        for pool in BEAKER_CONFIG.get('pools', []):
            data = self._beaker_get(f'/pools/{pool}')
            self.metric_pool_count.labels(pool).set(
                len(data.get('systems'))
            )

    def update_system(self):
        """Update the cki_beaker_system metric."""
        for system in BEAKER_CONFIG.get('systems', []):
            data = self._beaker_get(f'/systems/{system}')
            current_reservation = data.get('current_reservation')

            self.metric_system.labels(system).info(
                {
                    'status': data.get('status'),
                    'recipe_id': str((current_reservation or {}).get('recipe_id', 0)),
                }
            )

    def update(self):
        """Update the metrics."""
        self.update_pool_count()
        self.update_system()
