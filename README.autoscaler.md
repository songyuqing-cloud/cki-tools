# Autoscaler - Scale up/down replicas

`cki.deployment_tools.autoscaler`

Dynamically scale up and down the replicas of a DeploymentConfig
depending on the number of messages in the queue it's consuming.

## Configuration

This script reads the configuration from the `AUTOSCALER_CONFIG`
environment variable, with the following content:

```json
{
    ".default": {
        "namespace": "namespace-name",
        "messages_per_replica": 20,
        "max_scale_up_step": 1,
        "max_scale_down_step": 1,
        "replicas_min": 1,
        "replicas_max": 5
    },
    "app-name-1": {
        "deployment_config": "dc-app-1",
        "queue": "queue-app-1"
    },
    "app-name-2": {
        "deployment_config": "dc-app-2",
        "queue": "queue-app-2",
        "replicas_min": 2,
        "replicas_max": 20
    }
}
```

If `AUTOSCALER_CONFIG` is not present, it will try to load the
config from the `--config` path, in YAML format.

```yaml
---
.default:
  namespace: namespace-name
  messages_per_replica: 20
  max_scale_up_step: 1
  max_scale_down_step: 1
  replicas_min: 1
  replicas_max: 5

app-name-1:
  deployment_config: dc-name-1
  queue: queue-name-1

app-name-2:
  deployment_config: dc-name-2
  queue: queue-name-2
  replicas_min: 2
  replicas_max: 20
```

Each application needs to be configured with the corresponding
`deployment-config` and `queue` values.
Default values can be overriden on the application section.

| Yaml variable          | Description                                                                |
|------------------------|----------------------------------------------------------------------------|
| `namespace`            | Openshift namespace.                                                       |
| `messages_per_replica` | Number of messages a replica can handle before scaling up.                 |
| `max_scale_up_step`    | Maximum number of replicas that can be spawned on a single refresh period. |
| `max_scale_down_step`  | Maximum number of replicas that can be deleted on a single refresh period. |
| `replicas_min`         | Minumum number of replicas spawned at the same time.                       |
| `replicas_max`         | Maximum number of replicas spawned at the same time.                       |
| `deployment_config`    | Name of the OpenShift DeploymentConfig.                                    |
| `queue`                | Name of the RabbitMQ queue.                                                |

| Environment variable     | Description                                                                    |
|--------------------------|--------------------------------------------------------------------------------|
| `IS_PRODUCTION`          | Define if the environment is staging or production.                            |
| `AUTOSCALER_CONFIG`      | Configuration in JSON. If not present, falls back to `AUTOSCALER_CONFIG_PATH`. |
| `AUTOSCALER_CONFIG_PATH` | Path to the configuration YAML file (Default: config.yaml)                     |
| `REFRESH_PERIOD`         | Time in seconds between checks (Default: 30).                                  |
| `RABBITMQ_HOST`          | RabbitMQ host                                                                  |
| `RABBITMQ_PORT`          | RabbitMQ port, TLS is used for port 443                                        |
| `RABBITMQ_USER`          | RabbitMQ user                                                                  |
| `RABBITMQ_PASSWORD`      | RabbitMQ password                                                              |
| `RABBITMQ_CAFILE`        | RabbitMQ CA file path                                                          |
| `RABBITMQ_CERTFILE`      | RabbitMQ certificate + private key file path                                   |
| `SENTRY_SDN`             | Sentry SDN                                                                     |

### IS_PRODUCTION

On staging developments (`IS_PRODUCTION=False`), the changes are only logged but
not applied.
