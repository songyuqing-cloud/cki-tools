#! /usr/bin/python3
"""
Push service status to influxdb.

If a problem is found while pushing the data, it is saved to disk and
retried on the following calls until success.

Required environment variables:
    - MONIT_DESCRIPTION
    - MONIT_SERVICE
    - INFLUXDB_HOST
    - INFLUXDB_PORT
    - INFLUXDB_USER
    - INFLUXDB_PASSWORD
    - INFLUXDB_DATABASE
"""
import datetime
import json
import os
import pathlib
import re

from influxdb import InfluxDBClient

TEMP_FILE = '/tmp/monit-influxdb.json'

MONIT_DESCRIPTION = os.environ.get('MONIT_DESCRIPTION')
MONIT_SERVICE = os.environ.get('MONIT_SERVICE')

INFLUXDB_HOST = os.environ.get('INFLUXDB_HOST')
INFLUXDB_PORT = os.environ.get('INFLUXDB_PORT')
INFLUXDB_USER = os.environ.get('INFLUXDB_USER')
INFLUXDB_PASSWORD = os.environ.get('INFLUXDB_PASSWORD')
INFLUXDB_DATABASE = os.environ.get('INFLUXDB_DATABASE')

CURRENT_POINT = {
    "measurement": "monit",
    "tags": {
        "service": MONIT_SERVICE,
    },
    "time": datetime.datetime.utcnow(),
    "fields": {
        "status": not bool(re.search(r'failed|limit matched', MONIT_DESCRIPTION)),
        "description": MONIT_DESCRIPTION,
    }
}


def load_points():
    """Read points from a file."""
    try:
        data = json.loads(
            pathlib.Path(TEMP_FILE).read_text()
        )
    except (FileNotFoundError, json.JSONDecodeError):
        data = []

    return data


def save_points(data):
    """Save points into a file."""
    pathlib.Path(TEMP_FILE).write_text(
        json.dumps(data, default=str)
    )


def delete_points():
    """Delete points file."""
    if os.path.exists(TEMP_FILE):
        os.remove(TEMP_FILE)


if __name__ == '__main__':
    points = load_points()
    points.append(CURRENT_POINT)

    try:
        client = InfluxDBClient(
            INFLUXDB_HOST,
            INFLUXDB_PORT,
            INFLUXDB_USER,
            INFLUXDB_PASSWORD,
            INFLUXDB_DATABASE
        )

        client.write_points(points)
    # pylint: disable=broad-except
    except Exception:
        save_points(points)
    else:
        delete_points()
