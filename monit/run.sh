#!/bin/bash
set -euo pipefail

export HOME=/data
MONITRC="${HOME}/monitrc"
rm -f "${MONITRC}"  # restarted pods reuse the same volumes
echo "set daemon 30" >> "${MONITRC}"
echo "include /config/*" >> "${MONITRC}"
chmod 0700 "${MONITRC}"

# make getpwuid_r happy
if [ -w '/etc/passwd' ]; then
    echo "cki:x:`id -u`:`id -g`:,,,:${HOME}:/bin/bash" >> /etc/passwd
fi

exec /usr/bin/monit -v -I -c "${MONITRC}"
